import { CONFIG } from "../constants/Constants";

export const DOMAIN = CONFIG.IS_PROD ? CONFIG.PRODUCTION_API_HOST : CONFIG.DEVELOPMENT_API_HOST

export const EMAIL_DOMAIN = [
    "digital-dandelion.com",
    "customerfirstai.com",
    "aztecgroup.co.uk" 
]
