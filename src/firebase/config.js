import firebase from "firebase";

const prod_config = {
  apiKey: "AIzaSyAukzaQ_vvomMLE7-vnY0dk-e8pkPjB8nA",
  authDomain: "aztec-prod.firebaseapp.com",
  projectId: "aztec-prod",
  storageBucket: "aztec-prod.appspot.com",
  messagingSenderId: "665149210425",
  appId: "1:665149210425:web:87c5e6738eb0fbbd23f2e8",
  measurementId: "G-9DW4SMJQZ8"
}

const dev_config = {
  apiKey: "AIzaSyB3lYDFVwoCldDgJBCwIi9QHaP1UvUCYHo",
  authDomain: "aztec-technical-poc.firebaseapp.com",
  projectId: "aztec-technical-poc",
  storageBucket: "aztec-technical-poc.appspot.com",
  messagingSenderId: "937707950778",
  appId: "1:937707950778:web:395484f97924c7c7ade883",
  measurementId: "G-793WCH6S7K"
}

export const firebaseui = {
    signInFlow:"popup",
    signInOptions:[
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    ],
    
    callbacks: {
        // Avoid redirects after sign-in.
        signInSuccessWithAuthResult: function(authResult, redirectUrl) {
            console.log(authResult, redirectUrl);
        },
        signInFailure: function(error) {
            // Some unrecoverable error occurred during sign-in.
            // Return a promise when error handling is completed and FirebaseUI
            // will reset, clearing any UI. This commonly occurs for error code
            // 'firebaseui/anonymous-upgrade-merge-conflict' when merge conflict
            // occurs. Check below for more details on this.
            if (error.code === 'auth/multi-factor-auth-required') {
                // The user is a multi-factor user. Second factor challenge is required.
                let resolver = error.resolver;
                console.log(resolver)
                // ...
            } 
            console.log(error);
            // return handleUIError(error);
        },
      },
}

if (!firebase.apps.length) {
    let h = window.location.hostname;
    let config = dev_config;
    if (h.indexOf("aztec-innovation-hub.com") !== -1){
        config = prod_config;
    }
    console.log(config.projectId);
    firebase.initializeApp(config);
}else {
   firebase.app(); // if already initialized, use that one
}

export default firebase;