import React from 'react';
import Header from '../../component/header/Header';
import { Dropdown, Input } from 'semantic-ui-react'
import { Button, Icon } from 'semantic-ui-react'
import { getHistoricalProphet, getProphetPredictions } from '../../services/common_services';
import BezierCurveChart from '../../component/Graphs/Charts/line-chart';

const options = [
    { key: 'default', text: 'Linear', value: 'default' },
    { key: 'logistic', text: 'Logistic', value: 'logistic' },  
]

const Profit = (props) => {

    const [numberOfDays, setNumberOfDays] = React.useState(5);
    const [optionType, setOptionType] = React.useState("default");
    const [isNumExceed, setNumberExceed] = React.useState(false);
    const [temp, setTemp] = React.useState(false);
    const [historicalData, setHistoricalData] = React.useState({
        labels: null,
        historial_data: null,
        yhat: null,
        upper_bound: null,
        lower_bound: null
    });
    const [renderGraph, setRenderGraph] = React.useState(false);

    React.useEffect(() => {        
        let yhat = [], upper_bound = [], lower_bound = [];
        let labels = [], plot_data = [];
        
        getHistoricalProphet().then(data => {
            
            data.forEach((item) => {                
                labels.push(new Date(item.ds).toLocaleDateString())
                plot_data.push(item.y)
            })                                    
            getProphetPredictions(numberOfDays, optionType).then(result => {
                
                result.forEach((item) => {
                    yhat.push({x: new Date(item.ds).toLocaleDateString(), y: item.yhat})
                    upper_bound.push({x: new Date(item.ds).toLocaleDateString(), y: item.yhat_upper})
                    lower_bound.push({x: new Date(item.ds).toLocaleDateString(), y: item.yhat_lower})                
                })
                // labels.push()
                plot_data.push(null);
                setHistoricalData({
                    ...historicalData,
                    labels,
                    historial_data: plot_data,
                    yhat:yhat,
                    upper_bound: upper_bound,
                    lower_bound: lower_bound
                })            
                setRenderGraph(true)
            })          
        });  
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [temp])

    // const getHistoricalData = () => {
        
    // }

    // const getProphetData = () => {
          
    // }

    const handleChange = ({target}) => {
        
        if(target.value > 365 || target.value < 1) setNumberExceed(true)
        else setNumberExceed(false);
        setNumberOfDays(target.value);
    }

    const handleDropDownChange = (e, data) => {
        
        setOptionType(data.value);
    }

    return (
        <div id="profit__prediction_container">
            <Header />
            <div className="container-fluid mt-4">
                <hr />
                <div id="profit_actions">
                    <div className="d-flex">                        
                        <Input
                            type="number"
                            error={isNumExceed}                            
                            style={{width: '33%'}}
                            label={
                                <Dropdown defaultValue='default' selection value={optionType}
                                options={options} onChange={handleDropDownChange} />
                            }
                            value={numberOfDays}
                            labelPosition='right'
                            placeholder='Enter number of days...'
                            min="1"
                            max="365"
                            onChange={handleChange}
                        />                        
                        <Button icon labelPosition='right' className="ms-4" onClick={() => {
                            setRenderGraph(false);
                            setTemp(!temp)
                        }}>
                            Search
                            <Icon name='right arrow' />
                        </Button>
                    </div>
                    {isNumExceed && <span class="alert alert-danger d-inline-block mt-3">Should greater then 1 and less then 366</span>}
                </div>
                <div className="plotted__prophet">
                    {
                        renderGraph ? (
                            <BezierCurveChart chartId="prophet__prediction" chartData={historicalData} />
                        ) : (
                            <div className="m-5" style={{ textAlign: "center" }}>                    
                                <i className="fa fa-spinner fa-spin" style={{fontSize: 30, color: '#09a2fb'}}></i>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    );
}

export default Profit;