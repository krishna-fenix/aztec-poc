import React from 'react';
import Header from '../../component/header/Header';
import NeoVisTest from './Tools/NeoVis-js/neo-vis-test';
import { NEO4JCONFIG } from './Tools/NeoVis-js/neo4j-config';

const DataVisulisation = ({headerHasBackground}) => {
    return (
        <div id="graph__test_container">
            {/* <Header hasBgColor={headerHasBackground} itemToBeActive='/structuring' /> */}
            <div id="neovis__graph_container" className="container-fluid">
                <NeoVisTest config={NEO4JCONFIG} />               
            </div>
        </div>
    );
}

export default DataVisulisation;