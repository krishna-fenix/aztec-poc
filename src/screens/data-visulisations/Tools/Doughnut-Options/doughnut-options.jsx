import React from 'react';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AllOutIcon from '@material-ui/icons/AllOut';
import OpenWithIcon from '@material-ui/icons/OpenWith';
import AssessmentIcon from '@material-ui/icons/Assessment';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import Zoom from '@material-ui/core/Zoom';

const DoughnutOptions = ({
    nodePosX, nodePosY,
    onNodeActionChange,
    openExplorePanel,
    onViewOnly
}) => {
    return (
        <div id="node_actions" className="doughnut action_container" style={{
            display: 'none',               
            left: nodePosX,
            top: nodePosY
        }}>
            <Tooltip title="Expand" placement="right" arrow TransitionComponent={Zoom}>
                <div className="box box1" onClick={() => onNodeActionChange('expand_min')}>
                    {/* <i className="icon icon1 fa fa-plus"></i> */}
                    <Fab size="small" color="primary" className="fab__btn">
                        {/* <OpenWithIcon /> */}
                        <span class="icon icon1 material-icons">open_with</span>
                    </Fab>
                </div>
            </Tooltip>                                    
            <Tooltip title="Explore" placement="bottom" arrow TransitionComponent={Zoom}>
                <div className="box box2" onClick={openExplorePanel}>
                    {/* <i className="icon icon2 fa fa-minus"></i> */}
                    <Fab size="small" color="primary" className="fab__btn" >
                        {/* <AssessmentIcon style={{transform: 'rotate(223deg)'}} /> */}
                        <span class="material-icons icon icon2">loupe</span>
                    </Fab>
                </div>
            </Tooltip>      
            <Tooltip title="collapse" placement="left" arrow TransitionComponent={Zoom}>
                <div className="box box3" onClick={() => onNodeActionChange('collapse')}>
                    <Fab size="small" className="fab__btn">
                        {/* <ExpandLessIcon style={{transform: 'rotate(180deg)'}} /> */}
                        <span class="material-icons icon icon3">unfold_less</span>
                    </Fab>                                              
                </div>
            </Tooltip>   
            <Tooltip title="View Only" placement="top" arrow TransitionComponent={Zoom}>
                <div className="box box4" onClick={() => onViewOnly()}>
                    <Fab size="small" className="fab__btn">
                        {/* <AllOutIcon style={{transform: 'rotate(180deg)'}} /> */}
                        <span class="material-icons icon icon4">adjust</span>
                    </Fab>                                              
                </div>
            </Tooltip>     
        </div>
    );
}

export default DoughnutOptions;