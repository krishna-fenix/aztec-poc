import React from 'react';
import { endOfToday, set } from 'date-fns' 
import TimeRange from 'react-timeline-range-slider' 
import './timeline-slider.css';
import Button from '@material-ui/core/Button';
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import CloseIcon from '@material-ui/icons/Close';

const TimelineSlider = ({onClose, onSlideChange}) => {

    const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const now = new Date();    
    const getIntervalForDate = (date_for) => set(date_for, { hours: 24, minutes: 0, seconds: 0, milliseconds: 0 })
    const selectedStart = new Date(new Date().setFullYear(now.getFullYear() - 5))
    const selectedEnd = now;

    const [isMounted, setIsMounted] = React.useState(false);
    const [error, setError] = React.useState(false);
    const [filter_date_1, setFilterDate1] = React.useState(new Date(new Date().setFullYear(now.getFullYear() - 5)))
    const [filter_date_2, setFilterDate2] = React.useState(new Date())

    const [selectedInterval, setSelectedInterval] = React.useState([
        set(filter_date_1, { hours: 24, minutes: 0, seconds: 0, milliseconds: 0 }),
        set(filter_date_2, { hours: 24, minutes: 0, seconds: 0, milliseconds: 0 })
    ]);

    const [timeInterval, setTimeInterval] = React.useState([
        set(selectedStart, { hours: 24, minutes: 0, seconds: 0, milliseconds: 0 }),
        set(selectedEnd, { hours: 24, minutes: 0, seconds: 0, milliseconds: 0 })
    ]);

    const errorHandler = ({ error }) => setError(error)  

    const onSliderChangeCallback = selectedInterval => {
        console.log(selectedInterval);        
        if(isMounted){
            setSelectedInterval(selectedInterval);
            const firstDate = getDate(selectedInterval[0]);
            const secondDate = getDate(selectedInterval[1]);
            onSlideChange(firstDate, secondDate);   
        }else{
            console.log('initial render change...')
        }
    }

    const getDate = (date_value) => {
        let month = (date_value.getMonth()+1)
        if(month < 10) month = "0"+month
        let date = date_value.getDate() 
        if(date < 10) date = "0"+ date

        const m_date = date_value.getFullYear() + "-" + month + "-" + date
        return m_date;
    }

    const handleDateChange = (values, date_for) => {                    
        if(date_for === "1"){
            setFilterDate1(values);
            setSelectedInterval([getIntervalForDate(values), getIntervalForDate(filter_date_2)])
            setTimeInterval([getIntervalForDate(values), getIntervalForDate(filter_date_2)])
        }else{
            setFilterDate2(values);
            setSelectedInterval([getIntervalForDate(filter_date_1), getIntervalForDate(values)])
            setTimeInterval([getIntervalForDate(filter_date_1), getIntervalForDate(values)])
        }
    };

    React.useEffect(() => {
        setIsMounted(true);
    }, [])

    return (
        <div id="timeline__filter">
            <div className="text-center mt-3 fs-3 position-relative">
                {/* <div>Filter Organizations by date</div> */}
                <div className="close__drawer" onClick={() => onClose('none')}><CloseIcon style={{color: 'white'}} /></div>        
            </div>
            <div className="d-flex justify-content-evenly align-items-end w-100 my-3">
                <div className="date__picker">
                    <p className="text-left mb-0" style={{color: 'white'}}>From</p>                    
                    <SemanticDatepicker clearable={false} maxDate={new Date()} showToday={false} value={filter_date_1} onChange={(e, date) => handleDateChange(date.value, "1")} />                        
                </div>
                <div className="date__picker">
                    <p className="text-left mb-0" style={{color: 'white'}}>To</p>                    
                    <SemanticDatepicker clearable={false} maxDate={new Date()} value={filter_date_2} onChange={(e, date) => handleDateChange(date.value, "2")} />
                </div> 
                {/* <div className="text-center mb-2">
                    <Button variant="contained" color="primary" onClick={() => onClose('none')}>
                        Filter
                    </Button>
                </div> */}
            </div>                            
            <TimeRange
                error={error}  
                ticksNumber={36}  
                selectedInterval={selectedInterval}  
                timelineInterval={timeInterval}  
                onUpdateCallback={errorHandler}  
                onChangeCallback={onSliderChangeCallback}
                disabledIntervals={[]}             
            />
            <div className="d-flex timeline__year mb-1">
                {
                    (filter_date_1.getFullYear().toString() === filter_date_2.getFullYear().toString()) ? 
                        [...MONTHS.slice(filter_date_1.getMonth(), filter_date_2.getMonth()+1)]
                        .map((month) => (
                            <div className="month__item text-center">{month}</div>
                        ))
                        : 
                        [...MONTHS.slice(filter_date_1.getMonth()), ...MONTHS.slice(0, filter_date_2.getMonth()+1)]
                        .map((month) => (
                            <div className="month__item text-center">{month}</div>
                        ))                       
                }                
            </div>
            <div className="text-center timeline__year">
                {
                    (filter_date_1.getFullYear().toString() === filter_date_2.getFullYear().toString()) ? (
                        filter_date_2.getFullYear().toString()
                    ) : (
                        filter_date_1.getFullYear().toString() + "-" +
                        filter_date_2.getFullYear().toString() 
                    )
                }
            </div>
        </div>
    );
}

export default TimelineSlider;