import React from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Organisations from '../../../../../assets/images/organisations-1.svg'
import Person from '../../../../../assets/images/person-1.svg';
import Invesment from '../../../../../assets/images/investments-1.svg';
import Fundings from '../../../../../assets/images/funding.svg';
import Employees from '../../../../../assets/images/business-communication.svg';

// MATCH (b)-[r:PART_OF|INVESTED_INTO]->(o:Organizations) RETURN b,o,r LIMIT 100

const NeoOptions = ({defaultOptions, sidebarWidth, closeSideBar, onOptionChange}) => {

    const [checked, setChecked] = React.useState(defaultOptions);
    const TYPES = {        
        0: 'PART_OF',
        1: 'INVESTMENT_OF',
        2: 'FUNDINGROUND_OF',
        3: 'NO_OF_EMPLOYEES'
    }

    const OPTIONS = [        
        {icon: Person, label: 'People', description: 'People connected to organizations'},
        {icon: Invesment, label: 'Investements', description: 'Companies invested into organizations'},
        {icon: Fundings, label: 'Funding Rounds', description: 'Fund type of organizations.'},
        {icon: Employees, label: 'Employee Count', description: 'Number of employees work in an organizations.'},
    ];    

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
    
        setChecked(newChecked);
    };

    const onOptionUpdate = () => {
        // console.log(checked);
        let relations = 'INVESTED_INTO|';
        if(checked.length > 0){
            checked.forEach((item) => {
                relations += ` ${TYPES[item]}`
            })
        }else relations = relations.replace('|', '')
        
        relations = relations.replace(' ', '').replaceAll(' ', '|');
        // console.log(relations);
        const q = `MATCH (b)-[r:${relations}]->(o:Organizations) RETURN b,o,r LIMIT 100`;
        // console.log(q)
        onOptionChange(q, checked, relations)
    }

    return (
        <div id="custom__graph_drawer" className="custom_az_drawer"
        style={{
            width: sidebarWidth
        }}>            
            <div className="az_drawer_header d-flex">
                <ArrowBackIosIcon style={{color: '#fff', cursor: 'pointer'}} onClick={() => closeSideBar(false)} />
                <p style={{flex: 1,color: 'white'}}>Options</p>
            </div>
            <div className="small pt-3 pb-0 px-4">Check/Uncheck to view only particular nodes.</div>
            <div className="az_drawer__body az_border__bottom p-3 pt-0">
                <List>
                    <ListItem dense button disabled={true}>
                        <ListItemIcon>
                            <Checkbox
                                edge="start"
                                checked={true}
                                tabIndex={-1}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 'checkbox-list-label-00' }}
                            />
                        </ListItemIcon>
                        <img src={Organisations} alt="Organizations" className="me-3" style={{width: 20, height: 20}} />
                        <ListItemText id="checkbox-list-label-00" primary={`Organizations`} />                                
                    </ListItem>
                    {OPTIONS.map((value, index) => {
                        const labelId = `checkbox-list-label-${value.label}`;
                        return (
                            <ListItem key={index} role={undefined} dense button                                                            
                             onClick={handleToggle(index)}>
                                <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    checked={checked.indexOf(index) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{ 'aria-labelledby': labelId }}
                                />
                                </ListItemIcon>
                                <img src={value.icon} alt={value.label} className="me-3" style={{width: 20, height: 20}} />                                
                                <ListItemText id={labelId} primary={`${value.label}`} />                                
                            </ListItem>
                        );
                    })}
                </List>
                <button className="btn btn-primary algo__action_btn ms-3"
                style={{width: 100}} onClick={() => onOptionUpdate()}>Update</button>
            </div>   
            <div id="nodes__information" className="mt-3">
                <ul className="" style={{listStyle: 'none'}}>
                    <li className="my-3">                        
                        <img src={Organisations} alt="Organizations" className="me-3" style={{width: 20, height: 20}} />
                        <small>Organizations</small>
                    </li>
                    {
                        OPTIONS.map((option) => (
                            <li className="my-3">
                                <img src={option.icon} alt={option.label} className="me-3" style={{width: 20, height: 20}} />
                                <small>{option.description}</small>
                            </li>
                        ))
                    }
                </ul>
            </div>                         
        </div>
    );
}

export default NeoOptions;