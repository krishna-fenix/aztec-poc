import React from 'react';
import './neo-sidebar.css';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import MemoryIcon from '@material-ui/icons/Memory';
import NeoVis from 'neovis.js';
import GLoader from '../../../../../assets/images/g_loader.svg';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { CURRENCY_SYMBOLS, getFormatedCurrency } from '../../../../../services/common_services';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined';

const NeoSideBar = ({neoConfig, sidebarWidth, node, closeSideBar, selectedNodeId, onShowSimilarity}) => {

    const [loadingGraph, setToLoadGraph] = React.useState(true);
    const [viz, setViz] = React.useState(null);
    const [ceo, setCEO] = React.useState('Unknown');        
    const [findActive, setActive] = React.useState(false);
    const [hasGraphRecords, setHasGraphRecords] = React.useState(true);

    React.useEffect(() => {
        
        const vis = new NeoVis({
            container_id: "sub_graph",           
            initial_cypher: `MATCH (p:People)-[r:PART_OF]->(o:Organizations {uuid: '${node?.properties?.uuid}'}) RETURN p,o,r`,
            // MATCH (o:Organizations {uuid: '${node?.properties?.uuid}'})-[r:INVESTED_INTO]->(b) RETURN o,r,b LIMIT 10
            ...neoConfig
        });
        setViz(vis);        
        
        vis.registerOnEvent('completed', (e) => {
            
            // console.log("complete event...", e);   
            if(e?.record_count === 0) setHasGraphRecords(false)

            setToLoadGraph(false);   
            if(localStorage.getItem('findSimilarity')) {
                setActive(true);               
            }
            else {
                const connectedNodes = vis['_network'].getConnectedNodes(selectedNodeId);
                setCEO(vis["_nodes"][connectedNodes[0]]?.raw?.properties?.name);
            }          
           
            vis['_network'].on('click', (event)=> {
                               
            })

        })              
        
        vis.render();     
        return () => {
            if(vis.nodes.getIds().length === 0) localStorage.removeItem('findSimilarity');
        }                
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const findSimilarity = () => {
        
        localStorage.setItem('findSimilarity', true);
        setToLoadGraph(true);   
        // const q = `
        // MATCH (org1:Organizations {name:'${node?.properties?.name}', specialties_wikidata_fetched:True})
        // MATCH (org2:Organizations {specialties_wikidata_fetched:True}) WITH org1,org2 LIMIT 5
        // WITH collect(org1) as org1, collect(org2) as org2
        // CALL apoc.cypher.mapParallel("MATCH (_)-[r1:BELONGS_TO]->(c1:Category) RETURN c1,_ as o1", {}, org1) YIELD value as p1
        // CALL apoc.cypher.mapParallel("MATCH (_)-[r2:BELONGS_TO]->(c2:Category) RETURN c2,_ as o2", {}, org2) YIELD value as p2
        // WITH p1,p2
        // WITH collect(distinct p1.c1) as catNames1, collect(distinct p2.c2) as catNames2, p1.o1 as o1,p2.o2 as o2
        // CALL apoc.cypher.mapParallel("MATCH (_)-[:IN_CATEGORY|SUBCAT_OF]-(page1) RETURN page1", {catNames1:catNames1}, catNames1) YIELD value as val1
        // WITH collect(id(val1.page1)) AS p1Cuisine,o1,o2, catNames2
        // CALL apoc.cypher.mapParallel("MATCH (_)-[:IN_CATEGORY|SUBCAT_OF]-(page2) RETURN page2", {catNames2:catNames2}, catNames2) YIELD value as val2
        // WITH p1Cuisine, collect(id(val2.page2)) AS p2Cuisine,o1,o2
        // WITH max(gds.alpha.similarity.jaccard(p1Cuisine, p2Cuisine)) as similarity, o1,o2
        // WHERE similarity>0 AND similarity < 1
        // RETURN avg(similarity) AS similarity, o1,o2,apoc.create.vRelationship(o1,'SIMILARITY',{count:count(o2), similarityCount:similarity},o2) as rel
        // `;
        const q = `MATCH (o1:Organizations {name: '${node?.properties?.name}'})-[:BELONGS_TO]->(cat1)
        WITH o1, collect(id(cat1)) AS cat1_new
        MATCH (o2:Organizations)-[:BELONGS_TO]->(cat2) WHERE o1 <> o2
        WITH o1 as from, cat1_new, o2 as to, collect(id(cat2)) AS cat2_new, o2.cb_url AS url		
        WITH gds.alpha.similarity.jaccard(cat1_new, cat2_new) AS similarity,from,to,url
        RETURN from,to,url, similarity,
        apoc.create.vRelationship(from,'SIMILARITY',{count:count(to), similarityCount:similarity},to) as rel
        ORDER BY similarity DESC LIMIT 100`;
        viz.renderWithCypher(q);
    }


    return (
        <div id="custom__graph_drawer" className="custom_az_drawer"
        style={{
            width: sidebarWidth
        }}>            
            <div className="az_drawer_header d-flex">
                <ArrowBackIosIcon style={{color: '#fff', cursor: 'pointer'}} onClick={() => closeSideBar(false)} />
                <p style={{flex: 1,color: '#fff'}}>Company Info</p>
            </div>
            <div className="az_drawer__body az_border__bottom p-3">
                <Accordion className="az_drawer_accordian">
                    <AccordionSummary
                    expandIcon={<ExpandMoreIcon style={{color: 'white'}} />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                        <Typography>Company Details</Typography>
                    </AccordionSummary>
                    <AccordionDetails className="flex-column">
                        {
                            node?.properties?.logo_url ? 
                            <div style={{width: 50}}>
                                <img src={node?.properties?.logo_url} alt="Company Logo" />
                            </div>  : ''
                        }
                        <table class="table table-striped mb-0 az_cpm_inf">
                            <tbody>
                                <tr>                    
                                    <td>Company name</td>
                                    <td>{node?.properties?.name || 'N/A'}</td>                    
                                </tr>
                                <tr>                    
                                    <td>Sector</td>
                                    <td>{node?.properties?.category_groups_list || 'N/A'}</td>                    
                                </tr>                  
                                <tr>                    
                                    <td>Industry</td>
                                    <td>{node?.properties?.primary_role || 'N/A'}</td>                    
                                </tr>
                                <tr>                    
                                    <td>Founded On</td>
                                    <td>{node?.properties?.founded_on || 'N/A'}</td>                    
                                </tr>
                                <tr>                    
                                    <td>Website</td>
                                    <td>
                                        {node?.properties?.homepage_url ? <a href={node?.properties?.homepage_url} target="__blank">
                                            {node?.properties?.homepage_url}
                                        </a> : 'N/A'}
                                    </td>                    
                                </tr>
                                <tr>                    
                                    <td>No. of employees</td>
                                    <td>{node?.properties?.employee_count === 'unknown' ? 'N/A' : node?.properties?.employee_count}</td>                    
                                </tr>
                                <tr>                    
                                    <td>Revenue</td>
                                    <td>
                                        {node?.properties?.total_funding_currency_code ? CURRENCY_SYMBOLS[node?.properties?.total_funding_currency_code] : ''} 
                                        {node?.properties?.total_funding ? getFormatedCurrency(parseInt(node?.properties?.total_funding)) : 'N/A'}
                                    </td>                    
                                </tr>
                                <tr>                    
                                    <td>Address</td>
                                    <td>{node?.properties?.address || 'N/A'}</td>                    
                                </tr>
                                <tr>                    
                                    <td>Type</td>
                                    <td>{node?.properties?.roles || 'Unknown'}</td>                    
                                </tr>
                                <tr>                    
                                    <td>CEO</td>
                                    <td>{ceo}</td>                    
                                </tr>
                            </tbody>
                        </table>
                    </AccordionDetails>
                </Accordion>
            </div>                
            <div className="algo__actions az_border__bottom p-3">
                <small style={{color: '#a7a7a7'}}>AI Algorithms</small>
                <div className="algo-actions mt-3 d-flex justify-content-between align-items-center">
                    <MemoryIcon style={{color: '#59c580', fontSize: 34}} />
                    <button className="algo__action_btn" onClick={() => findSimilarity()}>
                        Similar Companies
                    </button>
                    <button className="algo__action_btn">
                        Match with fund
                    </button>
                    <button className="algo__action_btn">
                        Match portfolio
                    </button>
                </div>
            </div>
            {
                loadingGraph && (
                    <div class="g_loader" style={{zIndex: -1}}>
                        <img src={GLoader} alt="laoder" />
                    </div>
                )
            }
            { !hasGraphRecords && <div className="no__graph text-center">
                <AccountTreeOutlinedIcon style={{
                    color: 'white', fontSize: 50
                }} />
                <div>No records found</div>
            </div> }
            {
                findActive && hasGraphRecords ? (
                    <div className="text-right p-2">
                        <OpenInNewIcon className="openInNew" onClick={()=>onShowSimilarity(node?.properties?.name)} />
                    </div>
                ) : ''
            }
            <div id="sub_graph"></div>
        </div>
    );
}

export default NeoSideBar;