import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
// import EditIcon from '@material-ui/icons/Edit';
// import RefreshIcon from '@material-ui/icons/Refresh';
// import ZoomInIcon from '@material-ui/icons/ZoomIn';
// import ZoomOutIcon from '@material-ui/icons/ZoomOut';
// import ZoomOutMapIcon from '@material-ui/icons/ZoomOutMap';
import Person from '../../../../../assets/images/person-1.svg';
import Invesment from '../../../../../assets/images/investments-1.svg';
import Fundings from '../../../../../assets/images/funding.svg';
import FunnelFilter from '../../../../../assets/images/funnel_filter.svg'
import CheckUncheck from '../../../../../assets/images/check-uncheck.svg';
import ToggleOptions from '../../../../../assets/images/toggle_off.svg';
import PrismLogo from '../../../../../assets/images/prism-logo_transparetn.svg';
import ZoomIn from '../../../../../assets/images/zoom_in_white.svg';
import ZoomOut from '../../../../../assets/images/zoom_out_white.svg';
import FullScreen from '../../../../../assets/images/zoom_out_map_white.svg';
import Refresh from '../../../../../assets/images/refresh_white.svg';

const ActionBar = ({
    openFilterDrawer,    
    onRefresh,
    openOptionsDrawer,
    quickOptionUpdate
}) => {

    const el = (id) => { return document.getElementById(id); }
    const [toggleSelectNode, setToggleSelectNode] = React.useState(false);

    const onZoomIn = () => {
        const canvas = document.querySelector('.vis-network > canvas');
        if (!canvas) return; // It should always find the canvas.
        const box = canvas.getBoundingClientRect();
        canvas.dispatchEvent(
        new WheelEvent('wheel', {
                deltaY: -1,
                clientX: box.left + box.width / 2,// x,
                clientY: box.top + box.height / 2,// y,
                scale: 1.5
            })
        );
        el('node_actions').style.display = 'none';

    }

    const onZoomOut = () => {
        const canvas = document.querySelector('.vis-network > canvas');
        if (!canvas) return; // It should always find the canvas.
        const box = canvas.getBoundingClientRect();
        canvas.dispatchEvent(
        new WheelEvent('wheel', {
                deltaY: 1,
                clientX: box.left + box.width / 2,
                clientY: box.top + box.height / 2
            })
        );
        el('node_actions').style.display = 'none';
    }

    const viewFullScreen = () => {
        const elem = document.getElementById('neovis__graph_container');    
        elem.style.backgroundColor = '#0e1117'
        elem.requestFullscreen();
    }

    return (
        <div className="graph_actions d-flex justify-content-between pt-4">
            <img src={PrismLogo} alt="Prism" className="prism__logo" />
            <div className="options__container">
                <div className="gradient__module_border">
                    <ul className="action__types d-flex m-0 p-0">
                        <li className="action_item d-flex flex-column align-items-center" onClick={() => openFilterDrawer('block')}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={FunnelFilter} alt="Funnel Filter" style={{width: 24, 
                                    height: 24}}/>
                            </Tooltip>
                            <small className="text-small">Filter</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={onZoomIn}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={ZoomIn} alt="Zoom In" style={{width: 24, 
                                    height: 24}}/>
                            </Tooltip>
                            <small className="text-small">Zoom In</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={onZoomOut}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={ZoomOut} alt="Zoom Out" style={{width: 24, 
                                    height: 24}}/>
                            </Tooltip>
                            <small className="text-small">Zoom Out</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={viewFullScreen}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={FullScreen} alt="Full Screen" style={{width: 24, 
                                    height: 24}}/>
                            </Tooltip>
                            <small className="text-small">Full Screen</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={() => setToggleSelectNode(!toggleSelectNode)}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={CheckUncheck} alt="Select Node" style={{width: 24, 
                                    height: 24}} />   
                            </Tooltip>
                            {/* <span class="material-icons">face</span> */}
                            <small className="text-small">Select Node</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={() => openOptionsDrawer(true)}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={ToggleOptions} alt="Options" style={{width: 24, 
                                    height: 24}} />   
                            </Tooltip>
                            <small className="text-small">Options</small>
                        </li>
                        <li className="action_item d-flex flex-column align-items-center" onClick={() => onRefresh()}>
                            <Tooltip title="" placement="bottom" arrow TransitionComponent={Zoom}>
                                {/* <i className="fa fa-filter"></i> */}
                                <img src={Refresh} alt="Options" style={{width: 24, 
                                    height: 24}} />   
                            </Tooltip>
                            <small className="text-small">Refresh</small>
                        </li>
                        {/* <li className="action_item" onClick={onZoomIn}>
                            <Tooltip title="Zoom In" placement="top" arrow TransitionComponent={Zoom}>
                                <ZoomInIcon />
                            </Tooltip>
                        </li>
                        <li className="action_item" onClick={onZoomOut}>
                            <Tooltip title="Zoom Out" placement="bottom" arrow TransitionComponent={Zoom}>
                                <ZoomOutIcon />
                            </Tooltip>
                        </li>
                        <li className="action_item" onClick={viewFullScreen}>
                            <Tooltip title="Full Screen" placement="top" arrow TransitionComponent={Zoom}>
                                <ZoomOutMapIcon />
                            </Tooltip>
                        </li>                                                      
                        <li className="action_item" onClick={() => onRefresh()}>
                            <Tooltip title="Refresh" placement="right" arrow TransitionComponent={Zoom}>
                                <RefreshIcon />
                            </Tooltip>
                        </li> */}
                    </ul>
                    {
                        toggleSelectNode ? (
                            <ul className="d-flex select_nodes">
                                <li className="action_item" onClick={() => {
                                    setToggleSelectNode(false)
                                    quickOptionUpdate(`PART_OF`)
                                }}>
                                    <Tooltip title="View only People" placement="bottom" arrow TransitionComponent={Zoom}>
                                        <img src={Person} alt="Organizations" style={{width: 20, 
                                            height: 20, borderRadius: '50%', backgroundColor: 'white'}} />
                                    </Tooltip>
                                </li> 
                                <li className="action_item" onClick={() => {
                                    setToggleSelectNode(false)
                                    quickOptionUpdate(`INVESTMENT_OF`)
                                }}>
                                    <Tooltip title="View only Investments" placement="bottom" arrow TransitionComponent={Zoom}>
                                        <img src={Invesment} alt="Organizations" style={{width: 20, height: 20}} />
                                    </Tooltip>
                                </li> 
                                <li className="action_item" onClick={() => {
                                    setToggleSelectNode(false)
                                    quickOptionUpdate(`FUNDINGROUND_OF`)
                                }}>
                                    <Tooltip title="View only Fundings" placement="bottom" arrow TransitionComponent={Zoom}>
                                        <img src={Fundings} alt="Organizations" style={{width: 20, height: 20}} />
                                    </Tooltip>
                                </li>  
                            </ul>
                        ) : ''
                    }
                </div>
            </div>
            {/* <ul className="action__types d-flex m-0 p-0" style={{listStyle: 'none'}}>
                <li className="action_item" onClick={() => quickOptionUpdate(`PART_OF`)}>
                    <Tooltip title="View only People" placement="bottom" arrow TransitionComponent={Zoom}>
                        <img src={Person} alt="Organizations" style={{width: 20, 
                            height: 20, borderRadius: '50%', backgroundColor: 'white'}} />
                    </Tooltip>
                </li> 
                <li className="action_item" onClick={() => quickOptionUpdate(`INVESTMENT_OF`)}>
                    <Tooltip title="View only Investments" placement="bottom" arrow TransitionComponent={Zoom}>
                        <img src={Invesment} alt="Organizations" style={{width: 20, height: 20}} />
                    </Tooltip>
                </li> 
                <li className="action_item" onClick={() => quickOptionUpdate(`FUNDINGROUND_OF`)}>
                    <Tooltip title="View only Fundings" placement="bottom" arrow TransitionComponent={Zoom}>
                        <img src={Fundings} alt="Organizations" style={{width: 20, height: 20}} />
                    </Tooltip>
                </li>               
                <li className="action_item" onClick={() => openOptionsDrawer(true)}>
                    <Tooltip title="Edit Options" placement="bottom" arrow TransitionComponent={Zoom}>
                        <EditIcon />
                    </Tooltip>
                </li>
            </ul> */}
        </div>
    );
}

export default ActionBar;