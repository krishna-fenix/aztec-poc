import { CONFIG } from "../../../../constants/Constants";
import Organisations from '../../../../assets/images/organisations-1.svg'
import Person from '../../../../assets/images/person-1.svg';
import Invesment from '../../../../assets/images/investments-1.svg';
import Fundings from '../../../../assets/images/funding.svg';
import Employees from '../../../../assets/images/business-communication.svg';

const neo4jUri = CONFIG.NEO4J.server_uri;
const pass =  CONFIG.NEO4J.password;

export const NEO4JCONFIG = {    
    server_url: neo4jUri,
    server_user: CONFIG.NEO4J.username,
    server_password: pass,
    encrypted: 'ENCRYPTION_ON', 
    trust: 'TRUST_SYSTEM_CA_SIGNED_CERTIFICATES',
    server_database: 'test',
    labels: {
        "Organizations": {
            "caption": "name",                    
            "size": "total_funding",
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "font": {
                "size":16,
                "color":"#59c681",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },
            "image": Organisations,
            "title_properties": []
        },              
        "Investments": {
            "caption": "name",                    
            "image": Invesment,
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                   
            "size": 6.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "FundingRounds": {
            "caption": "name",  
            "image": Fundings,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 3.0,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "People": {
            "caption": "name",                    
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },
            "image": Person,         
            "size": 1.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },  
        "NoOfEmployees": {
            "caption": "name",  
            "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 3.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "Acquisitions": {
            "caption": "name",  
            // "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 2.0,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "Category": {
            "caption": "catName",  
            // "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 2.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "RootCategory": {
            "caption": "catName",  
            // "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 2.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "Industries": {
            "caption": "name",  
            // "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 2.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
        "Page": {
            "caption": "pageTitle",  
            // "image": Employees,                  
            "font": {
                "size":16,
                "color":"#ffffff",
                strokeWidth: 0,
                strokeColor: 'transparent'
            },                    
            "size": 2.5,
            // "sizeCypher": "MATCH (n) WHERE id(n) = $id RETURN SIZE((n)--()) AS s;",
            "title_properties": []
        },
    },
    relationships: {
        "PART_OF": {
            "thickness": "weight",
            "caption": false,                            
        },
        "INVESTMENT_OF": {
            "thickness": "weight",
            "caption": false,
        },
        "INVESTED_INTO": {
            "thickness": "weight",
            "caption": false,
        },
        "FUNDINGROUND_INVESTMENTS": {
            "thickness": "weight",
            "caption": false,
        },
        "FUNDINGROUND_OF": {
            "thickness": "weight",
            "caption": false,
        },
        "ACQUIRED_BY": {
            "thickness": "weight",
            "caption": false,
        },
        "NO_OF_EMPLOYEES": {
            "thickness": "weight",
            "caption": false,
        },
        "ACQUISITION_OF": {
            "thickness": "weight",
            "caption": false,
        },
        "BELONGS_TO": {
            "thickness": "weight",
            "caption": false,
        },
        "SIMILARITY": {
            "thickness": "similarityCount",
            "caption": false,
        },
        "INDUSTRIES": {
            "thickness": "weight",
            "caption": false,
        },
        "SUBCAT_OF": {
            "thickness": "weight",
            "caption": false,
        },
        "IN_CATEGORY": {
            "thickness": "weight",
            "caption": false,
        }
    },
             
};

export const NODE_RELATIONS = {
    Organizations: ["PART_OF", "INVESTMENT_OF", "INVESTED_INTO", "NO_OF_EMPLOYEES", "BELONGS_TO", "INDUSTRIES",
    "FUNDINGROUND_OF", "ACQUIRED_BY", "ACQUISITION_OF"],
    Investments: ["FUNDINGROUND_INVESTMENTS", "INVESTMENT_OF"],
    FundingRounds: ["FUNDINGROUND_INVESTMENTS","FUNDINGROUND_OF"],
    People: ["PART_OF"],
    Acquisitions: ["ACQUISITION_OF"],
    Category: ["BELONGS_TO","IN_CATEGORY", "SUBCAT_OF"],
    NoOfEmployees: ["NO_OF_EMPLOYEES"],
    Industries: ["INDUSTRIES"],
    Level1Category: ["IN_CATEGORY","SUBCAT_OF", "BELONGS_TO"],
    Level2Category: ["IN_CATEGORY","SUBCAT_OF", "BELONGS_TO"],
    Page: ["IN_CATEGORY"],
    RootCategory: ["IN_CATEGORY","SUBCAT_OF", "BELONGS_TO"]
}

export const getQuery = (node_type) => {
    const relations = NODE_RELATIONS[node_type];
    let str = ``;
    let unwind = ``;
    let return_path = []
    relations.forEach((relation, index) => {        
        str += `CALL apoc.cypher.run('CALL apoc.path.expandConfig(p, {
            relationshipFilter: "${relation}",
            minLevel: 1,
            maxLevel: 1,
            limit: 5
        }) YIELD path as path${index} RETURN collect(COALESCE(path${index}, [null])) as path${index+1}', {p: p}) yield value as val${index+1} `
        unwind += `UNWIND CASE SIZE(val${index+1}.path${index+1}) WHEN 0 THEN [null] ELSE val${index+1}.path${index+1} END as path${index+1} `
        return_path.push(`nodes(path${index+1}), relationships(path${index+1})`)
    })
    str += unwind;
    str += ` RETURN `+ return_path.join()
    // console.log(str);
    return str;
}