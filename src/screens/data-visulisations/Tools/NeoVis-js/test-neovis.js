import React from 'react';
import '../../LIb/D3-js/d3-neo.css';
import { getGraph } from '../../LIb/D3-js/neo4j-api';

const TestNeovis = (props) => {
    
    const renderD3Graph = () => {
        const width = 800, height = 800;
        const d3 = window.d3;        
        
        
        getGraph()
          .then(graph => {
            
            
            const force = d3.forceSimulation(graph.nodes)
            .force("charge", d3.forceManyBody().strength(-50))
            .force("link", d3.forceLink(graph.links).id(function(d) {return d.id; }).distance(30).strength(2))
            
            .force("x", d3.forceX(width / 2).strength(1))
            .force("y", d3.forceY(height / 2).strength(1))
            // .charge(-200).linkDistance(30);

            // var labelLayout = d3.forceSimulation(label.nodes)
            // .force("charge", d3.forceManyBody().strength(-50))
            // .force("link", d3.forceLink(graph.links).distance(0).strength(2));
      
            const svg = d3.select("#d3_neo").append("svg")
            .attr("width", "100%").attr("height", "100%")
            .attr("pointer-events", "all");

            force.nodes(graph.nodes)
            // .links(graph.links).start();
            
            d3.forceLink().links(graph.links)
            .distance(30).id(function(d) { return d.name; })
            

            var drag = d3.drag().on('drag', function() {
                // dragMove(this, obj, 'points')                
            });
      
            const link = svg.selectAll(".link")
              .data(graph.links).enter()
              .append("line").attr("class", "link");
      
            const node = svg.selectAll(".node")
              .data(graph.nodes).enter()
              .append("circle")
              .attr("class", d => {
                return "node " + d.label
              })
              .attr("r", 10)
              .call(drag);
      
            // html title attribute
            node.append("title")
              .text(d => {
                return d.title;
              });
      
            // force feed algo ticks
            force.on("tick", () => {
              link.attr("x1", d => {
                  
                return d.source.x;
              }).attr("y1", d => {
                return d.source.y;
              }).attr("x2", d => {
                return d.target.x;
              }).attr("y2", d => {
                return d.target.y;
              });
      
              node.attr("cx", d => {
                return d.x;
              }).attr("cy", d => {
                return d.y;
              });
            });
          });
    }

    React.useEffect(() => {
        renderD3Graph() 
    }, [])

    return (
        <div id="d3_neo">
            {/* <svg id='viz'></svg> */}
        </div>
    );
}

export default TestNeovis;