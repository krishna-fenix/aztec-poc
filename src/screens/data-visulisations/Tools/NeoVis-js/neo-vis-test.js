import React from 'react';
import NeoVis from 'neovis.js';
import './neo-vis.css';
import GLoader from '../../../../assets/images/g_loader.svg';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import Grid from '@material-ui/core/Grid';
import NeoSideBar from './Neo-side-bar/neo-side-bar';
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined';
import { notifyInfo } from '../../../../services/toaster-service';
import { CURRENCY_SYMBOLS, getFormatedCurrency } from '../../../../services/common_services';
import NeoOptions from './Neo-Options/neo-options';
import DoughnutOptions from '../Doughnut-Options/doughnut-options';
import ActionBar from './Action-Bar/action-bar';
import TimelineSlider from '../Timeline-Slider/timeline-slider';
import { getQuery } from './neo4j-config';
import GiantMenu from '../../../../component/Giant-Menu/giant-menu';

const INICIAL_CYPHER = `MATCH (p:People)-[r:PART_OF]->(o:Organizations) RETURN p,o,r LIMIT 100`;
// `MATCH (b)-[r:INVESTED_INTO]->(o:Organizations) RETURN b,o,r LIMIT 100`;

const NeoVisTest = ({ config }) => {

    const [viz, setViz] = React.useState(null);    
    const [toggleSideNav, setSidenavToggle] = React.useState(false);
    const isNodeAction = React.useRef(true);
    const [nodeActionPos, setNodeActionPos] = React.useState({x: "0px", y: "0px"});
    const [selectedNode, setSelectedNode] = React.useState(null);
    const [clickedNode, setClickedNode] = React.useState(null);
    const [loadingGraph, setToLoadGraph] = React.useState(true);
    const [hasGraphRecords, setHasGraphRecords] = React.useState(true);
    const [toggleOptionNav, setToggleOptionsNav] = React.useState(false);    
    const [allowedOptions, setAllowedOptions] = React.useState([0,1,2,3]);
    const [relations, setQueryRelations] = React.useState('PART_OF|INVESTED_INTO|INVESTMENT_OF|FUNDINGROUND_OF|NO_OF_EMPLOYEES');
    const el = (id) => { return document.getElementById(id); }

    React.useEffect(() => {
        
        const vis = new NeoVis({
            container_id: "viz",
            initial_cypher: INICIAL_CYPHER,
            ...config           
        });
        setViz(vis);        
        // console.time('neoTime')
        let selectedNode = true;
        let tempUUId = null;
        if(localStorage.getItem("gParentNodes")){
            localStorage.removeItem('gParentNodes')
        }
        
        vis.registerOnEvent('completed', () => {                         

            setToLoadGraph(false);        
            let grand_parentNode = [];                                
            for(const node in vis["_nodes"]){
                const elem = vis["_nodes"][node];
                if(elem?.group === "Organizations"){
                    grand_parentNode.push(+node);
                }
            }
                        
            if(grand_parentNode.length === 0){
                setHasGraphRecords(false);
            }

            if(!localStorage.getItem("gParentNodes")){
                localStorage.setItem("gParentNodes", JSON.stringify(grand_parentNode))
            }     
            
            if(localStorage.getItem('viewOnlyMode') || localStorage.getItem('viewSimilarNodes')){
                
                if(!localStorage.getItem('renderComplete')){
                    
                    setTimeout(() => {                    
                        document.getElementById('hid_exp').click()
                    }, 2500)                
                    localStorage.setItem('renderComplete', true)
                }                      
            }

            if(localStorage.getItem('viewSimilarNodes') || localStorage.getItem('findSimilarity')){
                if(!localStorage.getItem('similarComplete')){
                    setTimeout(() => {                                        
                        createRingAroundNode(vis);                        
                    }, 2500)                
                    localStorage.setItem('similarComplete', true)
                    localStorage.removeItem('findSimilarity')
                }
            }
                    
            // vis['_network'].on('click', (event)=> {

            //     console.log(vis["_nodes"][event.nodes[0]]);
            //     setSidenavToggle(false);
            //     setToggleOptionsNav(false);
                
            //     if(vis["_nodes"][event.nodes[0]]){
            //         const uuid = vis["_nodes"][event.nodes[0]].raw.properties.uuid;
            //         const node_label = vis["_nodes"][event.nodes[0]]?.raw?.labels[0];
            //         setSelectedNode(vis["_nodes"][event.nodes[0]]?.raw)
            //         if(['Organizations', 'FundingRounds', 'Investments'].includes(node_label)){ 
            //             // toggleSideNav.current = !toggleSideNav.current;
            //             setClickedNode(event.nodes[0]);
            //             updateCanvasCordinates(event)                         
                      
            //             if(isNodeAction?.current){
            //                 el('node_actions').style.display = 'block';
            //             }else{
            //                 el('node_actions').style.display = 'none';
            //             }
            //             if(!(isNodeAction?.current) && tempUUId !== uuid){
            //                 el('node_actions').style.display = 'none';   
            //                 el('node_actions').style.display = 'block';
            //                 selectedNode = true;                          
            //                 isNodeAction.current = true;
            //             }else{                        
            //                 selectedNode = !selectedNode;                            
            //                 isNodeAction.current = selectedNode;                             
            //             }
            //             tempUUId = uuid;                                                                      
            //         }
            //     }
              
            // })
            
            vis['_network'].on('dragging', (event)=> {
                
                if(event?.nodes.length > 0){                    
                    updateCanvasCordinates(event);
                }else{
                    selectedNode = false;
                    isNodeAction.current = false;
                    el('node_actions').style.display = 'none';   
                }

            })            
        
            vis['_network'].on('zoom', (event)=> {
                selectedNode = false;
                isNodeAction.current = false;
                el('node_actions').style.display = 'none';  
            })   
                
            vis['_network'].on('selectNode', (event)=> {                  
                console.log(event);              
                
                setSidenavToggle(false);
                setToggleOptionsNav(false);
                const nodeInfo = vis.nodes.get(event.nodes[0]);
                
                if(vis["_nodes"][event.nodes[0]]){
                    console.log('hecl...')
                    const uuid = vis["_nodes"][event.nodes[0]]?.raw.properties.uuid;
                    const node_label = vis["_nodes"][event.nodes[0]]?.raw?.labels[0];
                    setSelectedNode(vis["_nodes"][event.nodes[0]]?.raw)
                    
                    if(['Organizations', 'FundingRounds', 'Investments'].includes(node_label)){ 
                        // toggleSideNav.current = !toggleSideNav.current;
                        setClickedNode(event.nodes[0]);
                        updateCanvasCordinates(event)                         
                      
                        if(isNodeAction?.current){
                            el('node_actions').style.display = 'block';
                        }else{
                            el('node_actions').style.display = 'none';
                        }
                        if(!(isNodeAction?.current) && tempUUId !== uuid){
                            el('node_actions').style.display = 'none';   
                            el('node_actions').style.display = 'block';
                            selectedNode = true;                          
                            isNodeAction.current = true;
                        }else{                        
                            selectedNode = !selectedNode;                            
                            isNodeAction.current = selectedNode;                             
                        }
                        tempUUId = uuid;                                                                      
                    }

                    
                }

                if(nodeInfo?.title === "Category"){
                    console.log(nodeInfo);
                    el('node_actions').style.display = 'none';   
                    let categoryType = nodeInfo?.label.split('-')[1].replace('\n', '');
                    categoryType = categoryType.replaceAll('\n', ' ');
                    // console.log(categoryType);similarComplete
                    localStorage.removeItem('renderComplete')
                    localStorage.removeItem('similarComplete')
                    
                    const q = `MATCH (o:Organizations) WHERE o.category_groups_list CONTAINS '${categoryType}' Return o LIMIT 15`;
                    // console.log(q);
                    vis.renderWithCypher(q);
                    setToLoadGraph(true);                       
                    localStorage.removeItem('gParentNodes');
                    localStorage.setItem('viewSimilarNodes', true);
                }           
                                                 
            })  
            
            // vis['_network'].on('beforeDrawing', (ctx)=> {
            //     // console.log(ctx);
            //     // const empCount = vis["_nodes"][nodeIds[count]]?.raw?.properties?.employee_count;
            //     ctx.globalCompositeOperation = "lighter";
            //     ctx.globalAlpha = 0.8;
            //     ctx.save();
            //     // if(empCount === 'unknown'){
            //     // }else{
            //     //     ctx.restore();
            //     // }
            //     // count++;                
            // }) 

            vis['_network'].on('doubleClick', (event)=> {
                // console.log(event);
                console.log(vis["_nodes"][event.nodes[0]])
                const uuid = vis["_nodes"][event.nodes[0]].raw.properties.uuid;
                const node_label = vis["_nodes"][event.nodes[0]]?.raw?.labels[0];
                const remaining_query = getQuery(node_label);
                let queryCondition = ``;

                if(['Organizations', "FundingRounds", "People", 'Investments', 'Industries'].includes(node_label)){
                    queryCondition = `{uuid: '${uuid}'}`
                }
                if(['NoOfEmployees'].includes(node_label)){
                    queryCondition = `{name: '${vis["_nodes"][event.nodes[0]].raw.properties.name}'}`
                }
                if(["Category"].includes(node_label)){
                    queryCondition = `{catName: '${vis["_nodes"][event.nodes[0]].raw.properties.catName}'}`
                }
                if(['Acquisitions'].includes(node_label)){
                    queryCondition = `{acquiree_uuid: '${vis["_nodes"][event.nodes[0]].raw.properties.acquiree_uuid}'}`
                }

                const q = `MATCH (p:${node_label} ${queryCondition})
                ${remaining_query}`;
               
                console.log(q);
                // ['gParentNodes', 'viewOnlyMode', 'viewSimilarNodes', 'renderComplete', 'similarComplete'].forEach(item => localStorage.removeItem(item));
                vis.updateWithCypher(q)
            })

        })           
        
        
        vis.render();                                     

        return () => {
            ['gParentNodes', 'viewOnlyMode', 'viewSimilarNodes', 'renderComplete', 'similarComplete'].forEach(item => localStorage.removeItem(item));
        }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    
    const updateCanvasCordinates = (event) => {
        const canvas = document.querySelector('.vis-network > canvas');
        if (!canvas) return; // It should always find the canvas.
        const rect = canvas.getBoundingClientRect();
        let x = event?.event?.srcEvent?.clientX - rect.left;
        let y = event?.event?.srcEvent?.clientY - rect.top;
        x = x + 28;
        y = y - 105;
        // x = x + 50;
        // y = y - 8;
        
        setNodeActionPos({
            ...nodeActionPos,
            x,
            y
        })   
    }   

    const onNodeAction = (action_type) => {
        if(viz["_nodes"][clickedNode]){
            const uuid = viz["_nodes"][clickedNode].raw.properties.uuid;
            const node_label = viz["_nodes"][clickedNode]?.raw?.labels[0];
            let grandParentNodes = JSON.parse(localStorage.getItem("gParentNodes"));
            // console.log(grandParentNodes)
            if(action_type !== 'collapse'){
                if(['Organizations', 'FundingRounds', 'Investments'].includes(node_label)){
                    if(!grandParentNodes.includes(clickedNode)){
                        grandParentNodes.push(clickedNode);                            
                        localStorage.setItem('gParentNodes', JSON.stringify(grandParentNodes))
                        setTimeout(() => {
                            addPropertyNodeTo(clickedNode);
                        }, 1000);
                    }
                    const queryRelations = relations ? `r:${relations}` : 'r'
                    const q1 = `MATCH (n:${node_label} {uuid: '${uuid}'})-[${queryRelations}]-(b) RETURN r, n, b LIMIT 20`;
                    console.log(q1);
                    viz.updateWithCypher(q1)
                }
            }else{
                
                // const collpse_back = getQueryForAction(action_type, node_label, uuid);
                // console.log(collpse_back)
                // viz.updateWithCypher(collpse_back)  
                
                // console.log(clickedNode); 
                // console.log(viz['_network'].getSelection());                                             
                // console.log("all node",viz['_network'].getConnectedNodes(clickedNode));
                // console.log("parent nodes", viz['_network'].getConnectedNodes(clickedNode, 'from'));
                // console.log("child nodes", viz['_network'].getConnectedNodes(clickedNode, 'to'));
                
                collapseNodes(clickedNode);
                const index = grandParentNodes.indexOf(clickedNode);
                grandParentNodes.splice(index, 1)
                localStorage.setItem('gParentNodes', JSON.stringify(grandParentNodes))
                
            }
            // setUUID(uuid);
            // setIsNodeAction(false);
            isNodeAction.current = false; 
            el('node_actions').style.display = 'none';
        }
    }

    const collapseNodes = (nodeId) => {
        const subNodes = getAllSubNodes(nodeId);
        // console.log(subNodes);
        if(subNodes.length > 1){
            subNodes.forEach((subNode) => {
                if(subNode !== nodeId){
                    let subChildNodes = getAllSubNodes(subNode);
                    // console.log("subsubNodes", subChildNodes);
                    if(subChildNodes.includes(nodeId)){
                        const index = subChildNodes.indexOf(nodeId);
                        subChildNodes.splice(index, 1);
                    }
                    if(subChildNodes.length > 0){
                        // console.log("recall to remove sub sub child nodes...")
                        collapseNodes(subNode);
                    }else{
                        // console.log('will remove node else...')
                        viz.nodes.remove({id: subNode})
                    }
                    // console.log('will remove node inside for...');
                    viz.nodes.remove({id: subNode})
                }
            })
        }else{
            viz.nodes.remove({id: subNodes[0]})
        }
    }

    const getAllSubNodes = (parentNodeId) =>{
        let subNodes = [], allConnectedNodes;
        // const node_label = viz["_nodes"][parentNodeId]?.raw?.labels[0];
        const grandParentNodes = localStorage.getItem("gParentNodes");
        allConnectedNodes = viz['_network'].getConnectedNodes(parentNodeId);
                
        allConnectedNodes.forEach((item) => {
            // if(item !== clickedNode) subNodes.push(item);
            if(!grandParentNodes.includes(item)) subNodes.push(item);
        })        

        return subNodes;
    }

    const viewOnlySelectedNode = () => {        
        el('node_actions').style.display = 'none';
        // console.log(selectedNode)
        if(["Organizations"].includes(selectedNode.labels[0])){
            setToLoadGraph(true);           
            // localStorage.removeItem('gParentNodes');
            ['gParentNodes', 'viewSimilarNodes', 'renderComplete', 'similarComplete'].forEach(item => localStorage.removeItem(item));
            localStorage.setItem('viewOnlyMode', true);
            const uuid = selectedNode?.properties?.uuid || null;
            // const query = `MATCH (o:Organizations {uuid: '${uuid}'})-[r:INVESTED_INTO]-(child)
            //     MATCH (f:FundingRounds)-[fr:FUNDINGROUND_OF]->(child)<-[pr:PART_OF]-(p:People)
            //     RETURN r, o, child, fr, f, p, pr LIMIT 50`;
            const query = `MATCH (o:Organizations {uuid: '${uuid}'}) RETURN o`;
            viz.renderWithCypher(query);
        }else{
            notifyInfo('Only orgnasations can be viewed!');
        }
    }
    
    const getNodeRawInfo = (nodeId) =>{ 
        const rawInfo = viz["_nodes"][nodeId]?.raw;
        // console.log(rawInfo);
        if(rawInfo?.labels[0] === 'Organizations'){
            // const empCount = rawInfo?.properties?.employee_count === 'unknown' ? 'N/A' : rawInfo?.properties?.employee_count 
            let orgNodes = [
                // {value: empCount, key: 'Employee Count', color: '#c14e34'},
                {value: rawInfo?.properties?.primary_role || 'N/A', key: 'Role', color: '#c14e34'},
                {value: rawInfo?.properties?.city, key: 'City', color: '#c14e34'},            
            ];
            const categories = rawInfo?.properties?.categories || []
            categories.forEach(category => orgNodes.push({
                value: category.replaceAll(' ', '\n'), key: 'Category', color: '#c14e34'
            }))
            return orgNodes;
        }
       
        if(rawInfo?.labels[0] === 'FundingRounds'){
            let raisedAmount = rawInfo?.properties?.raised_amount;
            const currentSymbol = CURRENCY_SYMBOLS[rawInfo?.properties?.raised_amount_currency_code] || '';

            if(raisedAmount) raisedAmount = getFormatedCurrency(raisedAmount)
            else raisedAmount = 'N/A';

            let fundNodes = [
                {value: rawInfo?.properties?.announced_on, key: 'Date', color: '#33a2b4'},
                {value: rawInfo?.properties?.city || 'N/A', key: 'City', color: '#33a2b4'},
                {value: parseInt(rawInfo?.properties?.investor_count), key: 'Number of\nInvestors', color: '#33a2b4'},                
                {
                    value: `${raisedAmount} ${currentSymbol}`,
                    key: 'Money Raised',
                    color: '#33a2b4'
                },
                {
                    value: `${raisedAmount} ${currentSymbol}`,
                    key: 'Money Raised\n(USD)',
                    color: '#33a2b4'
                }
            ];
            return fundNodes;
        }
        return [];
    }

    const addPropertyNodeTo = (nodeIdToAdd) => {
        const nodeInfo = getNodeRawInfo(nodeIdToAdd);
        nodeInfo.forEach((item) => {
            const newNode = viz.nodes.add({
                borderWidthSelected: 2,  
                title: item.key,                                                                        
                label: `${item.key}-\n${item.value}`,
                scaling: {
                    label: true
                },
                size: 50,
                shape: 'circle',
                // opacity: 0.3,
                font: {
                    size: 16,
                    color:"#ffffff",
                    strokeWidth: 0,
                    strokeColor: 'transparent',
                    multi: false,
                    bold: {
                        color: '#ffffff',
                        size: 16, // px
                        face: 'arial',
                        vadjust: 5,
                        mod: 'bold'
                    }
                },  
                // icon: {
                //     face: 'FontAwesome',
                //     code: '\uf1ad',
                //     // weight: 500,
                //     size: 30,  //50,
                //     color:'#fff'
                //   },
                color: {
                    border: '#fff',
                    background:item.color,
                    highlight: {
                        border: '#fff',
                        background:item.color,
                    },
                    hover: {
                        border: '#fff',
                        background:item.color,
                    }
                },
                margin: 10
                // value: 35                   
            })
            viz.edges.add({
                from: newNode[0],
                to: nodeIdToAdd,
                scaling:{
                    label: true,
                },
                smooth: true,
                color: '#d3d3d3',
                width: 2,
                hoverWidth: 4
            })
            
        })
    }

    const createRingAroundNode = (viz_instance) => {
        const similarNodes = JSON.parse(localStorage.getItem('gParentNodes'));
        const length = similarNodes.length;
        if(length > 2){
            similarNodes.forEach((node, index) => {
                viz_instance.edges.add({
                    from: node,
                    to: (index === length - 1) ? similarNodes[0] : similarNodes[index + 1],
                    scaling:{
                        label: true,
                    },
                    hoverWidth: 7,
                    width: 5,
                    smooth: true,
                    color: '#f34423'
                })            
            })
        }
        if(length === 2){
            viz_instance.edges.add({
                from: similarNodes[0],
                to: similarNodes[1],
                scaling:{
                    label: true,
                },
                hoverWidth: 7,
                width: 5,
                smooth: true,
                color: '#f34423'
            }) 
        }
    }

    const onFilter = (date1, date2) => {
        // let queryRelations = relations ? `r:${relations}` : 'r'
        setToLoadGraph(true);
        viz.renderWithCypher(`MATCH (p)-[r:INVESTED_INTO]->(o:Organizations) 
        WHERE '${date1}'>=o.founded_on<='${date2}' RETURN p,o,r LIMIT 100`)       
        el('node_actions').style.display = 'none';            
        // toggleDrawer(false)
    }        
    
    const openSidePanel = () => {
        if(["Organizations"].includes(selectedNode.labels[0])){
            el('node_actions').style.display = 'none';
            setSidenavToggle(true);
        }else{
            notifyInfo('Only orgnasations can be explored!');
        }
    }    

    const refreshGraph = () => {
        setToLoadGraph(true);
        setHasGraphRecords(true);  
        setToggleOptionsNav(false);
        setAllowedOptions([0,1,2,3]);        
        setQueryRelations('PART_OF|INVESTED_INTO|INVESTMENT_OF|FUNDINGROUND_OF|NO_OF_EMPLOYEES');     
        ['gParentNodes', 'viewOnlyMode', 'viewSimilarNodes', 'renderComplete', 'similarComplete'].forEach(item => localStorage.removeItem(item));
        viz.renderWithCypher(INICIAL_CYPHER)
    }

    const expandNodeFull = () => {
        const grand_parentNode = viz.nodes.getIds();
        grand_parentNode.forEach((node) => {
            addPropertyNodeTo(node);
        })
    }

    const updateOptions = (query, options, relations) => {
        setToLoadGraph(true);
        setAllowedOptions(options);
        setToggleOptionsNav(false);
        setQueryRelations(relations);
        viz.renderWithCypher(query);
    }

    const onQuickOption = (relationType) => {
        el('node_actions').style.display = 'none';
        const quickRelation = `INVESTED_INTO|${relationType}`;
        setToLoadGraph(true);
        setQueryRelations(quickRelation);
        console.log(`MATCH (b)-[r:${quickRelation}]->(o:Organizations) RETURN b,o,r LIMIT 100`);
        viz.renderWithCypher(`MATCH (b)-[r:${quickRelation}]->(o:Organizations) RETURN b,o,r LIMIT 100`)
    }

    const toggleDrawer = (styleValue) => {
        el('timeline__filter').style.display = styleValue;
    }

    const showSimilarity = (org_name) => { 
        ['gParentNodes', 'viewSimilarNodes', 'renderComplete', 'similarComplete'].forEach(item => localStorage.removeItem(item));        
        setSidenavToggle(false);        
        setToLoadGraph(true);   
        const q = `
        MATCH (o1:Organizations {name: '${org_name}'})-[:BELONGS_TO]->(cat1)
        WITH o1, collect(id(cat1)) AS cat1_new
        MATCH (o2:Organizations)-[:BELONGS_TO]->(cat2) WHERE o1 <> o2
        WITH o1 as from, cat1_new, o2 as to, collect(id(cat2)) AS cat2_new, o2.cb_url AS url		
        WITH gds.alpha.similarity.jaccard(cat1_new, cat2_new) AS similarity,from,to,url
        RETURN from,to,url, similarity,
        apoc.create.vRelationship(from,'SIMILARITY',{count:count(to), similarityCount:similarity},to) as rel
        ORDER BY similarity DESC LIMIT 100
        `;
        viz.renderWithCypher(q);
    }

    return (
        <div className="d-flex flex-column px-5 position-relative">           
            <ActionBar openFilterDrawer={toggleDrawer} onRefresh={refreshGraph} 
            openOptionsDrawer={setToggleOptionsNav} quickOptionUpdate={onQuickOption} />                    
            <Grid container spacing={4}  className="align-items-start mb-0">               
                <Grid item xs={12} sm={12} md={12} className="material-grid position-relative px-0" style={{overflow: 'hidden'}}>
                    {
                        loadingGraph && (
                            <div class="g_loader">
                                <img src={GLoader} alt="laoder" />
                            </div>
                        )
                    }
                    <div id="viz" className=""></div>
                    {
                        toggleSideNav && (
                            <NeoSideBar neoConfig={config} sidebarWidth={toggleSideNav ? 450 : 0}
                            node={selectedNode} selectedNodeId={clickedNode} onShowSimilarity={showSimilarity} closeSideBar={setSidenavToggle} />
                        )
                    }
                    {
                        toggleOptionNav && (
                            <NeoOptions sidebarWidth={toggleOptionNav ? 350 : 0} defaultOptions={allowedOptions}
                            closeSideBar={setToggleOptionsNav} onOptionChange={updateOptions} />
                        )
                    }
                    <TimelineSlider onClose={toggleDrawer} onSlideChange={onFilter} />   
                </Grid>  
            </Grid>
            <DoughnutOptions nodePosX={nodeActionPos.x} nodePosY={nodeActionPos.y}
            onNodeActionChange={onNodeAction}
            openExplorePanel={openSidePanel} onViewOnly={viewOnlySelectedNode}
            />       
            
            { !hasGraphRecords && <div className="no__graph text-center">
                <AccountTreeOutlinedIcon style={{
                    color: 'white', fontSize: 50
                }} />
                <div>No records found</div>
            </div> }
            {/* <GiantMenu /> */}
            <button id="hid_exp" className="d-none" onClick={() => expandNodeFull()}>Expand Node fully</button>
        </div>
    );
}

export default NeoVisTest;