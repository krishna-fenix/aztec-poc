import React from 'react';
import './d3-neo.css';
// import * as d3 from 'd3';
import { getGraph } from './neo4j-api';

const D3Neo = (props) => {

    const renderD3Graph = () => {
        
        const width = 800, height = 800;
        const d3 = window.d3;       
        var color = d3.scaleOrdinal(d3.schemeCategory10);
        
        // const svg = d3.select("#d3_neo").append("svg")
        //     .attr("width", "100%").attr("height", "100%")
        //     .attr("pointer-events", "all");                
  
        getGraph().then(graph => {

            // const force = d3.forceSimulation()        
            // .force("charge",  d3.forceManyBody())
            // .force("link", 30)

            var label = {
                'nodes': [],
                'links': []
            };
            
            graph.nodes.forEach(function(d, i) {
                label.nodes.push({node: d});
                label.nodes.push({node: d});
                label.links.push({
                    source: i * 2,
                    target: i * 2 + 1
                });
            });
            
            var labelLayout = d3.forceSimulation(graph.nodes)
                .force("charge", d3.forceManyBody().strength(-50))
                .force("link", d3.forceLink(graph.links).id(function(d) {return d.id; }).distance(0).strength(2));

            var graphLayout = d3.forceSimulation(graph.nodes)
                .force("charge", d3.forceManyBody().strength(-3000))
                .force("center", d3.forceCenter(width / 2, height / 2))
                .force("x", d3.forceX(width / 2).strength(1))
                .force("y", d3.forceY(height / 2).strength(1))
                .force("link", d3.forceLink(graph.links).id(function(d) {return d.id; }).distance(50).strength(1))
                .on("tick", ticked);

            var adjlist = [];

            graph.links.forEach(function(d) {
                adjlist[d.source.index + "-" + d.target.index] = true;
                adjlist[d.target.index + "-" + d.source.index] = true;
            });
            
            function neigh(a, b) {
                return a === b || adjlist[a + "-" + b];
            }

            var svg = d3.select("#viz").attr("width", width).attr("height", height);
            var container = svg.append("g");


            svg.call(
                d3.zoom()
                    .scaleExtent([.1, 4])
                    .on("zoom", function() { container.attr("transform", d3.event.transform); })
            );
            
            var link = container.append("g").attr("class", "links")
                .selectAll("line")
                .data(graph.links)
                .enter()
                .append("line")
                .attr("stroke", "#aaa")
                .attr("stroke-width", "1px");
            
            var node = container.append("g").attr("class", "nodes")
                .selectAll("g")
                .data(graph.nodes)
                .enter()
                .append("circle")
                .attr("r", 5)
                .attr("fill", function(d) { return "#1a94db"; })
                // +Math.floor(Math.random()*16777215).toString(16)
            node.on("mouseover", focus).on("mouseout", unfocus);
            
            node.call(
                d3.drag()
                    .on("start", dragstarted)
                    .on("drag", dragged)
                    .on("end", dragended)
            );

            var labelNode = container.append("g").attr("class", "labelNodes")
                .selectAll("text")
                .data(graph.nodes)
                .enter()
                .append("text")
                .text(function(d, i) { return i % 2 === 0 ? "" : d.title; })
                .style("fill", "#555")
                .style("font-family", "Arial")
                .style("font-size", 12)
                .style("pointer-events", "all"); // to prevent mouseover/drag capture
            
            node.on("mouseover", focus).on("mouseout", unfocus);
            
            function ticked() {
            
                node.call(updateNode);
                link.call(updateLink);
            
                labelLayout.alphaTarget(0.3).restart();
                
                labelNode.each(function(d_n, i) {
                    
                    if(i % 2 === 0) {
                        d_n.x = d_n.x;
                        d_n.y = d_n.y;
                    } else {
                        var b = this.getBBox();
            
                        var diffX = d_n.x - d_n.x;
                        var diffY = d_n.y - d_n.y;
            
                        var dist = Math.sqrt(diffX * diffX + diffY * diffY);
            
                        var shiftX = 16; // b.width * (diffX - dist) / (dist * 2);
                        shiftX = Math.max(-b.width, Math.min(0, shiftX));
                        var shiftY = 16;
                        this.setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
                    }
                });
                labelNode.call(updateNode);
            
            }
            
            function fixna(x) {
                if (isFinite(x)) return x;
                return 0;
            }
            
            function focus(d) {
                var index = d3.select(d3.event.target).datum().index;
                node.style("opacity", function(o) {
                    return neigh(index, o.index) ? 1 : 0.1;
                });
                labelNode.attr("display", function(o) {
                  return neigh(index, o.index) ? "block": "none";
                });
                link.style("opacity", function(o) {
                    return o.source.index === index || o.target.index === index ? 1 : 0.1;
                });
            }
            
            function unfocus() {
               labelNode.attr("display", "block");
               node.style("opacity", 1);
               link.style("opacity", 1);
            }
            
            function updateLink(link) {
                link.attr("x1", function(d) { return fixna(d.source.x); })
                    .attr("y1", function(d) { return fixna(d.source.y); })
                    .attr("x2", function(d) { return fixna(d.target.x); })
                    .attr("y2", function(d) { return fixna(d.target.y); });
            }
            
            function updateNode(node) {
                node.attr("transform", function(d) {
                    return "translate(" + fixna(d.x) + "," + fixna(d.y) + ")";
                });
            }
            
            function dragstarted(d) {
                d3.event.sourceEvent.stopPropagation();
                if (!d3.event.active) graphLayout.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
            }
            
            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }
            
            function dragended(d) {
                if (!d3.event.active) graphLayout.alphaTarget(0);
                d.fx = null;
                d.fy = null;
            }

            // force.nodes(graph.nodes)
           
            // d3.forceLink().links(graph.links)
            // .distance(30).id(function(d) { return d.name; })

            // var drag = d3.drag().on('drag', function() {
            //     // dragMove(this, obj, 'points')                
            // });
            
            // const link = svg.selectAll(".link")
            //     .data(graph.links).enter()
            //     .append("line").attr("class", "link");

            // const node = svg.selectAll(".node")
            //     .data(graph.nodes).enter()
            //     .append("circle")
            //     .attr("class", d => {
            //         return "node " + d.label
            //     })
            //     .attr("r", 10)
            //     .call(drag);

            // // html title attribute
            // node.append("title")
            //     .text(d => {
            //         return d.title;
            //     });

            // // force feed algo ticks
            // force.on("tick", () => {
            //     link.attr("x1", d => {
<<<<<<< HEAD
            //         // console.log(d)
=======
            
>>>>>>> fbbc33c42f27b15166d38389ccf16bdf1d68e325
            //         return Math.floor(Math.random() * 100);
            //     }).attr("y1", d => {
            //         return Math.floor(Math.random() * 100);
            //     }).attr("x2", d => {
            //         return Math.floor(Math.random() * 100);
            //     }).attr("y2", d => {
            //         return Math.floor(Math.random() * 100);
            //     });

            //     node.attr("cx", d => {
            //         return d.x;
            //     }).attr("cy", d => {
            //         return d.y;
            //     });
            // });
        });
    }

    React.useEffect(() => { 
        renderD3Graph()   
        // getGraph().then(graph => {
<<<<<<< HEAD
        //     console.log(graph);
=======
        
>>>>>>> fbbc33c42f27b15166d38389ccf16bdf1d68e325
        // })
    }, [])

    return (
        <div id="d3_neo">
            <svg id='viz'></svg>
        </div>
    );
}

export default D3Neo;