import React, { useEffect, useState } from 'react'

import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import ic_full_screen from '../../assets/images/ic_full_screen.svg';
import ic_download_white from '../../assets/images/ic_download_white.svg';
import { Image } from 'semantic-ui-react'
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import './styles.css';
import { exportTableState, saveTableState } from '../../services/common_services';

const Table = () => {
    const rowData = [
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 143.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 123, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },];

    // eslint-disable-next-line no-unused-vars
    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumnApi] = useState(null);

    const [selected, setSelected] = useState(false)
    const [style, setStyle] = useState({
        height: '85vh',
        width: '90%'
    });

    const [topStyle, setTopStyle] = useState({
        height: 'auto',
        width: '90%',
        position: 'relative'
    })

    const rowStyle = { background: 'rgba(0, 0, 0, 0.1)', color: "#fff" };

    const getRowStyle = params => {
        if (params.node.rowIndex % 2 !== 0) {
            return { background: 'rgba(255, 255, 255, 0.1)' };
        }
    };

    const dynamicCellStyleRevenue = params => {
        if (params.value === 123) {
            return {
                background: "linear-gradient(to right,rgba(14, 182, 121, 0.3), rgba(0, 0, 0, 0.1))"
            }
        }
        return null;
    };

    const dynamicCellStyleOperationEx = params => {
        if (params.value === 143.295) {
            return {
                background: "linear-gradient(to right,rgba(91, 53, 80, 0.8),rgba(91, 53, 80, 0.8), rgba(0, 0, 0, 0.1))"
            }
        }
        return null;
    };

    const onGridReady = (params) => {
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    };

    const saveState = () => {
        window.colState = gridColumnApi.getColumnState();
        saveTableState(window.colState)
    };

    const restoreState = () => {
        if (!exportTableState()) {
            return;
        }
        gridColumnApi.applyColumnState({
            state: exportTableState(),
            applyOrder: true,
        });
    };

    const fillLarge = () => {
        setWidthAndHeight('100%', '85vh');
    };

    const fillExact = () => {
        setWidthAndHeight('90%', '85vh');

    };

    const setWidthAndHeight = (width, height) => {
        setStyle({
            width,
            height
        });
        setTopStyle({
            width,
            position: 'relative'
        });
    };

    useEffect(() => {
        if (gridColumnApi != null)
            restoreState()

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [gridColumnApi]);

    return (
        <div id="grad">

            <div style={topStyle}>

                <div style={{ display: 'flex', flexDirection: 'row', padding: 5, position: 'absolute', right: 0 }}>

                    <div onClick={() => saveState()}>

                        <Image src={ic_download_white} size="small" style={{
                            height: 20,
                            width: 25,
                        }} />

                    </div>

                    <div onClick={() => {
                        if (selected) {
                            fillExact()
                            setSelected(false)
                        }
                        else {
                            fillLarge()
                            setSelected(true)
                        }
                    }}>
                        <Image src={ic_full_screen} size="small" style={{
                            height: 20,
                            width: 25,
                        }}
                        />

                    </div>

                </div>

            </div>

            <div className="ag-theme-alpine Back" style={style} >

                <AgGridReact
                    rowStyle={rowStyle}
                    getRowStyle={getRowStyle}
                    rowData={rowData}
                    rowSelection="multiple"
                    suppressRowClickSelection={true}
                    defaultColDef={{
                        editable: true,
                        sortable: true,
                        minWidth: 100,
                        filter: true,
                        resizable: true,
                        // floatingFilter: true,
                        flex: 1,
                    }}
                    components={{
                        rowNodeIdRenderer: function (params) {
                            return params.node.id + 1;
                        },
                    }}
                    sideBar={{
                        toolPanels: ['columns', 'filters'],
                        defaultToolPanel: '',
                    }}
                    pagination={true}

                    paginationPageSize={500}
                    onGridReady={onGridReady}
                    enableCharts={true}
                    enableRangeSelection={true}
                    rowDragManaged={true}
                    animateRows={true}>

                    <AgGridColumn field="Quarter" headerName="Quarter" enablePivot={true} enableRowGroup={true} minWidth={170} />

                    <AgGridColumn field="Revenue" cellStyle={dynamicCellStyleRevenue} enablePivot={true} enableValue={true} enableRowGroup={true} minWidth={150} />

                    <AgGridColumn field="NetIncome" headerName="Net Income" enableValue={true} enablePivot={true} enableRowGroup={true} />

                    <AgGridColumn
                        field="OperationIncome"
                        headerName="Operation Income"
                        enableValue={true}
                        columnGroupShow="closed"
                        filter="agNumberColumnFilter"
                        width={120}
                        flex={0}                        
                        enablePivot={true}
                        enableRowGroup={true}
                    />

                    <AgGridColumn
                        field="OperationExpenses"
                        headerName="Operation Expenses"
                        enableValue={true}
                        columnGroupShow="open"
                        filter="agNumberColumnFilter"
                        width={100}
                        flex={0}
                        cellStyle={dynamicCellStyleOperationEx}                        
                        enablePivot={true}
                        enableRowGroup={true}
                    />

                    <AgGridColumn
                        field="R_D"
                        headerName="R&D"
                        enableValue={true}
                        columnGroupShow="open"
                        filter="agNumberColumnFilter"
                        width={100}
                        flex={0}                        
                        enablePivot={true}
                        enableRowGroup={true}
                    />

                </AgGridReact>

            </div>

        </div>
    )
}

export default Table;
