import React from 'react'
import { create, useTheme } from "@amcharts/amcharts4/core";
import { ColumnSeries, XYChart, ValueAxis, CategoryAxis, XYCursor } from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import './styles.css';
import Header from '../../../component/header/Header';
import { useEffect } from 'react/cjs/react.development';

const Chart = () => {
    
    useTheme(am4themes_animated);
    useTheme(am4themes_kelly);

    useEffect(() => {
        // chart XY

        var chartXY = create("chartdiv", XYChart);
        chartXY.marginRight = 400;

        chartXY.data = [
            { "country": "Lithuania", "research": 501.9, "marketing": 250, "sales": 199 },
            { "country": "Czech Republic", "research": 301.9, "marketing": 222, "sales": 251 },
            { "country": "Ireland", "research": 201.1, "marketing": 170, "sales": 199 },
            { "country": "Germany", "research": 165.8, "marketing": 122, "sales": 90 },
            { "country": "Australia", "research": 139.9, "marketing": 99, "sales": 252 },
            { "country": "Austria", "research": 128.3, "marketing": 85, "sales": 84 },
            { "country": "UK", "research": 99, "marketing": 93, "sales": 142 },
            { "country": "Belgium", "research": 60, "marketing": 50, "sales": 55 },
            { "country": "The Netherlands", "research": 50, "marketing": 42, "sales": 25 }
        ];

        // Create axes
        var categoryAxis = chartXY.xAxes.push(new CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.title.text = "Local country offices";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;


        var valueAxis = chartXY.yAxes.push(new ValueAxis());
        valueAxis.title.text = "Expenditure (M)";

        // Create series
        var series = chartXY.series.push(new ColumnSeries());
        series.dataFields.valueY = "research";
        series.dataFields.categoryX = "country";
        series.name = "Research";
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = true;

        var series2 = chartXY.series.push(new ColumnSeries());
        series2.dataFields.valueY = "marketing";
        series2.dataFields.categoryX = "country";
        series2.name = "Marketing";
        series2.tooltipText = "{name}: [bold]{valueY}[/]";
        series2.stacked = true;

        var series3 = chartXY.series.push(new ColumnSeries());
        series3.dataFields.valueY = "sales";
        series3.dataFields.categoryX = "country";
        series3.name = "Sales";
        series3.tooltipText = "{name}: [bold]{valueY}[/]";
        series3.stacked = true;

        // Add cursor
        chartXY.cursor = new XYCursor();
    });

    return (
        <div>
            <Header />
            <div className="containerT">
                <script src="//cdn.amcharts.com/lib/4/core.js"></script>
                <script src="//cdn.amcharts.com/lib/4/charts.js"></script>
                <script src="//cdn.amcharts.com/lib/4/themes/animated.js"></script>
                <script src="//cdn.amcharts.com/lib/4/themes/kelly.js"></script>
                <div id="chartdiv"></div>
            </div>
        </div>
    )
}

export default Chart
