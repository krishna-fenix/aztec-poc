import React ,{ useState} from 'react'
import { Button, Form, Segment } from 'semantic-ui-react';
import Header from '../../../component/header/Header';
import firebase from "../../../firebase/config";
import './styles.css';

const TempContainer = () => {
    const [isMobAdded, setIsMobAdded] = useState(false)
    const [otp, setOtp] = useState('')
    const [phone, setPhone] = useState('')
    const [verifyId, setVerifyID] = useState('')

    function getOTP() {
        var user = firebase.auth().currentUser;

        setIsMobAdded(true)

        if (!!user) {

            var recaptchaVerifier = new firebase.auth.RecaptchaVerifier("container");

            user.multiFactor.getSession().then(function (multiFactorSession) {
                var phoneT = "+91" + phone
                // Specify the phone number and pass the MFA session.
                var phoneInfoOptions = {
                    phoneNumber: phoneT,
                    session: multiFactorSession
                };
                var phoneAuthProvider = new firebase.auth.PhoneAuthProvider();
                // Send SMS verification code.
                return phoneAuthProvider.verifyPhoneNumber(phoneInfoOptions, recaptchaVerifier)                    
            }).then(function (verificationId) {
                setVerifyID(verificationId)
                // var cred = firebase.auth.PhoneAuthProvider.credential(verificationId, otp);
                // var multiFactorAssertion = firebase.auth.PhoneMultiFactorGenerator.assertion(cred);
                // // Complete enrollment.
                // return user.multiFactor.enroll(multiFactorAssertion, "Krishna Vishwakarma");
            });
        }
    }

    const askForOTP = () => {

        var user = firebase.auth().currentUser;

        var cred = firebase.auth.PhoneAuthProvider.credential(verifyId, otp);
        var multiFactorAssertion = firebase.auth.PhoneMultiFactorGenerator.assertion(cred);
        // Complete enrollment.
        user.multiFactor.enroll(multiFactorAssertion, "Krishna Vishwakarma").then(response => {
            
        }).catch(error => {
            
        })
    }


    return (
        <div>
            <Header/>
            <Segment className="containerT">
                <h1>Temp</h1>
                <Form>
                    <Form.Field>
                        <label>Mobile Number</label>
                        <input placeholder='Mobile Number' input='number' onChange={(value) => 
                            setPhone(value.target.value)
                            } />
                    </Form.Field>

                    <Button
                        className="Button"
                        color="blue"
                        size="medium"
                        onClick={getOTP}
                        onKeyPress={getOTP}
                        style={{ marginTop: 50 }}>
                        Get OTP
                </Button>
                </Form>

                <div id="container" />

                {
                    isMobAdded &&
                    <div>
                        <Form>
                            <Form.Field>
                                <label>OTP</label>
                                <input placeholder='OTP' input='number' onChange={(value) =>
                                 setOtp(value.target.value)
                                } />
                            </Form.Field>
                            <button onClick={askForOTP}>Submit</button>
                        </Form>
                    </div>
                }

            </Segment>

        </div>
    )
}

export default TempContainer
