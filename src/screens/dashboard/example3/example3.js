import ic_app from '../../../assets/images/ic_apps.svg'
import ic_table_chart from '../../../assets/images/ic_table_chart.svg'
import ic_price from '../../../assets/images/ic_price_change.svg'
import ic_zoom from '../../../assets/images/ic_zoom_in.svg'
import ic_tv from '../../../assets/images/ic_tv.svg'
import ic_corporate from '../../../assets/images/ic_corporate_fare.svg'
import ic_tag from '../../../assets/images/ic_tag.svg'
import ic_notifications from '../../../assets/images/ic_notifications.svg'
import place_holder from '../../../assets/images/ic_place_holder.png'
import { Card, Icon, Image } from 'semantic-ui-react';
import './styles.css'
import { getUser } from '../../../services/common_services'
import RingPieChart from '../../../component/Graphs/Charts/pie-chart'
import AzTable from '../../../component/TableGrid/az-table'
import { INCOME_REPORT } from '../../../component/TableGrid/sample-table-data'
import { Tab } from 'semantic-ui-react'
import SideMenu from '../../../component/sidemenu/SideMenu.js'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

const Example3 = () => {

    const user = getUser()

    const panes = [
        {
            menuItem: 'Income Statement',
            //   render: () => <Tab.Pane attached={false}>Tab 1 Content</Tab.Pane>,
        },
        {
            menuItem: 'Cash flow',
            //   render: () => <Tab.Pane attached={false}>Tab 2 Content</Tab.Pane>,
        },
        {
            menuItem: 'Balance sheet',
            //   render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        },
    ]

    const TabExampleTabularFalse = () => (
        <Tab menu={{ attached: false, tabular: false }} panes={panes} />
    )

    return (
        <div id="back" className="d-flex" style={{ padding: 16 }}>

            <div className="card nav d-flex justify-content-between align-items-center fibo__side_nav pt-3 px-2">
                <div>
                    <div className="fibo_nav_option">
                        <Image className="fibo_nav_option_img justify-content-center" src={ic_app} />
                    </div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_table_chart} /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_price} /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_zoom} /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_corporate} /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_tv} /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_tag} /></div>
                </div>
                <div>
                    <hr />
                    <div className="fibo_nav_option"><Icon className="fibo_nav_option_img fibo_nav-icon justify-content-center" name="setting" /></div>
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img justify-content-center" src={ic_notifications} /></div>
                    <hr />
                    <div className="fibo_nav_option"><Image className="fibo_nav_option_img" src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} circular /></div>
                </div>

            </div>

            <div className="container-fluid px-4">

                <div className="row m-0">

                    <div className="row d-flex align-items-center">

                        <div className="card col-2 justify-content-center" style={{ width: "auto", backgroundColor: "#3fb6be" }}>
                            <h2 className="justify-content-center" style={{ color: "#fff" }}>Fibo Limited</h2>
                        </div>

                        <div className="w-31 justify-content-between fibo__header-tabs">

                            {/* <ul className="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 nav nav-pills card-header-pills">
                                <li className="nav-item ">
                                    <a className="nav-link"
                                        style={headSelected === CONST.INCOME ?
                                            { background: "#c2f0f0", margin: 4, padding: 6, border: "1px solid #7adddd" } :
                                            { background: "#fff", margin: 4, padding: 6 }}
                                        onClick={() => handleHeaderClick(CONST.INCOME)}
                                        href="#">
                                        <p className="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Income Statment</p>
                                    </a>
                                </li>

                                <li className="nav-item">
                                    <a className="nav-link"
                                        style={headSelected === CONST.BALANCE ?
                                            { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                            { background: "#fff", margin: 4, padding: 6 }}
                                        onClick={() => handleHeaderClick(CONST.BALANCE)}
                                        href="#">
                                        <p className="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Balance Sheet</p>
                                    </a>
                                </li>

                                <li className="nav-item ">
                                    <a className="nav-link"
                                        style={headSelected === CONST.CASH ?
                                            { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                            { background: "#fff", margin: 4, padding: 6 }}
                                        onClick={() => handleHeaderClick(CONST.CASH)}
                                        href="#">
                                        <p className="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Cash Flow</p>
                                    </a>
                                </li>

                            </ul> */}
                            <TabExampleTabularFalse />
                        </div>

                    </div>

                    <Card fluid />

                    <div className="d-flex justify-content-between flex-wrap p-0">
                        <div className="col-md-2">

                            <div className="card statiscal__content" style={{ borderRadius: 10 }}>
                                <div className="card-body row">

                                    <span className="fw-bolder statiscal_name">EBITDA</span>

                                    <div>
                                        <span className="align-top fw-bolder" style={{ fontSize: 40 }}>
                                            $2,812
                                            <span className="align-middle fs-3" style={{ fontWeight: 100 }}>k</span>
                                        </span>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div className="col-md-2">

                            <div className="card statiscal__content" style={{ borderRadius: 10 }}>
                                <div className="card-body row">

                                    <span className="fw-bolder statiscal_name">Cash Flow</span>

                                    <div >
                                        <span className="align-top fw-bolder" style={{ fontSize: 40 }}>
                                            $2,812
                                            <span className="align-middle fs-3" style={{ fontWeight: 100 }}>k</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-2">

                            <div className="card statiscal__content" style={{ borderRadius: 10 }}>
                                <div className="card-body row">

                                    <span className="fw-bolder statiscal_name">Margin</span>

                                    <div >
                                        <span className="align-top fw-bolder" style={{ fontSize: 40 }}>
                                            11.5
                                            <span className="align-middle fs-3" style={{ fontWeight: 100 }}>%</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-2">

                            <div className="card statiscal__content" style={{ borderRadius: 10 }}>
                                <div className="card-body row">

                                    <span className="fw-bolder statiscal_name">Growth Profit</span>

                                    <div >
                                        <span className="align-top fw-bolder" style={{ fontSize: 40 }}>
                                            $732
                                            <span className="align-middle fs-3" style={{ fontWeight: 100 }}>k</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div className="row m-0 mt-3 p-0 d-flex flex-wrap" style={{ marginTop: 20 }}>

                        <div className="col-md-4 d-flex flex-column justify-content-between ps-0">
                            <div className="card aztech__card py-4 px-3">
                                <p style={{ fontWeight: 'bold' }}>Revenue break down</p>
                                <RingPieChart id="pie_chart_1" />
                            </div>
                            <div className="card aztech__card py-4 px-3 mt-3">
                                <p style={{ fontWeight: 'bold' }}>EBITDA year to date</p>
                                <RingPieChart id="pie_chart_2" />
                            </div>
                        </div>

                        <div className="card aztech__card col-md-8 py-3 px-4 d-flex flex-column">
                            <p style={{ fontWeight: 600, fontSize: 16 }}>Income Statement Report</p>
                            <AzTable tableData={INCOME_REPORT} />
                        </div>


                    </div>

                </div>

            </div>

            <SideMenu style={{
                position: 'absolute',
                right: 0
            }} />

        </div>
    )
}


export default Example3