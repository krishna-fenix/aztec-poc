import React, { useEffect, useState } from 'react'
import Header from '../../../component/header/Header'
import './styles.css'
import { Card, Icon, Image } from 'semantic-ui-react';
import ic_insights from '../../../assets/images/ic_insights.png'
import ic_split from '../../../assets/images/ic_split.png'
import ic_trending from '../../../assets/images/ic_trending.png'
import ic_worksplaces from '../../../assets/images/ic_worksplaces.png'
import ic_people from '../../../assets/images/ic_people.png'
import ic_location from '../../../assets/images/ic_location.png'
import ic_bug from '../../../assets/images/ic_bug.png'
import { create, useTheme } from "@amcharts/amcharts4/core";
import { PieChart, PieSeries, ColumnSeries, XYChart, ValueAxis, CategoryAxis, XYCursor } from "@amcharts/amcharts4/charts";
import { Bar } from 'react-chartjs-2';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import { Tree, TreeNode } from 'react-organizational-chart';

const HomeContainer = ({headerHasBackground}) => {
    const CONST = {
        INCOME: "INCOME",
        BALANCE: "BALANCE",
        CASH: "CASH",
        REPORT: "REPORT",
        BENCHMAR: "BENCHMARK",
        FORECAST: "FORECAST",
        SIMULATION: "SIMULATION",
        QUARTER: "Quarter",
        YEAR: "YEAR"
    }

    useTheme(am4themes_animated);
    useTheme(am4themes_kelly);

    const [headSelected, setHeadSelected] = useState(CONST.INCOME)
    const [headSecondarySelected, setHeadSecondarySelected] = useState(CONST.REPORT)
    const [dataFilter, selectDataFilter] = useState(CONST.QUARTER)

    const list = [
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_insights
        },
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_split
        },
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_trending
        },
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_worksplaces
        },
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_people
        },
        {
            isActive: true,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_location
        },
        {
            isActive: false,
            title: "Production is dropping by -20% vs same time last year",
            isOpened: false,
            img: ic_bug
        }
    ]

    const dataBar = {
        labels: ['1Q15', '2Q15', '3Q15', '4Q15', '1Q16', '2Q26', '3Q16', '4Q16', '1Q17', '2Q17', '3Q17', '4Q17'],
        datasets: [
            {
                label: 'Operative Cash Flow',
                data: [2.5, 2.2, 2.1, 2.6, 2.5, 2.4, 2.2, 2.6, 2.5, 2.4, 2.2, 2.6],
                backgroundColor: [
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                ],
                borderColor: [
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                    'rgba(0, 185, 188, 1)',
                ],
                borderWidth: 1,
            },
            {
                label: 'Capital Expenditure',
                data: [-1.1, -1.3, -0.9, -1.3, -1.1, -1.2, -0.2, -1.2, -0.8, -0.1, -0.6, -0.5],
                backgroundColor: [
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                ],
                borderColor: [
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                    'rgba(0, 164, 177, 1)',
                ],
                borderWidth: 1,
            },
            {
                label: 'Free cash Flow',
                data: [1.5, 1.2, 1.3, 1.5, 1.4, 1.3, 1.4, 1.2, 1.3, 1.1, 1.6, 1.3, 1.4],
                backgroundColor: [
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                ],
                borderColor: [
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                    'rgba(0, 129, 140, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };

    const options = {
        responsive: true,
        maintainAspectRatio: true,
        animations: {
            tension: {
                duration: 1000,
                easing: 'linear',
                from: 1,
                to: 0,
                loop: true
            }
        },
        plugins: {
            legend: {
                display: true,
                position: "bottom"
            }
        },
        // maintainAspectRatio: false ,
        scales: {
            x: {
                grid: {
                    color: 'white',
                    borderColor: 'white',
                    tickColor: 'white'
                },
                ticks: {
                    color: "#16c1c3",
                }
            },
            y: {
                ticks: {
                    color: "#16c1c3",
                    // Include a dollar sign in the ticks
                    callback: function (value, index, values) {
                        return '$' + value + 'M';
                    }
                }
            },
            // yAxes: [
            //     {
            //         ticks: {
            //             beginAtZero: true,
            //         },
            //     },
            // ],
        },
    };

    useEffect(() => {
        var chart = create("chartdiv", PieChart);

        chart.data = [{ "country": "Lithuania", "litres": 501.9 },
        { "country": "Czech Republic", "litres": 301.9 },
        { "country": "Ireland", "litres": 201.1 },
        { "country": "Germany", "litres": 165.8 },
        { "country": "Australia", "litres": 139.9 },
        { "country": "Austria", "litres": 128.3 },
        { "country": "UK", "litres": 99 },
        { "country": "Belgium", "litres": 60 },
        { "country": "The Netherlands", "litres": 50 }];

        var pieSeries = chart.series.push(new PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";

        // chart XY

        var chartXY = create("chartdivXY", XYChart);
        chartXY.marginRight = 400;

        chartXY.data = [
            { "country": "Lithuania", "research": 501.9, "marketing": 250, "sales": 199 },
            { "country": "Czech Republic", "research": 301.9, "marketing": 222, "sales": 251 },
            { "country": "Ireland", "research": 201.1, "marketing": 170, "sales": 199 },
            { "country": "Germany", "research": 165.8, "marketing": 122, "sales": 90 },
            { "country": "Australia", "research": 139.9, "marketing": 99, "sales": 252 },
            { "country": "Austria", "research": 128.3, "marketing": 85, "sales": 84 },
            { "country": "UK", "research": 99, "marketing": 93, "sales": 142 },
            { "country": "Belgium", "research": 60, "marketing": 50, "sales": 55 },
            { "country": "The Netherlands", "research": 50, "marketing": 42, "sales": 25 }
        ];

        // Create axes
        var categoryAxis = chartXY.xAxes.push(new CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.title.text = "Local country offices";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;


        var valueAxis = chartXY.yAxes.push(new ValueAxis());
        valueAxis.title.text = "Expenditure (M)";

        // Create series
        var series = chartXY.series.push(new ColumnSeries());
        series.dataFields.valueY = "research";
        series.dataFields.categoryX = "country";
        series.name = "Research";
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = true;

        var series2 = chartXY.series.push(new ColumnSeries());
        series2.dataFields.valueY = "marketing";
        series2.dataFields.categoryX = "country";
        series2.name = "Marketing";
        series2.tooltipText = "{name}: [bold]{valueY}[/]";
        series2.stacked = true;

        var series3 = chartXY.series.push(new ColumnSeries());
        series3.dataFields.valueY = "sales";
        series3.dataFields.categoryX = "country";
        series3.name = "Sales";
        series3.tooltipText = "{name}: [bold]{valueY}[/]";
        series3.stacked = true;

        // Add cursor
        chartXY.cursor = new XYCursor();

        //Verticle Bar chart
    });

    const handleHeaderClick = (TYPE) => {
        setHeadSelected(TYPE)
    }

    const handleHeaderSecondaryClick = (TYPE) => {
        setHeadSecondarySelected(TYPE)
    }

    return (
        <div style={{ background: "#fdf9fd" }}>
            <Header hasBgColor={headerHasBackground} itemToBeActive='/myfunds'  />
            <div class="row">

                <div style={{ background: "#fff" }} class="col col-sm-12 col-md-12 col-lg-3 col-xl-2 col-xxl-2">

                    <div class='row' style={{ marginBottom: 20, padding: 24 }}>
                        <ic_hierarchy />
                        {/* <img src={require("../../../assets/images/hierarchy.svg")} />, */}
                        <p class="col col-sm col-md col-lg col-xl col-xxl fs-4" style={{ marginLeft: 12, fontWeight: 'normal' }}>Fund Hierarchy</p>
                    </div>

                    <Tree
                        lineWidth={'2px'}
                        lineColor={'green'}
                        lineBorderRadius={'4px'}
                        label={<div style={{ background: "#99c9bf" }} className="div">DK</div>}>

                        <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">WH</div>}>
                            <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">SM</div>} />
                        </TreeNode>

                        <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">MC</div>}>
                            <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">RK</div>}>
                                <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">JM</div>} />
                                <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">MB</div>} />
                            </TreeNode>
                        </TreeNode>

                        <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">EL</div>}>
                            <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">RL</div>} />
                            <TreeNode label={<div style={{ background: "#99c9bf" }} className="div">DW</div>} />
                        </TreeNode>

                    </Tree>
                    <div class="row row-sm row-md row-lg row-xl row-xxl" style={{ marginTop: 60, marginLeft: 12, marginBottom: 20 }} >
                        <Image class="col col-sm col-md col-lg col-xl col-xxl" src={ic_insights} style={{ height: 30, width: 50, marginRight: 25 }} />
                        <p class="col col-sm col-md col-lg col-xl col-xxl fs-4" style={{ fontWeight: 'normal', marginTop: 2 }}> Insight </p>
                    </div>

                    {
                        list.map((item, key) => (
                            <Card fluid style={{ marginTop: 2, marginLeft: 5, padding: 1 }}>
                                <div class="row row-sm row-md row-lg row-xl row-xxl" className="listClass" style={{ margin: 6 }}>
                                    {
                                        item.isActive && <Card class="col col-sm col-md col-lg col-xl col-xxl" className="centerItem" style={{ height: 10, width: 10, background: "#ff0024", marginRight: 6, marginTop: 6 }} />
                                    }
                                    <Image class="col col-sm col-md col-lg col-xl col-xxl" src={item.img} style={{ height: 20, width: 20 }} />
                                    <p className="fs-6 col col-sm col-md col-lg col-xl col-xxl" style={{ marginLeft: 4 }}>{item.title}</p>
                                    <Icon name="chevron down" />
                                </div>
                            </Card>
                        ))
                    }
                </div>

                <div class="col-sm-12 col-md-12 col-lg-8 col-xl col-xxl" style={{ marginLeft: 12, marginTop: 12, marginRight: 20, background: "#fff", padding: 24 }}>

                    <div>
                        <h1 class="row" style={{ paddingBottom: 32, paddingTop: 32, fontWeight: 'bold' }}>FIbo
                            <p class="col col-sm col-md col-lg col-xl col-xxl fs-3" style={{ fontWeight: 'normal', marginTop: 4 }}> Fibo Global Inc </p>
                        </h1>
                    </div>

                    <div class="row row-sm row-md row-lg row-xl row-xxl justify-content-between" style={{ marginBottom: 12 }}>

                        <ul class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 nav nav-pills card-header-pills">
                            <li class="nav-item ">
                                <button className="nav-link"
                                    style={headSelected === CONST.INCOME ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderClick(CONST.INCOME)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Income Statment</p>
                                </button>
                            </li>

                            <li class="nav-item">
                                <button className="nav-link"
                                    style={headSelected === CONST.BALANCE ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderClick(CONST.BALANCE)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Balance Sheet</p>
                                </button>
                            </li>

                            <li class="nav-item ">
                                <button className="nav-link"
                                    style={headSelected === CONST.CASH ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderClick(CONST.CASH)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Cash Flow</p>
                                </button>
                            </li>

                        </ul>

                        <ul class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 justify-content-start nav nav-pills card-header-pills">
                            <li class="nav-item ">
                                <button className="nav-link"
                                    style={headSecondarySelected === CONST.REPORT ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderSecondaryClick(CONST.REPORT)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Report</p>
                                </button>
                            </li>

                            <li class="nav-item">
                                <button className="nav-link"
                                    style={headSecondarySelected === CONST.BENCHMAR ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderSecondaryClick(CONST.BENCHMAR)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Benchmark</p>
                                </button>
                            </li>

                            <li class="nav-item ">
                                <button className="nav-link"
                                    style={headSecondarySelected === CONST.FORECAST ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderSecondaryClick(CONST.FORECAST)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Forecast</p>
                                </button>
                            </li>

                            <li class="nav-item ">
                                <button className="nav-link"
                                    style={headSecondarySelected === CONST.SIMULATION ?
                                        { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                        { background: "#fff", margin: 4, padding: 6 }}
                                    onClick={() => handleHeaderSecondaryClick(CONST.SIMULATION)}
                                    >
                                    <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Simulation</p>
                                </button>
                            </li>

                        </ul>
                    </div>

                    <div class="row row-sm row-md row-lg row-xl row-xxl justify-content-between" style={{ marginBottom: 12, marginTop: 100 }}>

                        <div class="col-6 col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                            <div class="row">

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60 }}>
                                        <span class="align-middle fs-6">$</span>
                                        73.5
                                        <span class="align-middle fs-6">M</span>
                                    </span>
                                </div>

                                <span class="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12 }}>+34 M (2.3%)</span>

                                <span class="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#16c1c3" }}>EBITA</span>
                            </div>

                        </div>

                        <div class="col-6 col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                            <div class="row">

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60 }}>
                                        <span class="align-middle fs-6">$</span>
                                        73.5
                                        <span class="align-middle fs-6">M</span>
                                    </span>
                                </div>

                                <span class="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12 }}>+34 M (2.3%)</span>

                                <span class="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#16c1c3" }}>Cash Flow</span>
                            </div>

                        </div>

                        <div class="col col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                            <div class="row">

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60 }}>
                                        24.7
                                        <span class="align-middle fs-6">%</span>
                                    </span>
                                </div>

                                <span class="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12 }}>+34 M (2.3%)</span>

                                <span class="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#16c1c3" }}>Margin</span>
                            </div>

                        </div>

                        <div class="row row-sm row-md row-lg row-xl row-xxl" style={{ marginTop: 60 }}>
                            <div class="col col-sm-12 col-md-12 col-lg-12 col-xl-6 col-xxl-6" >
                                <Bar data={dataBar} options={options} />
                            </div>

                            <div class="col col-sm-12 col-md-12 col-lg-12 col-xl-6 col-xxl-6" >
                                <div class="row row-sm row-md row-lg row-xl row-xxl justify-content-between" style={{ marginBottom: 12 }}>
                                    <p class="col col-sm col-md col-lg col-xl col-xxl fw-light fs-3">Income Statement</p>
                                    <ul class="col col-sm col-md col-lg col-xl col-xxl justify-content-end nav nav-pills card-header-pills">
                                        <li class="nav-item ">
                                            <button className="nav-link"
                                                style={dataFilter === CONST.QUARTER ?
                                                    { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                                    { background: "#fff", margin: 4, padding: 6 }}
                                                onClick={() => selectDataFilter(CONST.QUARTER)}
                                                >
                                                <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Quarter</p>
                                            </button>
                                        </li>

                                        <li class="nav-item">
                                            <button className="nav-link"
                                                style={dataFilter === CONST.YEAR ?
                                                    { background: "#c2f0f0", margin: 4, padding: 6, borderColor: "#7adddd", border: "1px solid #7adddd" } :
                                                    { background: "#fff", margin: 4, padding: 6 }}
                                                onClick={() => selectDataFilter(CONST.YEAR)}
                                                >
                                                <p class="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Year</p>
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">

                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Metric</th>
                                                    <th scope="col">1Q2021</th>
                                                    <th scope="col">2Q2021</th>
                                                    <th scope="col">3Q2021</th>
                                                    <th scope="col">4Q2021</th>
                                                    <th scope="col">1Q2020</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Total Revenue</th>
                                                    <td>$123.232</td>
                                                    <td>$125.232</td>
                                                    <td>$125.210</td>
                                                    <td>$103.132</td>
                                                    <td>$130.212</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Cost of Revenue</th>
                                                    <td>$123.232</td>
                                                    <td>$125.232</td>
                                                    <td>$125.210</td>
                                                    <td>$103.132</td>
                                                    <td>$130.212</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Growth profit</th>
                                                    <td>$123.232</td>
                                                    <td>$125.232</td>
                                                    <td>$125.210</td>
                                                    <td>$103.132</td>
                                                    <td>$130.212</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Operating expense</th>
                                                    <td>$123.232</td>
                                                    <td>$125.232</td>
                                                    <td>$125.210</td>
                                                    <td>$103.132</td>
                                                    <td>$130.212</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">R&D</th>
                                                    <td>$123.232</td>
                                                    <td>$125.232</td>
                                                    <td>$125.210</td>
                                                    <td>$103.132</td>
                                                    <td>$130.212</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    {/* <Grid columns={2} className="grid">
                        <Grid.Column >
                            <Card fluid raised>
                                <Card.Content>
                                    <Card.Header>Chart.Js Pollar graph</Card.Header>
                                    <Card.Content extra>
                                        <PolarArea data={data} />
                                    </Card.Content>
                                </Card.Content>
                            </Card>
                        </Grid.Column>

                        <Grid.Column>
                            <Card fluid raised>
                                <Card.Content>

                                    <Card.Header>amCharts4 Pie chart</Card.Header>
                                    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
                                    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
                                    <div id="chartdiv"></div>
                                </Card.Content>
                            </Card>

                        </Grid.Column>
                    </Grid>

                    <Grid columns={2} className="grid">

                        <div className="bottomContainer">
                            <Card fluid raised >
                                <Card.Content>

                                    <Card.Header>amCharts4 XYChart</Card.Header>
                                    <script src="//cdn.amcharts.com/lib/4/core.js"></script>
                                    <script src="//cdn.amcharts.com/lib/4/charts.js"></script>
                                    <script src="//cdn.amcharts.com/lib/4/themes/animated.js"></script>
                                    <script src="//cdn.amcharts.com/lib/4/themes/kelly.js"></script>
                                    <div id="chartdivXY"></div>
                                </Card.Content>
                            </Card>
                        </div>
                    </Grid> */}

                </div>

            </div>

        </div>
    )
}

export default HomeContainer

