import React from 'react'
import { Icon, Image } from 'semantic-ui-react'
import Header from '../../../component/header/Header'
import './styles.css'
import ic_hierarchy from '../../../assets/images/ic_hierarchy.png'
import { useState } from 'react'
// import { CircularAudioWave } from '../../../dist/circular-audio-wave.min.js'

const HomeContainer = ({headerHasBackground}) => {
    const CONST = {
        INCOME: "INCOME",
        BALANCE: "BALANCE",
        CASH: "CASH",
        REPORT: "REPORT",
        BENCHMAR: "BENCHMARK",
        FORECAST: "FORECAST",
        SIMULATION: "SIMULATION",
        QUARTER: "Quarter",
        YEAR: "YEAR"
    }

    const [headSelected, setHeadSelected] = useState(CONST.INCOME)
    const [dataFilter, selectDataFilter] = useState(CONST.QUARTER)

    const handleHeaderClick = (TYPE) => {
        setHeadSelected(TYPE)
    }

    return (
        <div id="grad">
            <div className="secondaryContainer">
                <Header hasBgColor={headerHasBackground} itemToBeActive='/fundraising' />

                <div className="row center mt-3">
                    <div className="col-2 row d-flex align-items-center">
                        <Icon inverted name="caret down" />
                        <Image src={ic_hierarchy} style={{ height: 25, width: 40 }} />
                        <ul className="col justify-content-start nav nav-pills card-header-pills">
                            <li className="nav-item ">
                                <button className="nav-link"
                                    style={{ background: "#04606c", margin: 4, padding: 6, borderColor: "#00757e", border: "1px solid #00757e", borderRadius: 8 }}
                                    onClick={() => handleHeaderClick(CONST.REPORT)}>
                                    <p className="fs-6" style={{ fontWeight: 'bold', color: "#000" }}>Fibo</p>
                                </button>
                            </li>
                        </ul>
                    </div>

                    <div className="col justify-content-center nav az_fraise_head">
                        <ul className="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 nav nav-pills card-header-pills">
                            <li className="nav-item" style={{ marginLeft: 20, marginRight: 20 }}>
                                <button className="nav-link"
                                    style={headSelected === CONST.INCOME ?
                                        { background: "#04606c", borderColor: "#00757e", margin: 4, padding: 6, border: "1px solid #00757e" } :
                                        { margin: 4, padding: 6 }}
                                    onbuttonlick={() => handleHeaderClick(CONST.INCOME)}
                                    >
                                    <p className="fs-6" style={{ fontWeight: 'normal', color: "#ccd2d5" }}>Income Statment</p>
                                </button>
                            </li>

                            <li className="nav-item" style={{ marginLeft: 20, marginRight: 20 }}>
                                <button className="nav-link"
                                    style={headSelected === CONST.BALANCE ?
                                        { background: "#04606c", borderColor: "#00757e", margin: 4, padding: 6, border: "1px solid #00757e" } :
                                        { margin: 4, padding: 6 }}
                                    onbuttonlick={() => handleHeaderClick(CONST.BALANCE)}
                                    >
                                    <p className="fs-6" style={{ fontWeight: 'normal', color: "#ccd2d5" }}>Balance Sheet</p>
                                </button>
                            </li>

                            <li className="nav-item" style={{ marginLeft: 20, marginRight: 20 }}>
                                <button className="nav-link"
                                    style={headSelected === CONST.CASH ?
                                        { background: "#04606c", borderColor: "#00757e", margin: 4, padding: 6, border: "1px solid #00757e" } :
                                        { margin: 4, padding: 6 }}
                                    onbuttonlick={() => handleHeaderClick(CONST.CASH)}
                                    >
                                    <p className="fs-6" style={{ fontWeight: 'normal', color: "#ccd2d5" }}>Cash Flow</p>
                                </button>
                            </li>

                        </ul>

                    </div>

                </div>

                <div style={{ marginTop: 20, fontWeight: 'bold', fontStyle: 'italic' }}>
                    <p className="justify-content-center nav" style={{ fontSize: 50, color: "#fff" }}>Fibo global is over performing</p>
                    <p className="justify-content-center nav" style={{ fontSize: 25, color: "#ccd2d5" }}>Cash flow injection we be needed in 18 months</p>
                </div>

                <div className="row row-sm row-md row-lg row-xl row-xxl justify-content-around" style={{ marginBottom: 60, marginTop: 100 }}>

                    <div className="col-6 col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                        <div className="row">

                            <div className="justify-content-center nav">
                                <span className="align-top " style={{ fontSize: 60, color: "#fff" }}>
                                    <span className="align-middle fs-6">$</span>
                                    73.5
                                    <span className="align-middle fs-6">M</span>
                                </span>
                            </div>

                            <span className="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12, color: "#fff" }}>+34 M (2.3%)</span>

                            <span className="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#009599" }}>EBITA</span>
                        </div>

                    </div>

                    <div className="col-6 col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                        <div className="row">

                            <div className="justify-content-center nav">
                                <span className="align-top " style={{ fontSize: 60, color: "#fff" }}>
                                    <span className="align-middle fs-6">$</span>
                                    1.2
                                    <span className="align-middle fs-6">M</span>
                                </span>
                            </div>

                            <span className="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12, color: "#fff" }}>+34 M (2.3%)</span>

                            <span className="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#009599" }}>Cash Flow</span>
                        </div>

                    </div>

                    <div className="col col-sm-6 col-md col-lg col-xl col-xxl justify-content-center nav ">

                        <div className="row">

                            <div className="justify-content-center nav">
                                <span className="align-top " style={{ fontSize: 60, color: "#fff" }}>
                                    24.7
                                    <span className="align-middle fs-6">%</span>
                                </span>
                            </div>

                            <span className="col col-sm col-md col-lg col-xl col-xxl fw-lighter justify-content-center nav" style={{ marginTop: 12, color: "#fff" }}>+34 M (2.3%)</span>

                            <span className="fw-lighter justify-content-center nav" style={{ marginTop: 12, fontSize: 25, color: "#009599" }}>Margin</span>
                        </div>

                    </div>
                </div>

                <div id="chart-container" style={{ width: "100%", height: "100%" }}></div>

                <div className="whiteBoarder">
                    <div className="center">
                        <div className="row">
                            <p className="justify-content-start" style={{ fontSize: 40, color: "#fdfdfc" }}>$234,399</p>
                            <div className="row">
                                <p className="col-sm-4 col-md-4 col-lg-8 col-xl-9 col-xxl-10 justify-content-start" style={{ fontSize: 20, color: "#fdfdfc" }}>Net income</p>
                                <ul className="col col-sm col-md col-lg col-xl col-xxl justify-content-start nav nav-pills card-header-pills az_fraise_head">
                                    <li className="nav-item ">
                                        <button className="nav-link"
                                            style={dataFilter === CONST.QUARTER ?
                                                { background: "#275e4e", borderColor: "#00757e", margin: 4, padding: 6, border: "2px solid #00757e" } :
                                                { margin: 4, padding: 6 }}
                                            onbuttonlick={() => selectDataFilter(CONST.QUARTER)}
                                            >
                                            <p className="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Quarter</p>
                                        </button>
                                    </li>

                                    <li className="nav-item">
                                        <button className="nav-link"
                                            style={dataFilter === CONST.YEAR ?
                                                { background: "#275e4e", borderColor: "#00757e", margin: 4, padding: 6, border: "2px solid #00757e" } :
                                                { margin: 4, padding: 6 }}
                                            onbuttonlick={() => selectDataFilter(CONST.YEAR)}
                                            >
                                            <p className="fs-6" style={{ fontWeight: 'normal', color: "#000" }}>Year</p>
                                        </button>
                                    </li>
                                </ul>
                            </div>

                            <div className="table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xxl">

                                <table className="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>Metric</th>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>1Q2021</th>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>2Q2021</th>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>3Q2021</th>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>4Q2021</th>
                                            <th scope="col" style={{ color: "rgba(255, 255, 255, 0.5)" }}>1Q2020</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row" style={{ color: "#fff" }}>Total Revenue</th>
                                            <td style={{ color: "#fff" }}>$123.232</td>
                                            <td style={{ color: "#fff" }}>$125.232</td>
                                            <td style={{ color: "#fff" }}>$125.210</td>
                                            <td style={{ color: "#fff" }}>$103.132</td>
                                            <td style={{ color: "#fff" }}>$130.212</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" style={{ color: "#fff" }}>Cost of Revenue</th>
                                            <td style={{ color: "#fff" }}>$123.232</td>
                                            <td style={{ color: "#fff" }}>$125.232</td>
                                            <td style={{ color: "#fff" }}>$125.210</td>
                                            <td style={{ color: "#fff" }}>$103.132</td>
                                            <td style={{ color: "#fff" }}>$130.212</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" style={{ color: "#fff" }}>Growth profit</th>
                                            <td style={{ color: "#fff" }}>$123.232</td>
                                            <td style={{ color: "#fff" }}>$125.232</td>
                                            <td style={{ color: "#fff" }}>$125.210</td>
                                            <td style={{ color: "#fff" }}>$103.132</td>
                                            <td style={{ color: "#fff" }}>$130.212</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" style={{ color: "#fff" }}>Operating expense</th>
                                            <td style={{ color: "#fff" }}>$123.232</td>
                                            <td style={{ color: "#fff" }}>$125.232</td>
                                            <td style={{ color: "#fff" }}>$125.210</td>
                                            <td style={{ color: "#fff" }}>$103.132</td>
                                            <td style={{ color: "#fff" }}>$130.212</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" style={{ color: "#fff" }}>R&D</th>
                                            <td style={{ color: "#fff" }}>$123.232</td>
                                            <td style={{ color: "#fff" }}>$125.232</td>
                                            <td style={{ color: "#fff" }}>$125.210</td>
                                            <td style={{ color: "#fff" }}>$103.132</td>
                                            <td style={{ color: "#fff" }}>$130.212</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default HomeContainer
