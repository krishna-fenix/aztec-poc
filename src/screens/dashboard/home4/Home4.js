import React, { useState } from 'react'
import { Image } from 'semantic-ui-react'
import './styles.css';
import home_logo from '../../../assets/images/ic_home_logo.png'

const Home4 = () => {
    const CONST = {
        SOLUTION: "SOLUTION",
        ABOUT: "ABOUT",
        LOGIN: "LOGIN",
        GET_STARTED: "GET_STARTED",
    }

    const [headSelected, setHeadSelected] = useState(CONST.SOLUTION)
    const handleHeaderClick = (TYPE) => {
        setHeadSelected(TYPE)
    }

    return (
        <div id="grad">
            <div className="secondaryContainer">
                <div className="topFlex">
                    <Image src={home_logo} height="80" width="150" />

                    <div style={{ display: 'flex', flexDirection: 'row' }}>

                        <button class="btn" style={headSelected === CONST.SOLUTION ?
                            { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                            { color: "#fff", marginLeft: 10, marginRight: 10 }}
                            onClick={() => handleHeaderClick(CONST.SOLUTION)}>
                            Solution
                        </button>

                        <button class="btn" style={headSelected === CONST.ABOUT ?
                            { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                            { color: "#fff", marginLeft: 10, marginRight: 10 }}
                            onClick={() => handleHeaderClick(CONST.ABOUT)}>
                            About
                        </button>

                        <button class="btn" style={headSelected === CONST.LOGIN ?
                            { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                            { color: "#fff", marginLeft: 10, marginRight: 10 }}
                            onClick={() => handleHeaderClick(CONST.LOGIN)}>
                            Login
                        </button>

                        <button class="btn" style={headSelected === CONST.GET_STARTED ?
                            { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                            { color: "#fff", marginLeft: 10, marginRight: 10 }}
                            onClick={() => handleHeaderClick(CONST.GET_STARTED)}>
                            Get Started
                        </button>

                    </div>

                </div>

                <div className="center">

                    <h2 style={{ color: "#0e895f" }}>Private Equity</h2>

                    <h1 style={{ color: "#fff", fontSize: 70 }}>Decision Platform</h1>

                    <h4 style={{ color: "#fff", maxWidth:"60%" }}>Make the right investment decision for your fund Predict, simulate, monitor, explore, and more</h4>

                    <div class="card">
                        <h6 style={{ color: "#0e895f", marginRight:12 }}>Enter your fund name</h6>

                        <button class="btn" style={{ background: "#0e895f", color: "#0a0c10", height: 40, border: "1px solid #0e895f" }}>
                            Get Started
                        </button>

                    </div>

                    <iframe src='https://my.spline.design/homepage3d-8a3c2c6619487c9a5d4091533ab357ff/' frameborder='0' width="80%" height="600" title="Animation"></iframe>

                </div>

            </div>

        </div>
    )
}

export default Home4;
