import React, { useState } from 'react'
import { Image } from 'semantic-ui-react'
import './styles.css';
import home_logo from '../../../../assets/images/aztec_group_logo_white.svg';
import { LOGIN, NEWHOME } from '../../../../constants/RoutePath';
import { useHistory } from 'react-router-dom';

const WelcomeContainer = () => {
    const CONST = {
        SOLUTION: "SOLUTION",
        ABOUT: "ABOUT",
        LOGIN: "LOGIN",
        GET_STARTED: "GET_STARTED",
    }
    const history = useHistory();

    const [headSelected, setHeadSelected] = useState(CONST.SOLUTION)

    const handleHeaderClick = (TYPE) => {
        if (TYPE === CONST.LOGIN) {
            history.push(LOGIN)
        }else if(TYPE === CONST.GET_STARTED){
            history.push(NEWHOME)
        }
        setHeadSelected(TYPE)
    }

    return (
        <div>

            <div id="grad">
                <div className="secondaryContainer">
                    <div className="topFlex">
                        <Image src={home_logo} style={{width: 150}} />

                        <div style={{ display: 'flex', flexDirection: 'row' }}>

                            <button className="btn" style={headSelected === CONST.SOLUTION ?
                                { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                                { color: "#fff", marginLeft: 10, marginRight: 10 }}
                                onClick={() => handleHeaderClick(CONST.SOLUTION)}>
                                Solution
                            </button>

                            <button className="btn" style={headSelected === CONST.ABOUT ?
                                { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                                { color: "#fff", marginLeft: 10, marginRight: 10 }}
                                onClick={() => handleHeaderClick(CONST.ABOUT)}>
                                About
                            </button>

                            <button className="btn" style={headSelected === CONST.LOGIN ?
                                { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                                { color: "#fff", marginLeft: 10, marginRight: 10 }}
                                onClick={() => handleHeaderClick(CONST.LOGIN)}>
                                Login
                            </button>

                            <button className="btn" style={headSelected === CONST.GET_STARTED ?
                                { background: "#fff", color: "#000", marginLeft: 10, marginRight: 10, height: 40, border: "1px solid #00757e" } :
                                { color: "#fff", marginLeft: 10, marginRight: 10 }}
                                onClick={() => handleHeaderClick(CONST.GET_STARTED)}>
                                Get Started
                            </button>

                        </div>

                    </div>

                    <div className="centerW">

                        <h2 style={{ color: "#0e895f" }}>Private Equity</h2>

                        <h1 style={{ color: "#fff", fontSize: 70 }}>Decision Platform</h1>

                        <h4 style={{ color: "#fff", maxWidth: "60%" }}>Make the right investment decision for your fund Predict, simulate, monitor, explore, and more</h4>

                        <div className="card">
                            <h6 style={{ color: "#0e895f", marginRight: 12 }}>Enter your fund name</h6>

                            <button className="btn" style={{ background: "#0e895f", color: "#0a0c10", height: 40, border: "1px solid #0e895f" }}>
                                Get Started
                            </button>

                        </div>

                        <iframe src='https://my.spline.design/homepage3d-8a3c2c6619487c9a5d4091533ab357ff/' frameborder='0' width="80%" height="600" title="Spline Animation"></iframe>

                    </div>

                </div>

            </div>

        </div>
    )
}

export default WelcomeContainer;
