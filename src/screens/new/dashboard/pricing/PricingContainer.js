import React from 'react'
import { Button } from 'semantic-ui-react'
import Header from '../../../../component/new/header/Header'
import './styles.css'

const PricingContainer = () => {
    return (
        <div id="parent">
            <iframe src='https://my.spline.design/creditcard-cdba3c7722050056df416d36fb4bf418/' className="child" title="Aztec"></iframe>

            <div style={{elevation:"50deg"}}>
                <Header />

                <h1 className="justify-content-center nav" style={{ color: "#fff" }}> Ready To Start With Aztec?</h1>

                <h5 className="justify-content-center nav" style={{ color: "#fff" }}> No suprise fees.</h5>

                <div className="col-md-12">

                    <div className="d-flex justify-content-center" style={{ marginLeft: 60, marginRight: 60, marginTop: 60 }}>

                        <div class="col-3 transaprentBox" >
                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 24, color: "#e70054" }}>Starter</span>

                            <h5 class="fw-lighter justify-content-center nav" style={{ color: "#fff" }}>Starter impliments features for small companies</h5>

                            <div class="justify-content-center nav" style={{ marginTop: 40, marginBottom: 40 }}>
                                <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                    <span class="fw-lighter align-middle fs-6">$</span>
                                    1,210.2
                                    <span class="align-middle fs-6">M</span>
                                </span>
                            </div>

                            {/* <div class="justify-content-center nav" style={{ marginTop: 30 }}> */}

                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 14, marginTop: 30, color: "#0e895f" }}>Upto 10 Users for a Month</span>

                            <div class="justify-content-center nav">
                                <Button
                                    style={{ marginTop: 40, width: 200, height: 40, background: "linear-gradient(90deg, rgba(231,0,84,1) 0%, rgba(255,160,146,1) 100%)" }}
                                    color="red"
                                    compact>
                                    BUY PLAN
                                </Button>

                            </div>

                        </div>

                        <div class="col-4 transaprentBox">
                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 24, color: "#e70054" }}>Busines</span>

                            <h5 class="fw-lighter justify-content-center nav" style={{ color: "#fff" }}>Starter impliments features for small companies</h5>

                            <div class="justify-content-center nav" style={{ marginTop: 40, marginBottom: 40 }}>
                                <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                    <span class="fw-lighter align-middle fs-6">$</span>
                                    2,210.2
                                    <span class="align-middle fs-6">M</span>
                                </span>
                            </div>

                            {/* <div class="justify-content-center nav" style={{ marginTop: 30 }}> */}

                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 14, marginTop: 30, color: "#0e895f" }}>Upto 10 Users for a Month</span>

                            <div class="justify-content-center nav">
                                <Button
                                    style={{ marginTop: 40, width: 200, height: 40, background: "linear-gradient(90deg, rgba(231,0,84,1) 0%, rgba(255,160,146,1) 100%)" }}
                                    color="red"
                                    compact>
                                    BUY PLAN
                                </Button>

                            </div>

                        </div>

                        <div class="col-3 transaprentBox" >
                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 24, color: "#e70054" }}>Enterprise</span>

                            <h5 class="fw-lighter justify-content-center nav" style={{ color: "#fff" }}>Starter impliments features for small companies</h5>

                            <div class="justify-content-center nav" style={{ marginTop: 40, marginBottom: 40 }}>
                                <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                    <span class="fw-lighter align-middle fs-6">$</span>
                                    510.2
                                    <span class="align-middle fs-6">M</span>
                                </span>
                            </div>

                            {/* <div class="justify-content-center nav" style={{ marginTop: 30 }}> */}

                            <span class="fw-lighter justify-content-center nav" style={{ fontSize: 14, marginTop: 30, color: "#0e895f" }}>Upto 10 Users for a Month</span>

                            <div class="justify-content-center nav">
                                <Button
                                    style={{ marginTop: 40, width: 200, height: 40, background: "linear-gradient(90deg, rgba(231,0,84,1) 0%, rgba(255,160,146,1) 100%)" }}
                                    color="red"
                                    compact>
                                    BUY PLAN
                                </Button>

                            </div>

                        </div>

                    </div>

                </div>

                <h1 style={{ color: "#fff", marginLeft: 170, width: "30%", marginTop: 40 }}>The Services are crafted smoothly for our customers and client.</h1>

            </div>

        </div>
    )
}

export default PricingContainer
