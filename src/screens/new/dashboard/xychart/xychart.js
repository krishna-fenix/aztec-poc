import React, { useEffect } from 'react'
import { create, useTheme, color, InterfaceColorSet, Triangle, Rectangle } from "@amcharts/amcharts4/core";
import { XYChart, DateAxis, ValueAxis, LineSeries, Bullet, CircleBullet, Legend, XYCursor } from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import './styles.css';

const Xychart = () => {
    useTheme(am4themes_animated)

    useEffect(() => {
        var chart = create("chartdiv", XYChart);
        chart.colors.step = 2;
        chart.data = generateChartData();

        // Create axes
        var dateAxis = chart.xAxes.push(new DateAxis());
        dateAxis.renderer.minGridDistance = 50;

        function createAxisAndSeries(field, name, opposite, bullet) {
            var valueAxis = chart.yAxes.push(new ValueAxis());
            if (chart.yAxes.indexOf(valueAxis) !== 0) {
                valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
            }

            var series = chart.series.push(new LineSeries());
            series.dataFields.valueY = field;
            series.dataFields.dateX = "date";
            series.strokeWidth = 2;
            series.yAxis = valueAxis;
            series.name = name;
            series.tooltipText = "{name}: [bold]{valueY}[/]";
            series.tensionX = 0.8;
            series.showOnInit = true;

            var interfaceColors = new InterfaceColorSet();
            
            switch (bullet) {
                case "triangle":
                    var bullet = series.bullets.push(new Bullet());
                    bullet.width = 12;
                    bullet.height = 12;
                    bullet.horizontalCenter = "middle";
                    bullet.verticalCenter = "middle";

                    var triangle = bullet.createChild(Triangle);
                    triangle.stroke = interfaceColors.getFor("background");
                    triangle.strokeWidth = 2;
                    triangle.direction = "top";
                    triangle.width = 12;
                    triangle.height = 12;
                    break;
                case "rectangle":
                    var bullet = series.bullets.push(new Bullet());
                    bullet.width = 10;
                    bullet.height = 10;
                    bullet.horizontalCenter = "middle";
                    bullet.verticalCenter = "middle";

                    var rectangle = bullet.createChild(Rectangle);
                    rectangle.stroke = interfaceColors.getFor("background");
                    rectangle.strokeWidth = 2;
                    rectangle.width = 10;
                    rectangle.height = 10;
                    break;

                default:
                    var bullet = series.bullets.push(new CircleBullet());
                    bullet.fill = color("#00000000");
                    bullet.circle.stroke = interfaceColors.getFor("background");
                    bullet.circle.strokeWidth = 2;
                    break;
            }

            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
        }


        createAxisAndSeries("visits", "Visits", false, "circle");
        createAxisAndSeries("views", "Views", true, "circle");

        // Add legend
        chart.legend = new Legend();

        // Add cursor
        chart.cursor = new XYCursor();

        // generate some random data, quite different range
        function generateChartData() {
            var chartData = [];
            var firstDate = new Date();
            firstDate.setDate(firstDate.getDate() - 100);
            firstDate.setHours(0, 0, 0, 0);

            var visits = 1600;
            var hits = 2900;
            var views = 8700;

            for (var i = 0; i < 15; i++) {
                // we create date objects here. In your data, you can have date strings
                // and then set format of your dates using chart.dataDateFormat property,
                // however when possible, use date objects, as this will speed up chart rendering.
                var newDate = new Date(firstDate);
                newDate.setDate(newDate.getDate() + i);

                visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
                hits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
                views += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

                chartData.push({
                    date: newDate,
                    visits: visits,
                    hits: hits,
                    views: views
                });
            }
            return chartData;
        }


    });

    return (
        <div id="grad">
            <div id="chartdiv"></div>
        </div>
    )
}

export default Xychart
