import React, { useEffect } from 'react'
import { create, useTheme, percent, color, ColorSet, InterfaceColorSet, Triangle, Rectangle } from "@amcharts/amcharts4/core";
import { PieChart, PieSeries, XYChart, DateAxis, ValueAxis, LineSeries, Bullet, CircleBullet,Legend,XYCursor } from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import './styles.css';

const Example = () => {
    //XY Chart
    var chartXY = create("chartdivXY", XYChart);

    useTheme(am4themes_myTheme);
    useTheme(am4themes_animated)

    function am4themes_myTheme(target) {
        if (target instanceof ColorSet) {
            target.list = [
                color("#008892"),
                color("#00b072"),
                color("#005271"),
            ];

            // target.setFor("text", color("#467B88"));
            // target.setFor('',color("#fff"))
        }
        if (target instanceof InterfaceColorSet) {
            target.setFor("text", color("#ffffff"));
        }
    }

    useEffect(() => {
        // PIE chart

        var chart = create("chartdiv", PieChart);
        chart.marginRight = 400;

        chart.data = [
            { "country": "Asia", "litres": 25 },
            { "country": "EU", "litres": 55 },
            { "country": "USA", "litres": 20 }
        ];

        chart.innerRadius = percent(60);

        // Add and configure Series
        var pieSeries = chart.series.push(new PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.slices.template.textDecoration = "#fff"
        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;

        var chartXY = create("chartdivXY", XYChart);

        // Increase contrast by taking evey second color
        chartXY.colors.step = 2;
        // Add data
        chartXY.data = [
            { date: "Jan", profit: 100, loss: 50 },
            { date: "Feb", profit: 110, loss: 60 },
            { date: "Mar", profit: 120, loss: 70 },
            { date: "Apr", profit: 130, loss: 80 },
            { date: "May", profit: 140, loss: 90 },
            { date: "June", profit: 150, loss: 100 },
            { date: "Jully", profit: 160, loss: 110 },
            { date: "Aug", profit: 170, loss: 120 },
            { date: "Sept", profit: 180, loss: 130 },
            { date: "Oct", profit: 190, loss: 140 },
            { date: "Nov", profit: 200, loss: 150 },
            { date: "Dec", profit: 210, loss: 160 },
        ];

        // Create axes
        var dateAxis = chartXY.xAxes.push(new DateAxis());
        dateAxis.renderer.minGridDistance = 50;

        createAxisAndSeries("profit", "profit", false, "circle");
        createAxisAndSeries("loss", "loss", true, "triangle");

        chartXY.legend = new Legend();
        chartXY.cursor = new XYCursor();
    });

    // Create series
    function createAxisAndSeries(field, name, opposite, bullet) {
        var valueAxis = chartXY.yAxes.push(new ValueAxis());
        if (chartXY.yAxes.indexOf(valueAxis) !== 0) {
            valueAxis.syncWithAxis = chartXY.yAxes.getIndex(0);
        }

        var series = chartXY.series.push(new LineSeries());
        series.dataFields.valueY = field;
        series.dataFields.dateX = "date";
        series.strokeWidth = 2;
        series.yAxis = valueAxis;
        series.name = name;
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.tensionX = 0.8;
        series.showOnInit = true;

        var interfaceColors = new InterfaceColorSet();

        switch (bullet) {
            case "triangle":
                var bullet = series.bullets.push(new Bullet());
                bullet.width = 12;
                bullet.height = 12;
                bullet.horizontalCenter = "middle";
                bullet.verticalCenter = "middle";

                var triangle = bullet.createChild(Triangle);
                triangle.stroke = interfaceColors.getFor("background");
                triangle.strokeWidth = 2;
                triangle.direction = "top";
                triangle.width = 12;
                triangle.height = 12;
                break;
            case "rectangle":
                var bullet = series.bullets.push(new Bullet());
                bullet.width = 10;
                bullet.height = 10;
                bullet.horizontalCenter = "middle";
                bullet.verticalCenter = "middle";

                var rectangle = bullet.createChild(Rectangle);
                rectangle.stroke = interfaceColors.getFor("background");
                rectangle.strokeWidth = 2;
                rectangle.width = 10;
                rectangle.height = 10;
                break;
            default:
                var bullet = series.bullets.push(new CircleBullet());
                bullet.circle.stroke = interfaceColors.getFor("background");
                bullet.circle.strokeWidth = 2;
                break;
        }
    }

    return (
        <div id="grad">
            <div id="chartdiv"></div>
        </div>
    )
}

export default Example;
