import React, { useEffect, useState } from 'react'

import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import ic_full_screen from '../../../../assets/images/ic_full_screen.svg';
import ic_download_white from '../../../../assets/images/ic_download_white.svg';
import { Image, Icon } from 'semantic-ui-react'
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import './styles.css';
// import ic_insights from '../../../../assets/images/ic_insights.png'
import ic_donut from '../../../../assets/images/ic_donut.svg'
import ic_show_chart from '../../../../assets/images/ic_show_chart.svg'
import ic_auto_graph from '../../../../assets/images/ic_auto_graph.svg'
import ic_ac_unit from '../../../../assets/images/ic_ac_unit.svg'
import place_holder from '../../../../assets/images/ic_place_holder.png'
import { exportTableState, saveTableState } from '../../../../services/common_services';
import Header from '../../../../component/new/header/Header';
import ic_table_chart from '../../../../assets/images/ic_table_chart.svg'

const HomeContainer = () => {
    const rowData = [
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 143.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 123, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },
        { Quarter: "17Q1", Revenue: 1.1234, NetIncome: -139.456, OperationIncome: -138.259, OperationExpenses: 138.295, R_D: 73.942 },
        { Quarter: "17Q2", Revenue: 1.322, NetIncome: -154.467, OperationIncome: -152.002, OperationExpenses: 152.002, R_D: 76.062 },];

    // eslint-disable-next-line no-unused-vars
    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumnApi] = useState(null);

    const [selected, setSelected] = useState(false)
    const [style, setStyle] = useState({
        height: '60vh',
        width: '60%',
        marginTop: 50
    });

    const [topStyle, setTopStyle] = useState({
        height: 'auto',
        width: '60%',
        position: 'relative'
    })

    const rowStyle = { background: 'rgba(0, 0, 0, 0.1)', color: "#fff" };

    const getRowStyle = params => {
        if (params.node.rowIndex % 2 !== 0) {
            return { background: 'rgba(255, 255, 255, 0.1)' };
        }
    };

    const dynamicCellStyleRevenue = params => {
        if (params.value === 123) {
            return {
                background: "linear-gradient(to right,rgba(14, 182, 121, 0.3), rgba(0, 0, 0, 0.1))"
            }
        }
        return null;
    };

    const dynamicCellStyleOperationEx = params => {
        if (params.value === 143.295) {
            return {
                background: "linear-gradient(to right,rgba(91, 53, 80, 0.8),rgba(91, 53, 80, 0.8), rgba(0, 0, 0, 0.1))"
            }
        }
        return null;
    };

    const onGridReady = (params) => {
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    };

    const saveState = () => {
        window.colState = gridColumnApi.getColumnState();
        saveTableState(window.colState)
    };

    const restoreState = () => {
        if (!exportTableState()) {
            return;
        }
        gridColumnApi.applyColumnState({
            state: exportTableState(),
            applyOrder: true,
        });
    };

    const fillLarge = () => {
        setWidthAndHeight('100%', '60vh');
    };

    const fillExact = () => {
        setWidthAndHeight('60%', '60vh');

    };

    const setWidthAndHeight = (width, height) => {
        setStyle({
            width,
            height,
            marginTop: 50
        });
        setTopStyle({
            width,
            position: 'relative'
        });
    };

    useEffect(() => {
        if (gridColumnApi != null)
            restoreState()

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [gridColumnApi]);

    return (
        <div id="grad">

            <div className="secondaryContainer">

                <Header />
                <div className="d-flex justify-content-between flex-wrap p-0" style={{ marginTop: 20, width: "100%" }}>

                    <div class="col-md-2 justify-content-start">

                        <div className="transaprentBoxStart">

                            <Image height={60} width={"100%"} src={place_holder} />

                            <div style={{ width: "100%", height: 1, background: "#b6bcbf", marginTop: 10 }} />
                            <p class="fs-6 col justify-content-start" style={{ color: "#fff", marginTop: 12 }}>Weekly Insight</p>
                            <div style={{ width: "100%", height: 1, background: "#b6bcbf" }} />

                            <div className="row" style={{ marginTop: 6, marginRight: 6, marginLeft: 6 }}>
                                <div style={{ height: 20, width: 20, background: "linear-gradient(to right,rgba(14, 182, 121, 0.6), rgba(0, 0, 0, 0.1))", borderRadius: 10 }} />

                                <p class="fs-6 col justify-content-start" style={{ color: "#fff" }}>Get revenue performance</p>

                                <Icon name="chevron down" inverted />

                            </div>

                            <div style={{ width: "100%", height: 1, background: "#b6bcbf" }} />

                            <div className="row" style={{ marginTop: 6, marginRight: 6, marginLeft: 6 }}>
                                <div style={{ height: 20, width: 20, background: "linear-gradient(to right,rgba(91, 53, 80, 0.8),rgba(91, 53, 80, 0.8), rgba(0, 0, 0, 0.1))", borderRadius: 10 }} />

                                <p class="fs-6 col justify-content-start" style={{ color: "#fff" }}>Drop in net income</p>

                                <Icon name="chevron down" inverted />

                            </div>

                            <div style={{ width: "100%", height: 1, background: "#b6bcbf" }} />

                            <div className="row" style={{ marginTop: 6, marginRight: 6, marginLeft: 6 }}>
                                <div style={{ height: 20, width: 20, background: "linear-gradient(to right,rgba(91, 53, 80, 0.8),rgba(91, 53, 80, 0.8), rgba(0, 0, 0, 0.1))", borderRadius: 10 }} />

                                <p class="fs-6 col justify-content-start" style={{ color: "#fff" }}>Increase expenses</p>

                                <Icon name="chevron down" inverted />

                            </div>

                            <div style={{ width: "100%", height: 1, background: "#b6bcbf" }} />

                            <div style={{ display: 'flex', justifyContent: 'center', textAlign: 'center', marginTop: 10 }}>

                                <Icon name="chevron up" inverted />

                            </div>

                        </div>

                    </div>

                    <div className="col-md-10">

                        <div className="d-flex justify-content-between" style={{ marginLeft: 60, marginRight: 60 }}>

                            <div class="row" className="transaprentBox">
                                <span class="fw-lighter justify-content-center nav" style={{ fontSize: 16, color: "#0e895f" }}>Revenue</span>

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                        <span class="fw-lighter align-middle fs-6">$</span>
                                        1,210.2
                                        <span class="align-middle fs-6">M</span>
                                    </span>
                                </div>

                                <div className="d-flex flex-wrap p-0" style={{ marginTop: 30 }}>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#0e895f" }}>+34 M (2.3%)</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf", marginLeft: 12, marginRight: 12 }}>|</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf" }}>Previous Quarter</span>

                                </div>
                            </div>

                            <div class="row" className="transaprentBox">
                                <span class="fw-lighter justify-content-center nav" style={{ fontSize: 16, color: "#0e895f" }}>Profit</span>

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                        <span class="fw-lighter align-middle fs-6">$</span>
                                        612.8
                                        <span class="align-middle fs-6">M</span>
                                    </span>
                                </div>

                                <div className="d-flex flex-wrap p-0" style={{ marginTop: 30 }}>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#db0052" }}>-30 M (-6.2%)</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf", marginLeft: 12, marginRight: 12 }}>|</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf" }}>Previous Quarter</span>

                                </div>
                            </div>

                            <div class="row" className="transaprentBox">
                                <span class="fw-lighter justify-content-center nav" style={{ fontSize: 16, color: "#0e895f" }}>Revenue</span>

                                <div class="justify-content-center nav">
                                    <span class="align-top " style={{ fontSize: 60, marginTop: 30, color: "#fff" }}>
                                        12.8
                                        <span class="align-middle fs-6">%</span>
                                    </span>
                                </div>

                                <div className="d-flex flex-wrap p-0" style={{ marginTop: 30 }}>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#0e895f" }}>+0.5 (+4.1%)</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf", marginLeft: 12, marginRight: 12 }}>|</span>

                                    <span class="fw-lighter justify-content-center" style={{ fontSize: 14, color: "#b6bcbf" }}>Previous Quarter</span>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div className="transparentRound" >

                    <div className="d-flex justify-content-evenly flex-wrap p-0">
                        <Image className="fill" style={{ height: 25, width: 25, marginLeft: 12, marginRight: 12 }} src={ic_donut} />

                        <Image className="fill" style={{ height: 25, width: 25, marginLeft: 12, marginRight: 12 }} src={ic_table_chart} />

                        <Image className="fill" style={{ height: 25, width: 25, marginLeft: 12, marginRight: 12 }} src={ic_auto_graph} />

                        <Image className="fill" style={{ height: 25, width: 25, marginLeft: 12, marginRight: 12 }} src={ic_show_chart} />

                        <Image className="fill" style={{ height: 25, width: 25, marginLeft: 12, marginRight: 12 }} src={ic_ac_unit} />

                    </div>

                </div>

                <div style={topStyle}>

                    <div style={{ display: 'flex', flexDirection: 'row', padding: 5, position: 'absolute', right: 0 }}>

                        <div onClick={() => saveState()}>

                            <Image src={ic_download_white} size="small" style={{
                                height: 20,
                                width: 25,
                            }} />

                        </div>

                        <div onClick={() => {
                            if (selected) {
                                fillExact()
                                setSelected(false)
                            }
                            else {
                                fillLarge()
                                setSelected(true)
                            }
                        }}>
                            <Image src={ic_full_screen} size="small" style={{
                                height: 20,
                                width: 25,
                            }}
                            />

                        </div>

                    </div>

                </div>

                <div className="ag-theme-alpine Back" style={style} >

                    <AgGridReact
                        rowStyle={rowStyle}
                        getRowStyle={getRowStyle}
                        rowData={rowData}
                        rowSelection="multiple"
                        suppressRowClickSelection={true}
                        defaultColDef={{
                            editable: true,
                            sortable: true,
                            minWidth: 100,
                            filter: true,
                            resizable: true,
                            // floatingFilter: true,
                            flex: 1,
                        }}
                        components={{
                            rowNodeIdRenderer: function (params) {
                                return params.node.id + 1;
                            },
                        }}
                        sideBar={{
                            toolPanels: ['columns', 'filters'],
                            defaultToolPanel: '',
                        }}
                        pagination={true}

                        paginationPageSize={500}
                        onGridReady={onGridReady}
                        enableCharts={true}
                        enableRangeSelection={true}
                        rowDragManaged={true}
                        animateRows={true}>

                        <AgGridColumn field="Quarter" headerName="Quarter" enablePivot={true} enableRowGroup={true} minWidth={170} />

                        <AgGridColumn field="Revenue" cellStyle={dynamicCellStyleRevenue} enablePivot={true} enableValue={true} enableRowGroup={true} minWidth={150} />

                        <AgGridColumn field="NetIncome" headerName="Net Income" enableValue={true} enablePivot={true} enableRowGroup={true} />

                        <AgGridColumn
                            field="OperationIncome"
                            headerName="Operation Income"
                            enableValue={true}
                            columnGroupShow="closed"
                            filter="agNumberColumnFilter"
                            width={120}
                            flex={0}                            
                            enablePivot={true}
                            enableRowGroup={true}
                        />

                        <AgGridColumn
                            field="OperationExpenses"
                            headerName="Operation Expenses"
                            enableValue={true}
                            columnGroupShow="open"
                            filter="agNumberColumnFilter"
                            width={100}
                            flex={0}
                            cellStyle={dynamicCellStyleOperationEx}                            
                            enablePivot={true}
                            enableRowGroup={true}
                        />

                        <AgGridColumn
                            field="R_D"
                            headerName="R&D"
                            enableValue={true}
                            columnGroupShow="open"
                            filter="agNumberColumnFilter"
                            width={100}
                            flex={0}                            
                            enablePivot={true}
                            enableRowGroup={true}
                        />

                    </AgGridReact>

                </div>

            </div>

        </div>
    )
}

export default HomeContainer;
