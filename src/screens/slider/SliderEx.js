import React, { useEffect, useState } from 'react'
import { Slider, withStyles } from '@material-ui/core'
import './styles.css'
import { axiosGet, axiosPost } from '../../helpers/AxiosInstance'
import { RESTAPI } from '../../constants/RestApi'
import { Button } from 'semantic-ui-react'

const SliderEx = () => {
    const [cash, setCash] = useState({ possible_values: [] });
    const [intangibleAssets, setIntangibleAssets] = useState({ possible_values: [] });
    const [netTangibleAssets, setNetTangibleAssets] = useState({ possible_values: [] });
    const [otherAssets, setOtherAssets] = useState({ possible_values: [] });
    const [otherCurrentAssets, setOtherCurrentAssets] = useState({ possible_values: [] });
    const [totalAssets, setTotalAssets] = useState({ possible_values: [] });

    const [selectedCash, setSelectedCash] = useState(0);
    const [selectedIntangibleAssets, setselectedIntangibleAssets] = useState(0);
    const [selectedNetTangibleAssets, setselectedNetTangibleAssets] = useState(0);
    const [selectedOtherAssets, setSelectedOtherAssets] = useState(0);
    const [selectedOtherCurrentAssets, setSelectedOtherCurrentAssets] = useState(0);
    const [selectedTotalAssets, setSelectedTotalAssets] = useState(0);


    useEffect(() => {
        axiosGet(RESTAPI.GET_NODE_VALUE).then((response) => {
            if (response.statusCode === 200) {
                let data = response.data

                setSelectedCash(data.cash.possible_values.findIndex(obj => obj === data.cash.current_value))
                setselectedIntangibleAssets(data.intangibleAssets.possible_values.findIndex(obj => obj === data.intangibleAssets.current_value))
                setselectedNetTangibleAssets(data.netTangibleAssets.possible_values.findIndex(obj => obj === data.netTangibleAssets.current_value))
                setSelectedOtherAssets(data.otherAssets.possible_values.findIndex(obj => obj === data.otherAssets.current_value))
                setSelectedOtherCurrentAssets(data.otherCurrentAssets.possible_values.findIndex(obj => obj === data.otherCurrentAssets.current_value))
                setSelectedTotalAssets(data.totalAssets.possible_values.findIndex(obj => obj === data.totalAssets.current_value))

                // setOnSelect({ ...onSelect, cash: data.cash.current_value })

                data.cash.sliderValue = []
                data.intangibleAssets.sliderValue = []
                data.netTangibleAssets.sliderValue = []
                data.otherAssets.sliderValue = []
                data.otherCurrentAssets.sliderValue = []
                data.totalAssets.sliderValue = []

                for (let i = 0; i < data.cash.possible_values.length; i++) {

                    const [coefficient, exponent] = (data.cash.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.cash.sliderValue.push({ value: i, label: label })
                }

                for (let i = 0; i < data.intangibleAssets.possible_values.length; i++) {

                    const [coefficient, exponent] = (data.intangibleAssets.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.intangibleAssets.sliderValue.push({ value: i, label: label })
                }

                for (let i = 0; i < data.netTangibleAssets.possible_values.length; i++) {

                    const [coefficient, exponent] = (data.netTangibleAssets.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.netTangibleAssets.sliderValue.push({ value: i, label: label })
                }

                for (let i = 0; i < data.otherAssets.possible_values.length; i++) {
                    const [coefficient, exponent] = (data.otherAssets.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.otherAssets.sliderValue.push({ value: i, label: label })
                }

                for (let i = 0; i < data.otherCurrentAssets.possible_values.length; i++) {

                    const [coefficient, exponent] = (data.otherCurrentAssets.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.otherCurrentAssets.sliderValue.push({ value: i, label: label })
                }

                for (let i = 0; i < data.totalAssets.possible_values.length; i++) {

                    const [coefficient, exponent] = (data.totalAssets.possible_values[i])
                        .toExponential()
                        .split('e')
                        .map((item) => Number(item));
                    let label = `${Math.round(coefficient)}e^${exponent}`

                    data.totalAssets.sliderValue.push({ value: i, label: label })
                }

                setCash(data.cash)
                setIntangibleAssets(data.intangibleAssets)
                setNetTangibleAssets(data.netTangibleAssets)
                setOtherAssets(data.otherAssets)
                setOtherCurrentAssets(data.otherCurrentAssets)
                setTotalAssets(data.totalAssets)

            } else {
            }

        }).catch(error => {
        })

    }, []);

    function valueLabelFormat(value) {
        return cash.possible_values[value];
    }

    function valueLabelFormatIntagible(value) {
        return intangibleAssets.possible_values[value];
    }

    function valueLabelFormatNetagible(value) {
        return netTangibleAssets.possible_values[value];
    }

    function valueLabelFormatOther(value) {
        return otherAssets.possible_values[value];
    }

    function valueLabelFormatOtherCurrent(value) {
        return otherCurrentAssets.possible_values[value];
    }

    function valueLabelFormatTotalAssets(value) {
        return totalAssets.possible_values[value];
    }

    function predict() {
        const data1 = {
            "totalAssets": totalAssets.possible_values[selectedTotalAssets],
            "otherCurrentAssets": otherCurrentAssets.possible_values[selectedOtherCurrentAssets],
            "otherAssets": otherAssets.possible_values[selectedOtherAssets],
            "intangibleAssets": intangibleAssets.possible_values[selectedIntangibleAssets],
            "cash": cash.possible_values[selectedCash],
            "netTangibleAssets": netTangibleAssets.possible_values[selectedNetTangibleAssets]
        }

        axiosPost(RESTAPI.POST_NODE, data1).then((response) => {
            if (response.statusCode === 200) {

                setSelectedCash(cash.possible_values.findIndex(obj => obj === response.data.cash))
                setselectedIntangibleAssets(intangibleAssets.possible_values.findIndex(obj => obj === response.data.intangibleAssets))
                setselectedNetTangibleAssets(netTangibleAssets.possible_values.findIndex(obj => obj === response.data.netTangibleAssets))
                setSelectedOtherAssets(otherAssets.possible_values.findIndex(obj => obj === response.data.otherAssets))
                setSelectedOtherCurrentAssets(otherCurrentAssets.possible_values.findIndex(obj => obj === response.data.otherCurrentAssets))
                setSelectedTotalAssets(totalAssets.possible_values.findIndex(obj => obj === response.data.totalAssets))
            }
        })
    }

    // const classes = useStyles();

    const handleChange = (event, newValue) => {
        setSelectedCash(newValue);
    };

    const handleChangeIntangible = (event, newValue) => {
        setselectedIntangibleAssets(newValue);
    };

    const handleChangeNetagibl = (event, newValue) => {
        setselectedNetTangibleAssets(newValue);
    };

    const handleChangeotherAssets = (event, newValue) => {
        setSelectedOtherAssets(newValue);
    };

    const handleChangeotherCurrentAssets = (event, newValue) => {
        setSelectedOtherCurrentAssets(newValue);
    };

    const handleChangetotalAssets = (event, newValue) => {
        setSelectedTotalAssets(newValue);
    };

    const CustomeSlider = withStyles({
        root: {
            color: '#52af77',
            height: 8,
        },
        thumb: {
            height: 24,
            width: 24,
            backgroundColor: '#fff',
            border: '2px solid currentColor',
            marginTop: -8,
            marginLeft: -12,
            '&:focus, &:hover, &$active': {
                boxShadow: 'inherit',
            },
        },
        active: {},
        valueLabel: {
            left: 'calc(-50% + 4px)',
            '& *': {
                background: 'transparent',
                color: '#52af77',
            },
        },
        track: {
            height: 8,
            borderRadius: 4,
        },
        rail: {
            height: 8,
            borderRadius: 4,
        },
    })(Slider);

    return (
        <div id="grad">

            <div className="Back">

                {/* <Dropdown clearable options={selectOptions} selection /> */}

                <div className="sliderWidth" style={{ marginTop: 50 }}>
                    <h1>Cash</h1>

                    <CustomeSlider
                        // defaultValue={selectedCash}
                        value={selectedCash}
                        getAriaValueText={valueLabelFormat}
                        valueLabelFormat={valueLabelFormat}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={cash.sliderValue}
                        min={0}
                        max={cash.possible_values.length - 1}
                        onChange={handleChange}
                    />

                </div>

                <div className="sliderWidth" style={{ marginTop: 50 }}>

                    <h1>Intangible Assets</h1>

                    <CustomeSlider
                        value={selectedIntangibleAssets}
                        valueLabelFormat={valueLabelFormatIntagible}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={intangibleAssets.sliderValue}
                        min={0}
                        max={intangibleAssets.possible_values.length - 1}
                        onChange={handleChangeIntangible}
                    />

                </div>

                <div className="sliderWidth" style={{ marginTop: 50 }}>

                    <h1>Net Tangible Assets</h1>

                    <CustomeSlider
                        defaultValue={selectedNetTangibleAssets}
                        value={selectedNetTangibleAssets}
                        valueLabelFormat={valueLabelFormatNetagible}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={netTangibleAssets.sliderValue}
                        min={0}
                        max={netTangibleAssets.possible_values.length - 1}
                        onChange={handleChangeNetagibl}
                    />

                </div>

                <div className="sliderWidth" style={{ marginTop: 50 }}>

                    <h1>Other Assets</h1>

                    <CustomeSlider
                        defaultValue={selectedOtherAssets}
                        value={selectedOtherAssets}
                        // valueLabelFormat={valueLabelFormat}
                        valueLabelFormat={valueLabelFormatOther}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={otherAssets.sliderValue}
                        min={0}
                        max={otherAssets.possible_values.length - 1}
                        onChange={handleChangeotherAssets}
                    />

                </div>

                <div className="sliderWidth" style={{ marginTop: 50 }}>
                    <h1>Other Current Assets</h1>

                    <CustomeSlider
                        defaultValue={selectedOtherCurrentAssets}
                        value={selectedOtherCurrentAssets}
                        // valueLabelFormat={valueLabelFormat}
                        valueLabelFormat={valueLabelFormatOtherCurrent}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={otherCurrentAssets.sliderValue}
                        min={0}
                        max={otherCurrentAssets.possible_values.length - 1}
                        onChange={handleChangeotherCurrentAssets}
                    />

                </div>

                <div className="sliderWidth" style={{ marginTop: 50 }}>
                    <h1>Total Assets</h1>

                    <CustomeSlider
                        defaultValue={selectedTotalAssets}
                        value={selectedTotalAssets}
                        valueLabelFormat={valueLabelFormatTotalAssets}
                        aria-labelledby="discrete-slider-restrict"
                        step={null}
                        valueLabelDisplay="on"
                        marks={totalAssets.sliderValue}
                        min={0}
                        max={totalAssets.possible_values.length - 1}
                        onChange={handleChangetotalAssets}
                    />

                </div>

            </div>

            <Button
                className="Button"
                color="blue"
                size="medium"
                onClick={predict}
                style={{ marginBottom: 50, marginTop: 50 }}>
                Predict
            </Button>

        </div>
    )
}

export default SliderEx
