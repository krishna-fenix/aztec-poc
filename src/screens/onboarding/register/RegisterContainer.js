import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Form, Header, Segment } from 'semantic-ui-react'
import { HOME } from '../../../constants/RoutePath'
// import avtar from '../../../assets/images/ic_avtar.jpg'
import './styles.css'


const RegisterContainer = () => {
    const genderList = [
        { key: "Male", text: "Male", value: "Male" },
        { key: "Female", text: "Female", value: "Female" },
        { key: "Other", text: "Other", value: "Other" }]

    const contact = [
        { key: 'in', value: 'in', flag: 'in', text: 'India' },
        { key: 'uk', value: 'uk', flag: 'uk', text: 'UK' },
        { key: 'us', value: 'us', flag: 'us', text: 'US' },
    ]

    const roleList = [
        { key: "Role 1", text: "Role 1", value: "Role 1" },
        { key: "Role 2", text: "Role 2", value: "Role 2" },
        { key: "Role 3", text: "Role 3", value: "Role 3" },

    ]

    const location = [
        { key: "Mumbai", text: "Mumbai", value: "Mumbai" },
        { key: "London", text: "London", value: "London" },
        { key: "New York", text: "New York", value: "New York" },
    ]

    const fundList = [
        { key: "Fund 1", text: "Fund 1", value: "Fund 1" },
        { key: "Fund 2", text: "Fund 2", value: "Fund 3" },
        { key: "Fund 3", text: "Fund 3", value: "Fund 3" },
    ]

    return (
        <div className="containerT">
            <Header as="h1" style={{ marginBottom: 40 }}>Get Started</Header>

            <Segment className="cardContainer">

                <Form widths='equal'>
                    <Form.Field>
                        <label>Name</label>
                        <input placeholder='Name' />
                    </Form.Field>

                    <Form.Group widths='equal'>

                        <Form.Select
                            label="Country Code"
                            placeholder="Country Code"
                            fluid
                            selection
                            options={contact} />

                        <Form.Field >
                            <label>Phone Number</label>
                            <input placeholder='Phone Number' type='number' />

                        </Form.Field>

                    </Form.Group>

                    <Form.Field>
                        <label>Address</label>
                        <input placeholder='Address' />
                    </Form.Field>

                    <Form.Select
                        label="Gender"
                        placeholder='Gender'
                        fluid
                        selection
                        options={genderList} />

                    <Form.Select
                        label="Select your Fund"
                        placeholder='Select your Fund'
                        fluid
                        selection
                        options={fundList} />

                    <Form.Select
                        label="Select your Role"
                        placeholder='Select your Role'
                        fluid
                        selection
                        options={roleList} />

                    <Form.Select
                        label="Location"
                        placeholder='Location'
                        fluid
                        selection
                        options={location} />

                    <Form.Field>
                        <label>LinkedIn Profile</label>
                        <input placeholder='LinkedIn Profile' />

                    </Form.Field>

                    <Form.Field>
                        <label>Enter Password</label>
                        <input placeholder='Password' type='password' />

                    </Form.Field>

                    <Form.Field>
                        <label>Confirm Password</label>
                        <input placeholder='Password' type='password' />

                    </Form.Field>

                </Form>
                <Button
                    as={Link}
                    to={HOME}
                    className="Button"
                    color="blue"
                    fluid
                    size="medium"
                    style={{ marginTop: 50 }}>
                    Register
                </Button>

            </Segment>

        </div>
    )
}

export default RegisterContainer
