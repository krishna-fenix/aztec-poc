import React from 'react'
import './styles.css'
import { Link } from 'react-router-dom'
import { Image } from 'semantic-ui-react'
import logo from '../../../assets/images/ic_logo.png'

const WelcomeContainer = () => {

    return (
        <div>
            <div className="container">
                <Image src={logo} centered />                
                <Link to="/onboarding/login" className="btn btn-primary">
                    Login
                </Link>             

                {/* <Button
                    as={Link}
                    to="/home"
                    className="Button"
                    color="olive"
                    labelPosition='right'
                    icon
                    size="big"
                    compact
                    onClick={handleClick}
                    onKeyPress={handleClick}>
                    HOME
                    <Icon name='right arrow' />
                </Button> */}
            </div>
        </div>
    )
}

export default WelcomeContainer
