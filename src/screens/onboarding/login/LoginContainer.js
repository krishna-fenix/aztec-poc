import React, { useEffect, useState } from 'react'

import firebase from "../../../firebase/config";
import { REGISTER } from '../../../constants/RoutePath';
import { useHistory } from 'react-router';
import { checkDomain, setUser } from '../../../services/common_services';
import LoginComponent from './LoginComponent';

const LoginContainer = () => {
  const history = useHistory();

  const [otp, setOtp] = useState('')
    // const [phone, setPhone] = useState('')
    const [verifyId, setVerifyID] = useState('')


  useEffect(() => {
    var unregisterAuthObserver = firebase.Unsubscribe;

    unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
      console.log(user);
      if (!!user) {
        // history.push('/home')
        // setUser(user)

        if (checkDomain(user.email)) {
          history.push('/myfunds')
          setUser(user)
        } else {
          firebase.auth().signOut()
          localStorage.clear()
          window.location.reload(true);
        }

        // logginSuccess(user)
      } else {
      }
    },
      error => {
        console.log("error on auth state changed", error)
      }
    );

    // var user = firebase.auth().currentUser;
    // if (!!user) {
    //   logginSuccess(user)
    // } else {
    //   try {
    //     unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
    //       if (!!user) {
    //         logginSuccess(user)
    //       }
    //     });
    //   } catch (error) {
    //   }
    // }

    return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChange = (otp) => {
    setOtp(otp)
  }

  const startRegister = () => {
    history.push(REGISTER)
  }

  const askForOTP = () => {

    var user = firebase.auth().currentUser;

    var cred = firebase.auth.PhoneAuthProvider.credential(verifyId, otp);
    var multiFactorAssertion = firebase.auth.PhoneMultiFactorGenerator.assertion(cred);
    // Complete enrollment.
    user.multiFactor.enroll(multiFactorAssertion, "Krishna Vishwakarma").then(response => {
      console.log(response)
    }).catch(error => {
      console.log(error);
    })
  }

    // eslint-disable-next-line no-unused-vars
    const logginSuccess = (user) => {
        var recaptchaVerifier = new firebase.auth.RecaptchaVerifier("container");
        user.multiFactor.getSession().then(function (multiFactorSession) {
        // Specify the phone number and pass the MFA session.
        var phoneInfoOptions = {
            phoneNumber: "+918982952675",
            session: multiFactorSession
        };
        var phoneAuthProvider = new firebase.auth.PhoneAuthProvider();
        // Send SMS verification code.
        return phoneAuthProvider.verifyPhoneNumber(phoneInfoOptions, recaptchaVerifier)        
        }).then(function (verificationId) {       
            setVerifyID(verificationId)
            
        });
    }

  // eslint-disable-next-line no-unused-vars
  const GetEmail = () => {
    var email = window.prompt('Please provide your email');
    firebase.auth().fetchSignInMethodsForEmail(email)
      .then((signInMethods) => {
        // This returns the same array as fetchProvidersForEmail but for email
        // provider identified by 'password' string, signInMethods would contain 2
        // different strings:
        // 'emailLink' if the user previously signed in with an email/link
        // 'password' if the user has a password.
        // A user could have both.
        console.log(signInMethods);
        if (signInMethods.indexOf(
          firebase.auth.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD) !== -1) {
          // User can sign in with email/password.
        }
        if (signInMethods.indexOf(
          firebase.auth.EmailAuthProvider.EMAIL_LINK_SIGN_IN_METHOD) !== -1) {
          // User can sign in with email/link.
        }
      })
      .catch((error) => {
        // Some error occurred, you can inspect the code: error.code
      });
  }

  return (
    <LoginComponent
      onChange={onChange}
      startRegister={startRegister}
      askForOTP={askForOTP} />
  )
}

export default LoginContainer
