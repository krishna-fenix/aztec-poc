import React from 'react'
import firebase, { firebaseui } from "../../../firebase/config";
import logo from '../../../assets/images/ic_logo.png'
import { StyledFirebaseAuth } from 'react-firebaseui';
import { Header, Image } from 'semantic-ui-react';
import './styles.css'

const LoginComponent = ({
    onChange,
    startRegister,
    askForOTP
}) => {
    
    return (
        <div className="container">
            <Image src={logo} centered />
            <StyledFirebaseAuth 
            uiConfig={firebaseui}                    
            firebaseAuth={firebase.auth()}             
            />
            <div onClick={startRegister}>
                <Header as='h5'>New here? Get Started</Header>
            </div>
            <div id="container" />
            {/* <Form>
                <Form.Field>
                    <label>OTP</label>
                    <input placeholder='OTP'
                        onChange={(value) => {
                            onChange(value.target.value)
                        }} />
                    <button className="btn btn-primary" onClick={askForOTP}>Submit</button>
                </Form.Field>
            </Form> 
            {
                firebase.auth()?.currentUser ? (
                <div>
                    <h1>My App</h1>
                    <p>Welcome {firebase.auth()?.currentUser?.displayName}! You are now signed-in!</p>
                    <button onClick={() => firebase.auth().signOut()}>Sign-out</button>
                </div>
                ) : ''
            } */}
        </div>
    )
}

export default LoginComponent
