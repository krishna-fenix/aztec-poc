import React, { Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import PrivateRoute from "../routes/private-routes";
import { ToastContainer } from 'react-toastify';

const LoginContainer = React.lazy(() => import('../screens/onboarding/login/LoginContainer'));
const RegisterContainer = React.lazy(() => import('../screens/onboarding/register/RegisterContainer'));
const HomeContainer = React.lazy(() => import('../screens/dashboard/home/HomeContainer'));
const Home2Container = React.lazy(() => import('../screens/dashboard/home2/HomeContainer'));
const TempContainer = React.lazy(() => import('../screens/dashboard/temp/TempContainer'));
const Table = React.lazy(() => import('../screens/table/Table'));
const Slider = React.lazy(() => import('../screens/slider/SliderEx'));
const Chart = React.lazy(() => import('../screens/dashboard/chart/Chart'));
const DataVisulisation = React.lazy(() => import('../screens/data-visulisations/data-visulisation'));
const Example3 = React.lazy(() => import('../screens/dashboard/example3/example3'));
const Home = React.lazy(() => import('../screens/new/dashboard/Home/HomeContainer'));
const Profit = React.lazy(() => import('../screens/profit/profit'));
const Example =  React.lazy(() => import('../screens/new/dashboard/example/Example'));
const Xychart =  React.lazy(() => import('../screens/new/dashboard/xychart/xychart'));
const Pricing = React.lazy(()=> import('../screens/new/dashboard/pricing/PricingContainer'))


const Routes = () => {
    return (
        <>
            <Suspense fallback={
                <div className="m-5" style={{ textAlign: "center" }}>
                    <i className="fa fa-spinner fa-spin" style={{ fontSize: 30, color: '#09a2fb' }}></i>
                </div>}>
                <Switch>
                    {/* <PrivateRoute path="/welcome" component={Welcome} /> */}
                    <Route exact path="/onboarding/login" component={LoginContainer} />
                    {/* <Route exact path="/onboarding/welcome" component={WelcomeContainer} />                                                             */}
                    <Route exact path="/onboarding/register" component={RegisterContainer} />
                    <PrivateRoute path="/myfunds" component={HomeContainer} isHeaderHasBackground={false} />
                    <PrivateRoute path="/dashboard/temp" component={TempContainer} />
                    <PrivateRoute path="/dashboard/chart" component={Chart} />
                    <PrivateRoute path="/fundraising" component={Home2Container} isHeaderHasBackground={true} />
                    <PrivateRoute path="/selecting" component={Example3} />
                    <PrivateRoute path="/new/home" component={Home} />                    
                    <PrivateRoute path="/structuring" component={DataVisulisation} isHeaderHasBackground={false} />
                    <PrivateRoute path="/monitoring" component={Table} />
                    <PrivateRoute path="/slider" component={Slider} />
                    <PrivateRoute path="/profit" component={Profit} />
                    <PrivateRoute path="/example" component={Example} />
                    <PrivateRoute path="/chart" component={Xychart} />
                    <PrivateRoute path="/prophet" component={Profit} />
                    <PrivateRoute path="/pricing" component={Pricing} />                    
                    <Route render={() => <Redirect to="/" />} />
                </Switch>
                <ToastContainer />
            </Suspense>
        </>
    );
};

export default Routes;
