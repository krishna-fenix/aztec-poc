
import { CHART, HOME, HOME2, HOME3, LOGIN, REGISTER, TEMP, WELCOME } from '../constants/RoutePath';
import HomeContainer from '../screens/dashboard/home/HomeContainer';
import Home2Container from '../screens/dashboard/home2/HomeContainer';
import LoginContainer from '../screens/onboarding/login/LoginContainer';
import RegisterContainer from '../screens/onboarding/register/RegisterContainer';
import WelcomeContainer from '../screens/onboarding/welcome/WelcomeContainer';
import TempContainer from '../screens/dashboard/temp/TempContainer';
import Chart from '../screens/dashboard/chart/Chart';
import Example3 from '../screens/dashboard/example3/example3';
import TestNeovis from '../screens/data-visulisations/Tools/NeoVis-js/test-neovis';

const Routes = [
    {
        path : WELCOME,
        component : WelcomeContainer,
        title : "Welcome",
        needsAuth: false,
    },
    {
        path : LOGIN,
        component: LoginContainer,
        title : "Login",
        needsAuth: false,
    },
    {
        path : REGISTER,
        component: RegisterContainer,
        title : "Register",
        needsAuth: false,
    },
    {
        path : TEMP,
        component: TempContainer,
        title : "Temp",
        needsAuth: true,
    },
    {
        path : CHART,
        component: Chart,
        title : "Chart",
        needsAuth: true,
    },
    {
        path : HOME2,
        component: Home2Container,
        title : "Home 2",
        needsAuth: true,
    },
    {
        path : HOME3,
        component: Example3,
        title : "Home 3",
        needsAuth: true,
    },
    {
        path : HOME,
        component: HomeContainer,
        title : "Home",
        needsAuth: true,
    },
    {
        path : '/test-neovis',
        component: TestNeovis,
        title : "Neo Viz",
        needsAuth: false,
    },
    
]

export default Routes;