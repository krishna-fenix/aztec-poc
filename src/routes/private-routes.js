// eslint-disable-next-line no-unused-vars
import React, { Component } from 'react';
import { Route, Redirect } from "react-router-dom";
import { isLoggedIn } from "../services/common_services";

const PrivateRoute = ({ component: Component, ...rest }) => {

    let user = isLoggedIn();     
    return (
        <Route exact
            {...rest}
            render = {props =>
                user ? (
                    <Component headerHasBackground={rest.isHeaderHasBackground} {...props} />
                ) : (
                    <Redirect to={{ pathname: '/onboarding/login', state: { from: props.location } }} />
                )
            }
        />
    );
}

export default PrivateRoute;