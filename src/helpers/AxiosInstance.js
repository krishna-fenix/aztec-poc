import axios from 'axios';
// import { notify, getAccessToken, getFamilyId } from '../services/common_services';

const AxiosInstance = axios.create({
  baseURL: "",
});

const axiosRequestHandler = async (request) => {
  //   const accessToken = await getAccessToken();
  // request.headers['authorization'] = ' xxxxxxxxxx';
  // request.headers['Content-Type'] = 'application/json';

  return request;
};

const axiosResponseHandler = (response) => {
  const response_data = response.data;

  return {
    data: response_data,
    statusCode: 200,
  }

  // if (response.data.success === '1') {
  //   return {
  //     data: response_data.data,
  //     message: response_data.user_friendly_message,
  //     statusCode: 200,
  //   };
  // } else {
  //   return {
  //     data: response_data.data,
  //     message: response_data.user_friendly_message,
  //     statusCode: response_data.error[0],
  //   };
  // }
};

const axiosResponseErrorHandler = (error) => {
  // const {status, data} = error.response;
  // switch (status) {
  //   case 400:
  //   case 401:
  //   case 402:
  //   case 403:
  //   case 405:
  //   case 406:
  //   case 407:
  //     notify(data.message, 'error');
  //     break;
  //   case 404:
  //     notify(data.message, 'error');
  //     break;
  //   case 500:
  //     notify(data.message, 'error');
  //     break;
  //   default:
  //     notify(data.message, 'error');
  //     break;
  // }
};

AxiosInstance.interceptors.request.use(
  (request) => axiosRequestHandler(request),
  (error) => Promise.reject(error),
);

AxiosInstance.interceptors.response.use(
  (response) => axiosResponseHandler(response),
  (error) => axiosResponseErrorHandler(error),
);

export const axiosGet = (api, queryParams = {}) => {

  return AxiosInstance
    .get(api, { params: queryParams })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log('eror');
    });
};

export const axiosPost = (api, body = {}) => {
  // let formData = new FormData();

  // for (const key in body) {
  //   if (body.hasOwnProperty(key)) {
  //     formData.append(key, body[key]);
  //   }
  // }

  return AxiosInstance
    .post(api, body)
    .then((response) => {
      return response;
    })
    .catch((error) => { });
};
