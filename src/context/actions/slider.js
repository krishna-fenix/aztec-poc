import { RESTAPI } from "../../constants/RestApi";
import { axiosGet, axiosPost } from "../../helpers/AxiosInstance";
import { SLIDER_FAIL, SLIDER_LOADING, SLIDER_SUCCESS } from "../../constants/ActionTypes";

export const slider = () => (dispatch) => {
    dispatch({
        type: SLIDER_LOADING,
    });

    axiosGet(RESTAPI.GET_NODE_VALUE).then((response) => {
        if (response.statusCode === 200) {
            dispatch({
                type: SLIDER_SUCCESS,
                payload: response.data
            })
        } else {
            dispatch({
                type: SLIDER_FAIL,
                payload: response.message ? response.message : "Something went wrong"
            })
        }

    }).catch(error => {
        dispatch({
            type: SLIDER_FAIL,
            payload: error.response ? error.response.data : "Something went wrong"
        })
    })
}