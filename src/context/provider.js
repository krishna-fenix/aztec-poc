import { createContext, useReducer } from "react";
import {INITIAL_STATE} from "./initialstate/SliderInitialState";
import Slider from "./reducers/Slider";

export const GlobalContext = createContext({});

const GlobalProvider = ({ children }) => {
    const [sliderState, sliderDispatch] = useReducer(Slider, INITIAL_STATE);

    return (
        <GlobalContext.Provider
        
            value={{
                sliderState,
                sliderDispatch
            }}>
            {children}

        </GlobalContext.Provider>
    )
}

export default GlobalProvider