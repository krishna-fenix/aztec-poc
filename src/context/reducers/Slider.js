import { SLIDER_FAIL, SLIDER_LOADING, SLIDER_SUCCESS } from "../../constants/ActionTypes";

const Slider = (state, { type, payload }) => {

    switch (type) {
        case SLIDER_LOADING:
            return {
                ...state,
                loading: true,
            };

        case SLIDER_SUCCESS:
            return {
                ...state,
                loading: false,
                data: payload,
            };

        case SLIDER_FAIL:
            return {
                ...state,
                loading: false,
                error: payload,
            };

        default:
            return state
    }
}

export default Slider;
