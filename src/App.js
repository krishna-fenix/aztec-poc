import React from 'react';
import './App.css';
import Routes from './routes/routes';
import { useLocation } from "react-router-dom";
import WelcomeContainer from './screens/new/onboarding/welcome/WelcomeContainer';
import HomeContainer from './screens/dashboard/home/HomeContainer';
import { getUser } from './services/common_services';

function App() {

    let location = useLocation();    

    return (        
        <div id="main__app"> 
            {
                (!!getUser() && location.pathname === '/') ? <HomeContainer/> : 
                location.pathname !== "/" ? <Routes /> : <WelcomeContainer />
            }                       
        </div>
    );
}

export default App;