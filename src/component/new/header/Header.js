import React, { useState } from 'react'
import './styles.css';
import { useHistory } from 'react-router-dom';
import { Button, Image } from 'semantic-ui-react'
import home_logo from '../../../assets/images/ic_home_logo.png'
import { Menu, Icon, Popup } from 'semantic-ui-react'
import { getUser } from '../../../services/common_services';
import place_holder from '../../../assets/images/ic_place_holder.png'
import firebase from "../../../firebase/config";

const Header = () => {
    const CONST = {
        HOME: "HOME",
        REPORT: "REPORT",
        MY_FUND: "My Fund",
        FUNDRAISING: "Fundraising",
        SELECTING: "Selecting",
        STRUCTURING: "Structuring",
        MONITORING: "Monitoring",
        EXITING: "Exiting",
    }
    const history = useHistory();
    const user = getUser()

    const [headSelected, setHeadSelected] = useState(CONST.HOME)
    const [activeItem, setActiveItem] = useState(CONST.MY_FUND)

    const handleHeaderClick = (TYPE) => {
        if (TYPE === CONST.HOME) {
            // history.push(LOGIN)
        } else if (TYPE === CONST.REPORT) {
            // history.push(NEWHOME)
        }
        setHeadSelected(TYPE)
    }

    const handleItemClick = (name) => {
        if (name === CONST.MY_FUND) {
            // history.push(HOME)
        } else if (name === CONST.FUNDRAISING) {
            // history.push(HOME2)
        }
        setActiveItem(name)
    }

    const onSignOut = () => {
        firebase.auth().signOut()
        // localStorage.removeItem("user")
        localStorage.clear()
        history.push('/onboarding/login')
    }

    return (
        <div style={{ width: "100%" }} >

            <div className="d-flex flex-wrap p-0">
                <div className="col-md-1">

                    <Image src={home_logo} height="80" width="150" />

                </div>

                <div className="col-md-2" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>

                    <div className="d-flex justify-content-start flex-wrap p-0">

                        <div style={{ marginRight: 10 }}>
                            <span className="btn"
                                style={{ fontSize: 15, color: "#fff" }}
                                onClick={() => handleHeaderClick(CONST.HOME)}>
                                Home
                            </span>

                            {
                                headSelected === CONST.HOME && <div style={{ background: "#0e895f", height: 2 }} />
                            }

                        </div>

                        <div >
                            <span className="btn"
                                style={{ fontSize: 15, color: "#fff" }}
                                onClick={() => handleHeaderClick(CONST.REPORT)}>
                                Report
                            </span>

                            {
                                headSelected === CONST.REPORT && <div style={{ background: "#0e895f", height: 2 }} />
                            }
                        </div>

                    </div>

                </div>

                <div className="col-md-3 justify-content-center" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>

                    <ul class="navbar-nav me-auto">

                        <Menu fluid secondary stackable >

                            <Menu.Item
                                style={{ color: "#fff" }}
                                name={CONST.FUNDRAISING}
                                active={activeItem === CONST.FUNDRAISING}
                                onClick={() => handleItemClick(CONST.FUNDRAISING)}
                            />

                            <Menu.Item
                                style={{ color: "#fff" }}
                                name={CONST.SELECTING}
                                active={activeItem === CONST.SELECTING}
                                onClick={() => handleItemClick(CONST.SELECTING)}
                            />

                            <Menu.Item
                                style={{ color: "#fff" }}
                                name={CONST.STRUCTURING}
                                active={activeItem === CONST.STRUCTURING}
                                onClick={() => handleItemClick(CONST.STRUCTURING)}
                            />

                            <Menu.Item
                                style={{ color: "#fff" }}
                                name={CONST.MONITORING}
                                active={activeItem === CONST.MONITORING}
                                onClick={() => handleItemClick(CONST.MONITORING)}
                            />

                            <Menu.Item
                                style={{ color: "#fff" }}
                                name={CONST.EXITING}
                                active={activeItem === CONST.EXITING}
                                onClick={() => handleItemClick(CONST.EXITING)}
                            />

                        </Menu>

                    </ul>

                </div>

                <div className="col-md-6">

                    <div className="d-flex justify-content-end flex-wrap p-0" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>

                        <Icon style={{ marginTop: 12 }} name="object group outline" inverted />

                        <h4 style={{ marginLeft: 10, marginRight: 10, color: "#fff" }}>|</h4>

                        <Popup flowing hoverable
                            className="popup"
                            trigger={
                                <div className="center">
                                    <Image src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} circular style={{ marginTop: 12, marginRight: 8, height: 30, width: 30 }} />
                                </div>
                            }>

                            <Button
                                style={{ marginTop: 10 }}
                                color="red"
                                size="small"
                                compact
                                onClick={onSignOut}>
                                Logout
                            </Button>

                        </Popup>

                    </div>

                </div>

            </div>

            <div style={{ width: "100%", height: 1, background: "#b6bcbf" }} />

        </div >
    )
}

export default Header
