import React from 'react'
import './styles.css';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react'
import home_logo from '../../assets/images/ic_home_logo.png';
import logoWhite from '../../assets/images/aztec_group_logo_white.svg';
import place_holder from '../../assets/images/ic_place_holder.png'
import firebase from "../../firebase/config";
import { getUser } from '../../services/common_services';
import NotifBlack from '../../assets/images/email_unread_black.svg';
import NotifWhite from '../../assets/images/email_unread_white.svg';

const Header = ({ hasBgColor, itemToBeActive}) => {
    const data = {
        MY_FUND: "/myfunds",
        FUNDRAISING: "/fundraising",
        SELECTING: "/selecting",
        STRUCTURING: "/structuring",
        MONITORING: "/monitoring",
        EXITING: "/exiting",
    }
    const user = getUser()
    const history = useHistory();    
    const handleItemClick = (name) => {        
        history.push(name)                
    }

    const onSignOut = () => {
        firebase.auth().signOut()        
        localStorage.clear()
        history.push('/onboarding/login')
    }

    return (
        <div style={{ background: hasBgColor ? 'transparent' : '#fff' }}>

            {/* <nav class="navbar navbar-expand-lg navbar-light "> */}

                <div class="container-fluid">
                    <div className="d-flex align-items-center py-2">
                        <div className="logo">
                            <Link to="/">
                                <img src={hasBgColor ? logoWhite : home_logo} alt="Aztez Logo" style={{
                                    width: 150
                                }} />
                            </Link>
                        </div> 
                        <div className="aztec__nav_menu d-flex justify-content-between align-items-end">
                            <div>                            
                                <ul class="p-0 m-0">
                                    <Menu fluid secondary pointing stackable className="header__navmenu">
                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="My Funds"
                                            active={itemToBeActive === data.MY_FUND}
                                            onClick={() => handleItemClick(data.MY_FUND)}
                                        />

                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="Fundraising"
                                            active={itemToBeActive === data.FUNDRAISING}
                                            onClick={() => handleItemClick(data.FUNDRAISING)}
                                        />

                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="Selecting"
                                            active={itemToBeActive === data.SELECTING}
                                            onClick={() => handleItemClick(data.SELECTING)}
                                        />

                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="Structuring"
                                            active={itemToBeActive === data.STRUCTURING}
                                            onClick={() => handleItemClick(data.STRUCTURING)}
                                        />

                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="Monitoring"
                                            active={itemToBeActive === data.MONITORING}
                                            onClick={() => handleItemClick(data.MONITORING)}
                                        />

                                        <Menu.Item
                                            className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                            name="Exiting"
                                            active={itemToBeActive === data.EXITING}
                                            onClick={() => handleItemClick(data.EXITING)}
                                        />                                   

                                        {/* <Menu.Item position="right" /> */}

                                    </Menu>                               
                                </ul>
                            </div>
                            <div className="aztec_nav_dd d-flex align-items-center" style={
                                hasBgColor ? {color: 'white'} : {color: '#333'}
                            }>
                                {/* <Icon style={{ marginTop: 12 }} name="object group outline" /> */}
                                <img src={hasBgColor ? NotifWhite : NotifBlack} alt="Notification icon" style={{
                                    width: 25, height: 25
                                }} />
                                <h2 className="my-0 ms-2">|</h2>
                                <button className="btn btn-outline-secondary dropdown-toggle profileBtn me-2 d-flex align-items-center" 
                                type="button" data-bs-toggle="dropdown" aria-expanded="false" style={
                                    hasBgColor ? {color: 'white'} : {color: '#333'}
                                }>
                                    <div className="profile-picture me-2">
                                        <img src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} alt="" style={{
                                            borderRadius: '50%'
                                        }} />
                                    </div>
                                    <div>
                                        <span style={{textTransform: 'capitalize'}}>{user.displayName}</span>
                                    </div>
                                </button>
                                <ul className="dropdown-menu mpf-user-dd">                                               
                                    <li onClick={() => onSignOut()} className="dropdown-item">Logout</li>
                                </ul>  

                                {/* <Popup flowing hoverable
                                    className="popup"
                                    trigger={
                                        <div className="center">
                                            <Image src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} circular style={{ marginTop: 12, marginRight: 8, height: 30, width: 30 }} />
                                            <h5>{user.displayName}</h5>
                                            <Icon style={{ marginTop: 10, marginLeft: 12 }} name="caret down" />
                                        </div>
                                    }>                                    

                                    <Button
                                        style={{ marginTop: 10 }}
                                        color="red"
                                        size="small"
                                        compact
                                        onClick={onSignOut}>
                                        Logout
                                    </Button>

                                </Popup> */}

                                <ul className="mpf-nav p-0 m-0">
                                    <li className="mpf-nav-link">
                                        <button className="btn btn-outline-secondary dropdown-toggle hamburger-icon" 
                                        type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                        style={hasBgColor ? {color: 'white'} : {color: 'black'}}>
                                            <i className="fa fa-bars"></i>                            
                                        </button>
                                        <ul className="dropdown-menu ham-menu">
                                            <li>
                                                <Link className="dropdown-item" to="/structuring">Structuring</Link>
                                            </li>
                                            <li>
                                                <Link className="dropdown-item" to="/prophet">Prophet</Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>           
                    </div>

                    {/* <div className="logo" class="col-sm-12 col-md-12 col-lg-3 col-xl-2 col-xxl-2">
                        <Image src={hasBgColor ? logoWhite : home_logo} size="small" />
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 justify-content-center">

                        <div class="row">

                            <ul class="col-sm-12 col-md-12 col-lg-8 col-xl-6 col-xxl-6  navbar-nav me-auto">

                                <Menu fluid secondary pointing stackable className="header__navmenu">
                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="My Funds"
                                        active={itemToBeActive === data.MY_FUND}
                                        onClick={() => handleItemClick(data.MY_FUND)}
                                    />

                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="Fundraising"
                                        active={itemToBeActive === data.FUNDRAISING}
                                        onClick={() => handleItemClick(data.FUNDRAISING)}
                                    />

                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="Selecting"
                                        active={itemToBeActive === data.SELECTING}
                                        onClick={() => handleItemClick(data.SELECTING)}
                                    />

                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="Structuring"
                                        active={itemToBeActive === data.STRUCTURING}
                                        onClick={() => handleItemClick(data.STRUCTURING)}
                                    />

                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="Monitoring"
                                        active={itemToBeActive === data.MONITORING}
                                        onClick={() => handleItemClick(data.MONITORING)}
                                    />

                                    <Menu.Item
                                        className={hasBgColor ? 'activeWithBg' : 'activeWithFg'}
                                        name="Exiting"
                                        active={itemToBeActive === data.EXITING}
                                        onClick={() => handleItemClick(data.EXITING)}
                                    />                                   

                                    <Menu.Item position="right" />

                                </Menu>

                            </ul>

                            <div className="col center" style={hasBgColor ? {color: '#fff'} : {color: '#333'}}>
                                <Icon style={{ marginTop: 12 }} name="object group outline" />

                                <h2 style={{ marginLeft: 10, marginRight: 10 }}>|</h2>

                                <Popup flowing hoverable
                                    className="popup"
                                    trigger={
                                        <div className="center">
                                            <Image src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} circular style={{ marginTop: 12, marginRight: 8, height: 30, width: 30 }} />
                                            <h5>{user.displayName}</h5>
                                            <Icon style={{ marginTop: 10, marginLeft: 12 }} name="caret down" />
                                        </div>
                                    }>                                    

                                    <Button
                                        style={{ marginTop: 10 }}
                                        color="red"
                                        size="small"
                                        compact
                                        onClick={onSignOut}>
                                        Logout
                                    </Button>

                                </Popup>
                                
                                <ul className="mpf-nav p-0 m-0 mt-3">
                                    <li className="mpf-nav-link">
                                        <button className="btn btn-outline-secondary dropdown-toggle hamburger-icon" 
                                        type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                        style={hasBgColor ? {color: 'white'} : {color: 'black'}}>
                                            <i className="fa fa-bars"></i>                            
                                        </button>
                                        <ul className="dropdown-menu ham-menu">
                                            <li>
                                                <Link className="dropdown-item" to="/structuring">Structuring</Link>
                                            </li>
                                            <li>
                                                <Link className="dropdown-item" to="/prophet">Prophet</Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>                        

                    </div> */}

                </div>

            {/* </nav> */}
        </div>
       
    )
}

export default Header
