import React, { useState } from 'react'
import ic_arrow_right from '../../assets/images/ic_arrow_right.svg'
import ic_search from '../../assets/images/ic_search.svg'
import Tree from 'react-animated-tree'
import { Image } from 'semantic-ui-react';
import './styles.css';

const treeStyles = {
    top: 40,
    left: 40,
    color: 'black',
    fill: 'black',
    width: '100%'
}

const typeStyles = {
    fontSize: '2em',
    verticalAlign: 'middle'
}

const SideMenu = () => {
    const [isShowing, setIsShowing] = useState(true)

    const showHide = () => {
        if (isShowing) {
            setIsShowing(false)
        } else {
            setIsShowing(true)
        }
    }

    return (
        isShowing && <div style={{ backgroundColor: "#fff", padding: 16, margin: 12, minWidth: 300, borderRadius: 12 }}>
            <div className="row">
                <div className="col-2">
                    <div className="back"
                        onClick={() => showHide()}
                    ><Image className="justify-content-center" src={ic_arrow_right} /></div>
                </div>
                <div className="col-1" style={{ marginTop: 6 }}>
                    <div><h1>FSN</h1></div>
                    <div><h5>CAPITAL</h5></div>
                </div>

                <div className="row" style={{ backgroundColor: "#f6f8fa", padding: 12, margin: 20 }}>
                    <Image className="col" src={ic_search} style={{ height: 25, width: 25 }} />
                    <h6 className="col-10">Fibo</h6>
                </div>

                <div style={{ backgroundColor: "#f6f8fa", height: 2 }} />

                <Tree content="FSN Capital" canHide open style={treeStyles}>
                    <Tree content="Alpha fund" type={<span style={typeStyles}>🙀</span>} canHide >
                        <Tree content="Europa portfolio" type={<span style={typeStyles}>🙀</span>} canHide  >
                            <Tree content="Fibo Limited" style={{ color: '#63b1de' }} />
                            <Tree content="Raliy Limited" style={{ color: '#63b1de' }} />
                            <Tree content="Crop Limited" style={{ color: '#63b1de' }} />
                        </Tree>
                    </Tree>
                    <Tree content="Growth portfolio" type={<span style={typeStyles}>🙀</span>} canHide />
                    <Tree content="Gamma Fund" canHide />
                    <Tree content="Data Fund" canHide />
                </Tree>
            </div>
        </div>
    )
}

export default SideMenu
