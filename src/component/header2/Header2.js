import React, { useState } from 'react'
import { Button, Icon, Image, Menu, Popup } from 'semantic-ui-react'
import home_logo from '../../assets/images/ic_home_logo.png'
import { HOME, HOME2 } from '../../constants/RoutePath'
import { getUser } from '../../services/common_services'
import firebase from "../../firebase/config";
import place_holder from '../../assets/images/ic_place_holder.png'
import { useHistory } from 'react-router'

const Header2 = () => {
    const data = {
        MY_FUND: "My Fund",
        FUNDRAISING: "Fundraising",
        SELECTING: "Selecting",
        STRUCTURING: "Structuring",
        MONITORING: "Monitoring",
        EXITING: "Exiting",
    }
    const user = getUser()
    const history = useHistory();
    const [activeItem, setActiveItem] = useState(data.MY_FUND)
    const handleItemClick = (name) => {
        if (name === data.MY_FUND) {
            history.push(HOME)
        }else if(name === data.FUNDRAISING ){
            history.push(HOME2)
        }
        setActiveItem(name)
    }

    const onSignOut = () => {
        firebase.auth().signOut()
        // localStorage.removeItem("user")
        localStorage.clear()
        history.push('/onboarding/login')
    }
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-light ">
                <div class="container-fluid">
                    <div className="logo" class="col-sm-12 col-md-12 col-lg-3 col-xl-2 col-xxl-2">
                        <Image src={home_logo} size="small" />
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 justify-content-center">

                        <div class="row">

                            <ul class="col-sm-12 col-md-12 col-lg-8 col-xl-6 col-xxl-6  navbar-nav me-auto">

                                <Menu fluid secondary stackable >
                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.MY_FUND}
                                        active={activeItem === data.MY_FUND}
                                        onClick={() => handleItemClick(data.MY_FUND)}
                                    />

                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.FUNDRAISING}
                                        active={activeItem === data.FUNDRAISING}
                                        onClick={() => handleItemClick(data.FUNDRAISING)}
                                    />

                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.SELECTING}
                                        active={activeItem === data.SELECTING}
                                        onClick={() => handleItemClick(data.SELECTING)}
                                    />

                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.STRUCTURING}
                                        active={activeItem === data.STRUCTURING}
                                        onClick={() => handleItemClick(data.STRUCTURING)}
                                    />

                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.MONITORING}
                                        active={activeItem === data.MONITORING}
                                        onClick={() => handleItemClick(data.MONITORING)}
                                    />

                                    <Menu.Item
                                        style={{ color: "#fff" }}
                                        name={data.EXITING}
                                        active={activeItem === data.EXITING}
                                        onClick={() => handleItemClick(data.EXITING)}
                                    />

                                </Menu>

                            </ul>

                            <div className="col center">
                                <Icon style={{ marginTop: 12 }} name="object group outline" inverted />

                                <h2 style={{ marginLeft: 10, marginRight: 10, color: "#fff" }}>|</h2>

                                <Popup flowing hoverable
                                    className="popup"
                                    trigger={
                                        <div className="center">
                                            <Image src={user.photoURL !== null && user.photoURL !== "" ? user.photoURL : place_holder} circular style={{ marginTop: 12, marginRight: 8, height: 30, width: 30 }} />
                                            <h5
                                                style={{ color: "#fff" }}>
                                                {user.displayName}
                                            </h5>
                                            <Icon style={{ marginTop: 10, marginLeft: 12 }} inverted name="caret down" />
                                        </div>
                                    }>

                                    {/* <PopupHeader>{user.displayName}</PopupHeader> */}

                                    <Button
                                        style={{ marginTop: 10 }}
                                        color="red"
                                        size="small"
                                        compact
                                        onClick={onSignOut}>
                                        Logout
                                    </Button>

                                </Popup>

                                <Icon style={{ marginTop: 12, marginLeft: 12 }} size="large" name="bars" inverted />

                            </div>

                        </div>

                    </div>

                </div>
            </nav>
        </div>
    )
}

export default Header2
