import React from 'react';
import Chart from 'chart.js/auto';

const BezierCurveChart = ({chartId, chartData}) => {

    React.useEffect(() => {
        var ctx = document.getElementById(chartId).getContext('2d');
        initializeChart(ctx);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [chartId])

    const initializeChart = (ctx) => {       
        
        new Chart(ctx, {
            type: "line",
            data: {
                labels: chartData?.labels,
                datasets: [                    
                    {
                        label: "Historical",
                        backgroundColor: 'rgb(243,119,40, 0.9)',
                        borderColor: 'rgb(243,119,40, 1)',
                        data: chartData?.historial_data,
                        fill: false,
                        borderWidth: 1,
                        pointHitRadius: 1,
                        pointRadius: 1,
                        tension: 0.4,
                        showLine: true,                        
                    },
                    {
                        label: "Yhat",
                        backgroundColor: 'rgba(73,159,248,0.9)',
                        borderColor: 'rgba(73,159,248, 1)',
                        data: chartData?.yhat,
                        fill: false,
                        borderWidth: 1,
                        pointHitRadius: 1,
                        pointRadius: 1,
                        // tension: 0.4,
                        showLine: true,                      
                    },
                    {
                        label: "Upper Bound",
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132, 1)',
                        data: chartData?.upper_bound,
                        fill: '-1',
                        borderWidth: 1,
                        pointHitRadius: 1,
                        pointRadius: 1,
                        // tension: 0.4,
                        showLine: true,                      
                    },
                    {
                        label: "Lower Bound",
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        data: chartData?.lower_bound,
                        fill: '-1',
                        borderWidth: 1,
                        pointHitRadius: 1,
                        pointRadius: 1,
                        // tension: 0.4,
                        showLine: true,                       
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,               
                scales: {
                    x: {
                        grid: {
                            drawBorder: true,
                            display: true
                        },
                        ticks: {
                            display: true
                        }
                    },
                    y: {
                        grid: {
                            display: true,
                            drawBorder: true,
                        },
                        ticks: {
                            stepSize: 100,
                            display: true
                        },
                        min: 0,
                        max: 200,                       
                        beginAtZero: true
                    }                                                 
                },
                plugins: {
                    legend: {
                        position: 'top',
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Line'
                    },
                    tooltip: {
                        enabled: true
                    }
                },
                // parsing: false
            }            
        });
    }

    return (
        <div id="bezier__chart">
            <div className="bezier__chart_container w__graph_container">
                <canvas id={chartId} width="150" height="350"></canvas>
            </div>
        </div>
    );
}

export default BezierCurveChart;