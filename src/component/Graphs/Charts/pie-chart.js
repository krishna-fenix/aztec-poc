import React from 'react';
import { PieChart, PieSeries } from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import { create, useTheme, percent, color, Label, Container } from "@amcharts/amcharts4/core";

const RingPieChart = ({id}) => {

    useTheme(am4themes_animated);
    useTheme(am4themes_kelly);

    React.useEffect(() => {
        var chart = create(id, PieChart);
        
        chart.data = [{ "country": "Website sales", "litres": "$14.3M" },
        { "country": "Subscription", "litres": "$3.2M" },
        { "country": "Add-on", "litres": "$5.1M" }];

        chart.innerRadius = percent(50);

        var pieSeries = chart.series.push(new PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        pieSeries.stroke = '#26808e';
        pieSeries.fill = pieSeries.stroke
        pieSeries.fontSize = '12px';
        pieSeries.fontWeight = 'normal';
        pieSeries.slices.template.stroke = color("#fff");
        pieSeries.slices.template.strokeWidth = 1;
        pieSeries.slices.template.strokeOpacity = 1;

        var container = new Container();
        container.parent = pieSeries;

        var label = new Label();
        label.parent = container;
        label.text = "$22.5 M";
        label.stroke = '#000000';        
        label.horizontalCenter = "middle";
        label.verticalCenter = "middle";
        label.fontSize = 16;
        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
    }, [id])

    return (
        <div id={id}></div>
    );
}

export default RingPieChart;