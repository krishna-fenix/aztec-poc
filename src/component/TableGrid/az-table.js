import React from 'react';
import {AgGridColumn, AgGridReact} from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
// import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import './az-table.css';

const AzTable = ({tableData}) => {

    const [gridApi, setGridApi] = React.useState(null);
    const [gridColumnApi, setGridColumnApi] = React.useState(null);

    const onGridReady = (params) => {
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    };

    const rowStyle = { background: 'rgba(0, 0, 0, 0.1)', color: "#fff" };

    const getRowStyle = params => {
        if (params.node.rowIndex % 2 !== 0) {
            return { background: 'rgba(255, 255, 255, 0.1)' };
        }
    };

    // const dynamicCellStyleRevenue = params => {
    //     if (params.value === 123) {
    //         return {
    //             background: "linear-gradient(to right,rgba(14, 182, 121, 0.3), rgba(0, 0, 0, 0.1))"
    //         }
    //     }
    //     return null;
    // };

    // const dynamicCellStyleOperationEx = params => {
    //     if (params.value === 143.295) {
    //         return {
    //             background: "linear-gradient(to right,rgba(91, 53, 80, 0.8),rgba(91, 53, 80, 0.8), rgba(0, 0, 0, 0.1))"
    //         }
    //     }
    //     return null;
    // };

    return (
        <div id="aztech__grid_table" className="w-100">
             <div className="ag-theme-alpine">
                {/* <AgGridReact
                    rowData={tableData.rowData}>
                        {
                            tableData.fields.map((table_item, item_key) => (
                                <AgGridColumn field={table_item} sortable={ true } filter={ true } />
                            ))
                        }                    
                </AgGridReact> */}
                <AgGridReact
                    rowStyle={rowStyle}
                    getRowStyle={getRowStyle}
                    rowData={tableData.rowData}
                    rowSelection="multiple"
                    suppressRowClickSelection={true}
                    defaultColDef={{
                        editable: true,
                        sortable: true,
                        minWidth: 100,
                        filter: true,
                        resizable: true,
                        // floatingFilter: true,
                        flex: 1,
                    }}
                    components={{
                        rowNodeIdRenderer: function (params) {
                            return params.node.id + 1;
                        },
                    }}
                    sideBar={{
                        toolPanels: ['columns', 'filters'],
                        defaultToolPanel: '',
                    }}
                    pagination={true}

                    paginationPageSize={500}
                    onGridReady={onGridReady}
                    enableCharts={true}
                    enableRangeSelection={true}
                    rowDragManaged={true}
                    animateRows={true}>

                        {
                            tableData.fields.map((table_item, item_key) => (
                                // <AgGridColumn field={table_item} sortable={ true } filter={ true } />
                                <AgGridColumn
                                    field={table_item}
                                    headerName={table_item}
                                    enableValue={true}
                                    columnGroupShow="closed"                                    
                                    width={120}
                                    flex={0}                        
                                    enablePivot={true}
                                    enableRowGroup={true}
                                    sortable={ true } 
                                    filter={ true }
                                />
                            ))
                        }  
                    
                </AgGridReact>
            </div>
        </div>
    );
}

export default AzTable;