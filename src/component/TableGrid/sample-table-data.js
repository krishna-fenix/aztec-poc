export const INCOME_REPORT = {
    rowData: [
        {market: "Ireland", profit: "$388", revenue: '$626,663'},
        {market: "Sweden", profit: "$89,412", revenue: '$556,531'},
        {market: "Uruguay", profit: "$90,993", revenue: '$674,045'},
        {market: "Franch", profit: "$76,458", revenue: '$601,871'},
        {market: "Portugal", profit: "$80,454", revenue: '$519,643'},
        {market: "Columbia", profit: "$72,889", revenue: '$503,344'},
        {market: "Malta", profit: "$6,772", revenue: '$906,076'},
        {market: "Italy", profit: "$12,536", revenue: '$660,049'}        
    ],
    fields: ['market', 'profit', 'revenue']
}