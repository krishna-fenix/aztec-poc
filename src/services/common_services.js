import { EMAIL_DOMAIN } from "../config/Env";
import { RESTAPI } from "../constants/RestApi";

const BASEURI = 'https://us-central1-aztec-technical-poc.cloudfunctions.net/prophet';

export const setUser = (userData) => {
  localStorage.user = JSON.stringify(userData)
};

export const getUser = () => {
  const jsonValue = localStorage.user;
  return jsonValue != null ? JSON.parse(jsonValue) : null;
};

export const saveTableState = (state) => {
  localStorage.tableState = JSON.stringify(state)
};

export const exportTableState = () => {
  const jsonValue = localStorage.tableState;
  return jsonValue != null ? JSON.parse(jsonValue) : null;
};

export const isLoggedIn = () => {
    const user = getUser()
    return user ? true : false;
    //   return !!getUser()
}

export const getProphetOptions = () => {
    return fetch(BASEURI+RESTAPI.PROPHET_OPTIONS)
    .then(response => response.json())  
}

export const getHistoricalProphet = () => {
    return fetch(BASEURI+RESTAPI.HISTORICAL_PROPHET)
    .then(response => response.json())  
}

export const getProphetPredictions = (period, option) => {
    return fetch(`${BASEURI+RESTAPI.PREDICT_PROPHET}?period=${period}&&option=${option}`)
    .then(response => response.json())  
}

export const checkDomain = (email) => {
    for(let i=0; i<EMAIL_DOMAIN.length;i++){
      if(email.split('@')[1] === EMAIL_DOMAIN[i]){
        return true
      }
    }

    return false
}

export const getFormatedCurrency = (amount) => {
    if(amount > 999 && amount < 1000000){
        return (amount/1000).toFixed(0) + 'K';
    }else if(amount > 1000000){
        return (amount/1000000).toFixed(0) + 'M';
    }else {
        return amount;
    }
}

export const CURRENCY_SYMBOLS = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'CRC': '₡', // Costa Rican Colón
    'GBP': '£', // British Pound Sterling
    'ILS': '₪', // Israeli New Sheqel
    'INR': '₹', // Indian Rupee
    'JPY': '¥', // Japanese Yen
    'KRW': '₩', // South Korean Won
    'NGN': '₦', // Nigerian Naira
    'PHP': '₱', // Philippine Peso
    'PLN': 'zł', // Polish Zloty
    'PYG': '₲', // Paraguayan Guarani
    'THB': '฿', // Thai Baht
    'UAH': '₴', // Ukrainian Hryvnia
    'VND': '₫', // Vietnamese Dong
};