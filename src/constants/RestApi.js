export const RESTAPI = {
    GET_NODE_VALUE : "https://us-central1-aztec-technical-poc.cloudfunctions.net/predict/autocomplete",
    POST_NODE : "https://us-central1-aztec-technical-poc.cloudfunctions.net/predict",
    HISTORICAL_PROPHET: '/historical',
    PREDICT_PROPHET: '/predict',
    PROPHET_OPTIONS: '/options'
}