// Login
export const LOGIN_LOADING = 'LOGIN_LOADING';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

// slider

export const SLIDER_LOADING = "SLIDER_LOADING";
export const SLIDER_SUCCESS = "SLIDER_SUCCESS";
export const SLIDER_FAIL = "SLIDER_FAIL";