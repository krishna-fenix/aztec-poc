//LOGIN
export const WELCOME = "/onboarding/welcome"
// export const WELCOME = "/welcome"
export const LOGIN = '/onboarding/login';
export const REGISTER = "/onboarding/register";

//Dashboard
export const HOME = '/home';
export const HOME2 = '/dashboard/home2';
export const HOME3 = '/dashboard/home3';
export const HOME4 = '/dashboard/home4';
export const NEWHOME = '/new/home';
export const PRICING = "/pricing"

export const TEMP = "/dashboard/temp"
export const CHART = "/dashboard/chart"
export const GRAPH = "/graphs";
export const TABLE = "/table";
export const SLIDER = "/slider";
